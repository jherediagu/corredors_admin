@include('layouts.header')
@include('layouts.menu')

<body>
  
    <div class="container-fluid">
    
      <div class="top-ranking top-proves">
            <div class="container">
              <h1>Possem a prova...</h1>
            </div>
        </div>           
        
        <!-- PROVES top--> 
        <div class="bgblau ">
          <section class="serveis-llebres"> 
                <div class="container"> 
                  <div class="row"> 
                        <div class="col-sm-7">
                          <div class="tit-prova">... Asics GEL VIRAGE 4 RUNNING</div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                            <p class=""><strong>Nom de l’espònsor</strong></p>
                        </div>
                        <div class="col-sm-5">
                            <div class="img-circle"><img src="img/proves1.jpg" width="100%" height="auto" class="img-circle perfil_entrev"></div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <!-- //PROVES top--> 
        
        <!-- PROVES descripció/imatges/video--> 
        <section class="serveis"> 
            <div class="container"> 
                <h1 class="titular-form marbot45 martop45">Títol de l’article sobre la prova de material</h1>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi. </p>
                
                <p>&nbsp;</p>
                <div class="imat-noti marbot60">
                  <img src="img/noticia1.jpg">
                </div>
                
                <div class="imat-noti marbot60">
                  <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" width="100%" src="https://www.youtube.com/embed/3YFg5zHUdy4" frameborder="0" allowfullscreen=""></iframe>
          </div>
                </div>
                
            </div>
        </section> 
        <!-- //PROVES descripció/imatges/video- -->     
        
        
        <!--PROVES SORTEIG de material -->
        <div class="bgblau2">
          <section class="entrena-inf"> 
                <div class="container"> 
                  <p class="text-center"><img src="img/icon-proves.png" width="103" height="103"></p>
                    <p class="text-center"><span>Sorteigs de material </span></p>
                    <p class="text-justify">Sorteigs de material 
Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi. </p>
                    
                   
                     <form class="form-associat text-left">
                         <div class="row">
                            <div class="col-sm-7">
                                <label>Nom del sorteig</label>
                                <div class="select-style" style="background-color:#d6ebeb">
                                    <select class="form-control">
                                      <option selected="selected" disabled="disabled">Selecciona una cursa</option>
                                      <option>Nom del sorteig 1</option>
                                      <option>Nom del sorteig 2</option>
                                      <option>Nom del sorteig 3</option>
                                      <option>Nom del sorteig 4</option>
                                      <option>Nom del sorteig 5</option>
                                    </select>
                                </div>
                            </div>
                             <div class="col-sm-5">
                                <label>Espònsor</label>
                                <div class="select-style" style="background-color:#d6ebeb">
                                    <select class="form-control">
                                      <option selected="selected" disabled="disabled">Selecciona un espònsor</option>
                                      <option>Espònsor 1</option>
                                      <option>Espònsor 2</option>
                                      <option>Espònsor 3</option>
                                      <option>Espònsor 4</option>
                                      <option>Espònsor 5</option>
                                    </select>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                      </div>
                     </form>
                    
                    <div class="infosorteig">
                        <div class="entrenador">
                          <form class="form-associat text-left">
                                <div class="row">
                                    <div class="col-md-5 col-sm-7" id="sliderSortejos">
                                      <ul class="bxslider3" style="height:auto">
                                          <li style="background:url(img/prod1.jpg) no-repeat center center; background-size:contain"><img src="img/tr-q.png" width="100%" height="auto"></li>
                                            <li style="background:url(img/prod2.jpg) no-repeat center center; background-size:contain"><img src="img/tr-q.png" width="100%" height="auto"></li>
                                            <li style="background:url(img/prod3.jpg) no-repeat center center; background-size:contain"><img src="img/tr-q.png" width="100%" height="auto"></li>
                                            <li style="background:url(img/prod4.jpg) no-repeat center center; background-size:contain"><img src="img/tr-q.png" width="100%" height="auto"></li>
                                        </ul>
                                        <div class="clearfix"></div>
                                        <div id="bx-pager">
                                          <a data-slide-index="0" href="" style="background:url(img/prod1.jpg) no-repeat center center; background-size:contain"><img src="img/tr-q.png"></a>
                                          <a data-slide-index="1" href="" style="background:url(img/prod2.jpg) no-repeat center center;background-size:contain"><img src="img/tr-q.png"></a>
                                          <a data-slide-index="2" href="" style="background:url(img/prod3.jpg) no-repeat center center;background-size:contain"><img src="img/tr-q.png"></a>
                                          <a data-slide-index="3" href="" style="background:url(img/prod4.jpg) no-repeat center center;background-size:contain"><img src="img/tr-q.png"></a>
                                        </div>
                                         <div class="clearfix"></div>
                                    </div>
                                    <div class="col-md-7 col-sm-5">
                                        <p class="tit-prova2">Testeig de roba tèrmica de running</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis </p>
                                        <p><strong>Espònsor</strong><br>
                                        Nom de l’espònsor.</p>
                                        <strong>Opcions del material</strong><br>
                                        <div class="select-style" style="margin-top:5px">
                                            <select class="form-control">
                                              <option selected="selected" disabled="disabled">Selecciona un material</option>
                                              <option>Material 1</option>
                                              <option>Material 2</option>
                                              <option>Material 3</option>
                                              <option>Material 4</option>
                                              <option>Material 5</option>
                                            </select>
                                        </div>
                                        <div class="clearfix"></div>
                                        <p><strong>Data</strong><br>
                                        24/02/2016</p>
                                    </div>
                                    <div class="col-sm-12">
                                      <h2>Informació del sorteig</h2>
                                    </div>
                                    <div class="col-sm-8 text-justify marbot30">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error si</div>
                                    <div class="col-sm-4 marbot60"><img src="img/porves-logo.jpg" class="logo-sponsor"></div>
                                    <div class="col-sm-12">
                                        <div class="imat-noti marbot60">
                                            <div class="embed-responsive embed-responsive-16by9">
                                                <iframe class="embed-responsive-item" width="100%" src="https://www.youtube.com/embed/3YFg5zHUdy4" frameborder="0" allowfullscreen=""></iframe>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-default btn-lg  pull-right marbot30">Participar en el sorteig</button>
                                
                            </form>
                        </div>
                    </div>
                    
                    
 
              </div>
            </section>
        </div>
        <!-- //PROVES SORTEIG de material-->       
         

                
                
                
                
                
     
    </div>
    
@include('layouts.footer')     


     <script type="text/javascript">
  
    $(document).ready(function() {
        
         bxslider3 = $('.bxslider3').bxSlider({
                  //mode: 'horizontal',
                  //slideWidth: 247,
                  //infiniteLoop: true,
                  pager: true,
                  controls: true,
                  auto:true,
                  pause:4000,
                  speed:1000,
                  minSlides:1,
                  maxSlides:1,
                  moveSlides:1,
                  pagerCustom: '#bx-pager'
                });
                
        /*PLEGA DESPLEGA sortejos*/
        $(".mostrasorteig").click(function(event){
            event.preventDefault();
            $('.infosorteig').slideToggle(function(){       
                bxslider3.reloadSlider();
            });

        }); 
        
        
        });
    </script>