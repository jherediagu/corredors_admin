@include('layouts.header')
@include('layouts.menu')

<body>

 
    <div class="container-fluid">
    
        <div class="top-ranking top-reptes">
            <div class="container">
                <h1>Reptes a Corredors.cat</h1>
            </div>
        </div>
            
        
        <!-- REPTE--> 
           <section class="serveis"> 
            <div class="container"> 
            

                <h1 class="titular-form marbot45 martop20">Millora les teves marques amb Corredors.cat</h1>
                <p>Des de la comissió esportiva de l’associació oferim a totes les persones associades la possibilitat de millorar les seves marques en 10km, mitja i marató, a través dels reptes de Corredors.cat.</p>
                
                <h1 class="titular-form marbot45 martop20">Com funciona?</h1>
                <p>Des de l’associació ens vam adonar que el corredor popular a l’hora d’afrontar una cursa, mitja o marató de vegades desconeix com cal entrenar per a assolir una marca determinada segons les seves possibilitats de temps i de nivell.</p>
                <p>Des de 2008 hem preparat diferents <i>reptes</i> i ens han ajudat atletes de renom nacional com: <strong>Abel Antón</strong> (2008), <strong>Fabián Roncero</strong> (2009), <strong>Julio Rey</strong> (2011) o <strong>Maria Luisa Muñoz</strong> (2015) i d’altres entrenadors i atletes de caire més local com <strong>Pol Guillén</strong> (2012/13)-, <strong>Rosa Morató</strong> (2012/13) o <strong>Roger Roca</strong> (2010). </p>
                <p>Per tal d’ajudar al corredor, l’associació  al llarg de l’any organitza diversos <i>reptes</i> per millorar les marques dels seus corredors o bé per tenir una primera presa de contacte amb un entrenador/atleta profesional que els guiarà a l’hora d’afrontar la seva primera marató o mitja o bé millorar les seves marques.</p>
                <p><strong>Corredors.cat</strong> contacta amb un/dos atletes d’èlit de nivell nacional que organitzaran un pla d’entrenament i un seguiment personalitzat per a cada corredor. La durada del repte variarà en funció de l’objectiu: 14/16 setmanes marató, 10/12 setmanes una mitja o 8/10 setmanes un 10km.</p>
                <p>Els <i>reptes</i> inclouen trobades presencials on el corredor popular pot trobar el tracte més personal de l’entrenador i conèixer persones que tenen un objectiu similar al seu. Normalement es fan entre dues i cinc trobades.</p>
                <p>Els preus varien però estan al voltant de 50/70 € i, en ocasions,  s’inclou alguna inscripció a alguna cursa o serveis de fisioteràpia.</p>
                <p>&nbsp;</p>
                
                
        <div class="cap-asso">
                <div class="cap-asso-tit">
                    <p><strong>Que haig de fer per apuntar-me al repte?</strong></p>
                    <p class="text-light"><span>Primer de tot ser associat de Corredors.cat, després moltes ganes de compartir un mateix objectiu amb gent il·lusionada com tu.</span></p>
                   <div class="clearfix"></div>
                   <a class="btn btn-white btn-lg" href="form-soci.php">ASSOCIA’T</a>
                </div>
                
            </div>
    
    @foreach( $challenges as $challenge )       
            <!--bloc  REPTE 1-->
            <div class="repte">
                <h2>{{ $challenge->season_challenge }}</h2>
                <h1 class="text-uppercase">{{ $challenge->name }}</h1>
                <?php echo $challenge->description ?>
                
                <div class="blocrepte">
                    <div class="col-sm-12">
                        <h1 class="titular-form marbot45">Tens un repte amb...</h1>
                        <h3>...{{ $challenge->person_challange }}</h3>
                    </div>
                    <div class="col-sm-5">
                        <div class="img-circle"><img src="img/entrevista-perfil.jpg" width="100%" height="auto" class="img-circle perfil_entrev"></div>
                    </div>
                    <div class="col-sm-7">
                        <p class="tit-dades">Data d’inici</p>
                        <p class="dades-repte">{{ substr($challenge->start_date,0,10) }}</p>
                        <p class="tit-dades">Durada del repte</p>
                        <p class="dades-repte">{{ $challenge->total_time }}</p>
                        <p class="tit-dades">Preu repte:</p>
                        <p class="dades-repte">{{ $challenge->price_subscribed }}€ associats / {{ $challenge->price_registered }}€ no associats</p>
                        <p class="tit-dades marbot24">Termini d’inscripció</p>
                        <?php echo $challenge->limit_date ?>
                        <p><a href="{{ $challenge->link }}" target="_blank">{{ $challenge->link }}</a></p>
                    </div>
                </div>
                <a class="btn btn-default btn-lg pull-right marbot60" href="javascript:;">apuntar-me al repte</a>
            </div>
            <!--//bloc  REPTE 1-->
    @endforeach
        
            </div><!-- //container -->
        </section>
        <!-- //REPTE-->
        
        
    </div>

@include('layouts.footer')     
