@include('layouts.header')
@include('layouts.menu')




<body class="bg-associat">
   <!-- formulari associar-se-->
    <div class="container-fluid padbot60">
    
         <div class="container" style="padding:22px 0 0 0px">         
            <div class="col-sm-12 cap-asso">
                <div class="cap-asso-tit"><img src="img/icon-corredors-white.png"><br>Associa’t!!!</div>
            </div>
        </div>
        
        <section class="questions associat">
            <div class="container">
                <div class="row">           
                    <div class="col-sm-12"><br><br>

                    @if (Session::has('errors'))
                        <div class="alert alert-warning" role="alert">
                        <ul>
                            <strong>Oops! Something went wrong : </strong>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif 

                    </div>
                    
                    <div class="col-sm-12">

                     <h1 class="titular-form">Fes-te soci/a i gaudeix dels avantatges</h1>
                        
                        <p>L' associació esportiva <b>Corredors.cat</b>, una associació independent i sense ànim de lucre que vol ser un punt de trobada pels corredors populars i de tot nivell.</p>
                        <p>Entre els objectius d’aquest portal cal destacar el foment de l’activitat esportiva i la creació d’una via per a compartir experiències entre corredors de tots nivells, així com el d’ajudar als nostres associats en tots aquells aspectes relacionats amb allò que més ens agrada, el córrer.</p>
                        <p>Per mantenir aquest portal d’atletisme i tirar endavant el nostre projecte necessitem el teu suport, <b>associa't</b>!! Formaràs part del nostre col·lectiu i ajudaràs a consolidar el projecte.</p>
                        
                        <div class="tarifes">
                            <div class="row">            
                                <div class="col-sm-5"><strong>El preu varia en funció del trimestre de l’any que t’associïs:</strong></div>
                                <div class="col-sm-7 tarifes-all"><small>1er/</small> 10€ &nbsp;&nbsp;<small>2on/</small> 8€ &nbsp;&nbsp;<small>3er/</small> 6€ &nbsp;&nbsp;<small>4t/</small> 4€ </div>
                            </div>
                        </div>
                    </div>
    
                    

                {!! Form::open(['route' => 'auth/register', 'class' => 'form-associat']) !!}
    
                    
                        <div class="col-sm-12">
                            <h2>Dades d’associat</h2>
                        </div>
                    
                        <div class="col-md-5 col-sm-6">
                            <label>Nickname<span>*</span></label>
                            <input name="nickname" type="text" class="form-control" placeholder="Nom d'usuari">
                        </div>
                        <div class="col-md-7 col-sm-6">
                            <label>E-mail<span>*</span></label>
                            <input name="email" type="email" class="form-control" placeholder="direcciomail@mail.com">
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-5 col-sm-6">
                            <label>Password<span>*</span></label>
                            <input name="password" type="text" class="form-control" placeholder="Password">
                        </div>
                        <div class="col-md-5 col-sm-6">
                            <label>Confirmar password<span>*</span></label>
                            <input name="password_confirmation" type="password" class="form-control" placeholder="Confirmar password">
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-5 col-sm-6">
                            <label>Codi de seguretat<span>*</span></label><br>
                            <small><em>Aquí codi del captcha</em></small>
                            <!-- captcha -->
                        </div>
                        <div class="clearfix"></div>
                        
                        <div class="col-sm-12">
                            <h2>Informació personal</h2>
                        </div>
                    
                        <div class="col-md-5 col-sm-6">
                            <label>Nom<span>*</span></label>
                            <input name="name" type="text" class="form-control alert" placeholder="(exemple avís camp obligatori)">
                        </div>
                        <div class="col-md-7 col-sm-6">
                            <label>Cognoms<span>*</span></label>
                            <input name="lastname" type="text" class="form-control" placeholder="Cognoms">
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-5 col-sm-6">
                            <label>Data de naixement<span>*</span></label>
                            <input name="birthday" type="date" class="form-control" placeholder="dd/mm/aaaa">
                        </div>
                        <div class="col-md-5 col-sm-6">
                            <label>DNI<span>*</span></label>
                            <input name="dni" type="text" class="form-control" placeholder="Núm DNI + lletra">
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-7 col-sm-6">
                            <label>Adreça<span>*</span></label>
                            <input name="address" type="text" class="form-control" placeholder="Adreça">
                        </div>
                        <div class="col-md-5 col-sm-6">
                            <label>Ciutat<span>*</span></label>
                            <input name="city" type="text" class="form-control" placeholder="Ciutat">
                        </div>
                        
                        <div class="clearfix"></div>
                        <div class="col-md-2 col-sm-6">
                            <label>Codi Postal<span>*</span></label>
                            <input name="postal_code" type="number" class="form-control" placeholder="C.P.">
                        </div>

                        <div class="col-md-5 col-sm-6">
                            <label>Província<span>*</span></label>
                            <input name="province" type="text" class="form-control" placeholder="Província">
                        </div>
                        <div class="col-md-5 col-sm-6">
                            <label>Telèfon de contacte</label>
                            <input name="telephone" type="number" class="form-control" placeholder="Telèfon">
                        </div>
                        <div class="clearfix"></div>
                        
                        
                        <div class="col-sm-12">
                            <h2>Informació de pagament de la quota anual</h2>
                        </div>
                        
                        <div class="col-sm-6">
                            <label>Nom del titular<span>*</span></label>
                            <input name="titular_name" type="text" class="form-control" placeholder="Nom">
                        </div>
                        <div class="col-sm-6">
                            <label>Cognoms del titular<span>*</span></label>
                            <input name="titular_lastname" type="text" class="form-control" placeholder="Cognoms">
                        </div>
                        <div class="clearfix"></div>
                        
                        <div class="col-sm-12">
                            <label>Número de compte bancari IBAN<span>*</span></label>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-2 col-sm-2  col-xs-4">
                           <input name="IBAN_country_code" type="text" class="form-control cuenta cuentatot" maxlength="4" size="4" style="width:101px">
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-4">
                            <input name="IBAN_cheque_number" type="text" class="form-control cuenta cuentatot" maxlength="4" maxlength="4" size="4" style="width:101px">
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-4">
                            <input name="IBAN_bank_code" type="text" class="form-control cuenta cuentatot" maxlength="4" size="4" style="width:101px">
                        </div>
                        <div class="col-md-1 col-sm-1 col-xs-3">
                            <input name="IBAN_sort_code" type="text" class="form-control cuentatot" maxlength="2" size="2" style="width:51px">
                        </div>
                        <div class="col-md-5 col-sm-5  col-xs-9">
                            <input name="IBAN_account_number" type="text" class="form-control cuentatot" maxlength="10" size="10" style="width:251px">
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-sm-12">
                            <label>Concepte<span>*</span></label>
                            <input name="concept" type="text" class="form-control" placeholder="Concepte">
                            <div class="clearfix"></div>
                            
                            <p><span class="blauverd">*</span> Dades obligatòries</p>
                            <div class="clearfix"></div>
                            
                            <input name="legal" type="checkbox" value="" class="checknou"><label>He llegit i accepto <a href="javascript:;"  data-toggle="modal" data-target="#myModal2">les condicions</a></label>
                            <p>&nbsp;</p>
                        
                        {{ csrf_field() }}

                        {!! Form::submit('FER EL PAGAMENT i associar-me',['class' => 'btn btn-default btn-lg  marbot45']) !!}

                      </div>

                    {!! Form::close() !!}

                    <div class="clearfix"></div>
                    
                    
             <!--        <div class="col-sm-12">
                        <p><small><em>Exemples de resposta:</em></small></p>
                        
                        <div class="alert alert-success" role="alert">Formulari enviat correctament</div>
                        <div class="alert alert-warning" role="alert">Revisi els camps obligatoris</div>
                        <div class="alert alert-danger" role="alert">El formulari no s'ha pogut enviar.</div>
                    
                    </div> -->
                    <p>&nbsp;</p>
                    
               </div> <!-- //row-->
            </div><!-- //container-->
        </section>
    </div>
    <!-- //formulari associat -->
    

@include('layouts.footer')     
