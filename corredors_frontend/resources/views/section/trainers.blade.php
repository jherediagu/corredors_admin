@include('layouts.header')
@include('layouts.menu')

<body>


    <div class="container-fluid">
    
      <div class="top-ranking top-entrena">
            <div class="container">
              <h1>Entrenadors</h1>
            </div>
        </div>           
        
       <!-- ENTRENADOR--> 
        <section class="serveis"> 
            <div class="container"> 
            
                <h1 class="titular-form marbot45 martop20">Entrenadors Corredors.cat</h1>
                <p>Des de la comissió esportiva de l’associació oferim a totes les persones associades la possibilitat de disposar  del servei d’entrenadors de forma personalitzada segons la disponibilitat, objectius i nivell de l’atleta. Aquest servei l’estem oferint des de el 2009  i els dos entrenadors que col·laboren amb nosaltres en l’actualitat sónt: <strong>Jordi Ballesteros</strong> i <strong>Raúl Crespo</strong>. </p>
                <p>La disponibilitat dels entrenadors és total i són de fàcil accés via mail, washapp, trucada o foro. Ells ens envien quinzenal o setmanalment   els entrenaments en funció dels nostres objectius i disponibilitats.</p>
                <p>El grup del <strong>Jordi Ballesteros</strong> entrena a les pistes de Cornellà i el de <strong>Raúl Crespo</strong> a les del Prat. En tots dos casos no es conta l’entrada a pistes que va a banda.</p>
                <p>Amb el Cornellà Atlètic hem arribat a un acord anual per 140 € on podem utilitzar les pistes excepte dilluns, dimecres i divendres tarda on tenen prioritat l’escola d’atletisme infantil (de 17:00 a 19:00). També inclou vestidors, dutxes i gimnàs. </p>

                <p>&nbsp;</p>

            </div><!-- //container -->
        </section>     
        
        <div class="bgblau">
          <section class="serveis"> 
                <div class="container"> 
                  <h1 class="titular-form blauverd marbot15">Servei d’entrenadors</h1>
                    <div class="preus-mensuals">
                      <div class="tipus-entreno">
                          <div class="tipus-entreno-tit">Entrenaments presencials <img src="img/arrowright-blue.png" width="20" height="20"></div>
                            <div class="tipus-entreno-data blauverd">Dijous i dimarts quinzenals</div>
                            <div class="tipus-entreno-preu">26€</div>
                        </div>
                        <div class="tipus-entreno">
                          <div class="tipus-entreno-tit">Entrenaments virtuals <img src="img/arrowright-blue.png" width="20" height="20"></div>
                            <div class="tipus-entreno-data"></div>
                            <div class="tipus-entreno-preu">23€</div>
                        </div>
                    </div>
                    
              @foreach ($trainers as $trainer )

                     <!-- entrenador num 1-->
                    <div class="entrenador">
                      <div class="imat-entrena">
                          <div class="cap-perfil-tit"><img src="img/entrevista-perfil.jpg" class="img-circle"></div>
                        </div>
                      <div class="tit-entrena">ENTRENADOR</div>
                        <div class="nom-entrena">{{ $trainer->name }}</div>
                        <?=$trainer->description ?>
                    </div>
                    <!-- //entrenador num1-->

                @endforeach
                    
                </div>
            </section>
        </div>        
         <!-- //ENTRENADOR-->
         
         
         <div class="bgblau2">
          <section class="entrena-inf"> 
                <div class="container text-center"> 
                  <p><span>I ara què?</span></p>
                    <p>Si esteu interessats o, teniu cap dubte, passeu <br class="hidden-xs">pel tema del foro situat a l’apartat: </p>
                    <p class="tar3">corredors.cat/entrenaments/d’entrenadors-assessors/servei d’entrenadotrs a Corredors.cat</p>
                    <p>En aquest apartat els mateixos entrenadors i/o les persones que formen <br class="hidden-xs">part de la Junta i de la comissió esportiva us aclariran tots els dubtes.</p>
                    <p>Desitgem que us interessi aquesta oferta.</p>
                    <p><strong>CoRReDoRS.CaT</strong></p>
                </div>
            </section>
        </div>
                
                
                
                
                
     
    </div>

    
@include('layouts.footer')     
