@include('layouts.header')
@include('layouts.menu')

<body>
    <!--sabatilles -->
    <section class="primera">
    <div class="container-fluid bgmarro">
        <div class="container">
            <div class="row">
                <div class="col-sm-5 col-sm-offset-1 sabatilles"><img src="img/logo-sabatilles2.png" alt="Sabatilles Solidàries">                
    
                </div> 
             <div class="col-sm-6 text-center">
                    <p>&nbsp;</p>
                    <p class="text-light">Si has esgotat la vida útil de les teves sabatilles, però encara estan noves, o ja no saps què fer amb tantes samarretes tècniques a l'armari...</p>
                    <div class="btn-inclinat">...Dóna a<br>Sabatilles Solidàries!</div>
                </div>
            </div>    
        </div>
    </div>
    </section>
     <!-- //sabatilles -->
   

    
    <!-- projecte-->
    <section>
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <h1 class="titular-sab">El projecte solidari</h1>
                <p><em>Sabatilles Solidàries</em> es una de les activitats més conegudes de Corredors.cat, que es desenvolupa durant els dos dies que dura la fira del corredor de la Marató de Barcelona. </p>
                <p>Aquest projecte consisteix en la recollida de sabatilles usades i en bon estat general, per a ser entregades a entitats que ajuden gent necessitada.</p>
            </div>
            <div class="clearfix"></div>          

            <div class="col-sm-6">
                <div class="row">
                    <div class="col-sm-12 col-xs-6"><img src="img/sabatilles1.jpg" width="100%" height="auto" class="margin-imatge"></div>
                    <div class="col-sm-12 col-xs-6"><img src="img/sabatilles2.jpg" width="100%" height="auto"></div>
                </div>
            </div> 
            <div class="col-sm-6">
           <h1 class="titular-sab">Què puc donar?</h1>
           <p>Les entitats receptores volen el material per a caminar, no per a córrer, i les sabatilles hauran de complir uns requisits mínims: </p>
           <p><strong>El tèxtil i la sola han d’estar sencers.</strong> No acceptarem sabatilles amb esquerdes a la sola o trencades per la zona tèxtil.</p> 
           <p><strong>Cal entregar-les amb plantilla.</strong> No cal que duguin l'original, però ha de ser del mateix núm. que la sabatilla. És a dir si algú té unes Asics Cumulus, ha perdut la plantilla i té la d’unes NB 1080, ja serveix. </p>
           <p><strong>Han d’estar netes.</strong> Amb una rentada usant el programa curt n’hi haurà prou. </p>
           <p><strong>Poden ser d’entrenament per asfalt, mixtes, de competició o de trail.</strong> S’accepten sabates de muntanya, com les que Salomon ha popularitzat en els darrers anys. </p>
           <p><strong>També s’acceptarà roba, principalment samarretes.</strong> En aquest cas haurà de ser nova del tot o utilitzada tan poc que no es noti que s’ha usat.</p>
            </div>
            <div class="clearfix"></div>            
       </div> 
    </div>
    </section>
    <!-- //projecte-->
    
    
   <!--punts de recollida -->
    <section class="primera">
    <div class="container-fluid bgmarro">
        <div class="container">
            <div class="row">
                 
                <div class="col-sm-6 puntsrecollida">
                   <h1 class="titular-sab">Punts de recollida </h1>
                   
                   <p>Podeu apropar-vos a les següents entitar per a fer les vostres donacions a Sabatilles Solidàries:</p>
                   
                   <p class="ubicacio">Assís Centre d’Acollida</p>
                   <p>Associació sense ànim de lucre de Barcelona dedicada a l’atenció integral de persones en situació de sense llar.</p>
                   <p class="direccio">C. direcció de la tenda, 99, 08500 LOCALITAT</p>
                   <p><a href="http://www.assis.cat/" target="_blank">http://www.assis.cat/</a></p>
                   
                   <p class="ubicacio">Figueralia</p>
                   <p>ONG que recull material esportiu per a ser donat a entitats benèfiques.</p>
                   <p class="direccio">C. direcció de la tenda, 99, 08500 LOCALITAT</p>
                   <p><a href="http://figueralia.org/" target="_blank">http://figueralia.org/</a></p>
                  
                   <p class="ubicacio">Arrels</p>
                   <p>Col·laboren en el desenvolupament integral de les persones en situació d’exclusió social, concretament en el de les persones sense sostre que es troben en una situació més crítica.</p>
                   <p class="direccio">C. direcció de la tenda, 99, 08500 LOCALITAT</p>
                   <p><a href="http://www.arrelsfundacio.org/" target="_blank">http://www.arrelsfundacio.org/</a></p>
                  
                   <p class="ubicacio">Rauxa</p>
                   <p>Treballen per a la rehabilitació de persones marginals del carrer que pateixen alcoholisme crònic.</p>
                   <p class="direccio">C. direcció de la tenda, 99, 08500 LOCALITAT</p>
                   <p><a href="http://www.rauxa.org/" target="_blank">http://www.rauxa.org/</a></p>
                  
                   <p class="ubicacio">Casal dels Infants del Raval</p>
                   <p>L’objectiu és aconseguir millores concretes i duradores en la qualitat de vida dels infants, joves i famílies en situació o risc d’exclusió social.</p>
                   <p class="direccio">C. direcció de la tenda, 99, 08500 LOCALITAT</p>
                   <p><a href="http://www.casaldelsinfants.org/cat/" target="_blank">http://www.casaldelsinfants.org/cat/</a></p>
                  
                   <p class="ubicacio">Centre Maria Reina</p>
                   <p>Centre adreçat a menors que estan tutelats per la Direcció General d’Atenció a la Infància.</p>
                   <p class="direccio">C. direcció de la tenda, 99, 08500 LOCALITAT</p>

                </div>
                <div class="col-md-4 col-sm-5  col-sm-offset-1 sabatilles2 ">
                    <p class="text-center"><img src="img/logo-sabatilles3.png" alt="Sabatilles Solidàries"></p>
                    <p class="text-center infoad">Si ets organitzador i vols col.laborar, recollint sabatilles i roba esportiva, posa't en contacte amb nosaltres:</p>
                   <p class="enllasos"> <a class="ico-mail" href="mailto:sabatilles@corredors.cat">sabatilles@corredors.cat</a>
                    <a class="ico-twitter" href="https://twitter.com/ssolidaries" target="_blank">@ssolidaries</a>
                    <a class="ico-face" href="https://www.facebook.com/Sabatilles-Solid%C3%A0ries-464601166936153/?fref=ts" target="_blank">Sabatilles-Solidàries</a></p>
                </div>
            </div>    
        </div>
    </div>
    </section>
     <!-- //punts de recollida -->
    
        <!-- donatius-->
    <section>
    <div class="container">
            
            <div class="titular-sab">On i quan fer els donatius</div>

            @foreach ( $shoes as $shoes )
                <div class="lloc-donatiu">{{ $shoes->name }}</div>
                <div class="donatiu">
                    <?=$shoes->description?><br><br>
                    <a href="{{ $shoes->link }}" target="_blank">{{ $shoes->link }}</a>           
                </div>
            @endforeach

            <div class="clearfix"></div>
            
       </div> 
    </div>
    </section>
    <!-- //donatius-->
    

    
@include('layouts.footer')     
