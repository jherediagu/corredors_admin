@include('layouts.header')
@include('layouts.menu')

<body>

    <!-- container fluid-->
    <div class="container-fluid">
        <div class="submenu subquisom">
            <div class="container text-center">
                <ul>
                    <li><a href="#seccio-associacio" class="ancla">L’associació</a></li>
                    <li><a href="#seccio-historia" class="ancla">Història</a></li>
                    <li class=""><a href="#seccio-objectius" class="ancla">Objectius i Estatuts</a></li>
                    <li><a href="#seccio-associat" class="ancla">Com associar-m’hi?</a></li>
                    <li><a href="#seccio-sabatilles" class="ancla">Sabatilles Solidàries</a></li>
                    <li><a href="#seccio-comissions" class="ancla">Comissions</a></li>
                </ul>
            </div>
        </div>
      <div class="capsalera quisom">
        <div class="container">
            <h1 class="titblanc">Qui som</h1>
        </div>
    </div>

    </div>
    <!-- //container fluid-->
    
    <!-- principis-->
    <section><a id="seccio-associacio"></a>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="titular-form marbot45">L’associació Corredors.cat</h1>
                <p class="marbot20">Corredors.cat som una associació esportiva sene ànim de lucre formada per persones físiques amb la voluntat principal de fomentar i desenvolupar la pràctica continuada de l'esport.</p>
            </div>
            <div class="clearfix"></div>
            
            <!--principis-->
            <div class="col-sm-6">  
                <div class="principi pri-primer">  
                    <h1 class="titular-form minim">Principis Generals</h1>
                    <ul class="interior">
                        <li>L’objectiu principal és el foment, el desenvolupament i la pràctica continuada de l’activitat física i esportiva, fonamentalment de l'Atletisme, però també del triatló, l'excursionisme i en general qualsevol esport que s'hi pugui vincular.</li>
                        <li>No té finalitat de lucre.</li>
                        <li>L’associació s’estableix fonamentalment entre persones que s'entenen en català i usen Internet com a mitja de comunicació.</li>
                        <li>L'activitat esportiva principal de l'entitat és de caràcter de lleure, i no participar en competicions organitzades per les federacions esportives.</li> 
                    </ul>
                </div>       
            </div> 
            <div class="col-sm-6">
                <div class="principi"> 
                    <h1 class="titular-form minim">Voluntat de servei</h1>
                    <p>Per aconseguir els seus objectius, l'associació pretén aportar a la societat en general i als associats en particular, els següents serveis:</p>
                    <ul class="interior">
                        <li>Web i un fòrum d’accés públic</li>
                        <li>Informació i formació: entrenament, nutrició, medicina...</li>
                        <li>Beneficis econòmics , generalment descomptes. Aquest serveis va orientat únicament als associats</li>
                        <li>Organització d'actes, trobades, jocs, sortides...</li>
                        <li>Informació complerta sobre les competicions</li>
                        <li>Control de qualitat de les curses</li>
    
                    </ul>
                </div>
            </div>
            
           <div class="col-sm-12 "> Alguns d'aquests objectius ja s'ha assolit i es mantenen,  altres estan a prop del nivell d'assoliment desitjat, i d'altres tenen encara un llarg recorregut per assolir-los. Esperem que amb l'ajuda de tothom els acabem assolint tots.</div>
            <div class="clearfix"></div>
            <!--//principis-->
            
       </div> 
    </div>
    </section>
    <!-- //principis-->
    
        <!-- historia-->
    <section><a id="seccio-historia"></a>
    <div class="container">
        <div class="historia">
            
            <div class="titular-prods marbot25">Història</div>
            <div class="row">
                <div class="col-sm-3 titular-prods2">Antecedents</div>
                <div class="col-sm-9 histo">
                    El 2003 es va crear un fòrum d'atletisme anomenat <em>10de1000.com</em> que va ser un motor i un impulsor de l'atletisme popular a Catalunya. A finals del 2006, els propietaris del projecte decideixen tancar-lo per manca de rendibilitat com a projecte empresarial.<br><br>
Alguns dels usuaris habituals d'aquell fòrum, en sentir-nos orfes d'aquell punt de trobada i veien les dificultats que comportava mantenir-ho com un projecte empresarial, vam decidir associar-nos per crear un fòrum i mantenir i multiplicar aquelles relacions d'amistat que havíem construït.
                </div>
                 <div class="col-sm-12">&nbsp;</div>
                <div class="col-sm-3 titular-prods2">Constitució</div>
                <div class="col-sm-9 histo">
                    El 20 de Gener de 2007 es constitueix l'Associació Esportiva Corredors.cat. En aquella assemblea constituent va ser on es va decidir el nom i es van aprovar els estatuts i la Junta directiva que n'hauria de portar el timó els propers anys.<br><br>Hi van signar com a socis fundadors, 53 persones.
                </div>
            
            </div>
            <div class="clearfix"></div>
            
       </div> 
    </div>
    </section>
    <!-- //historia-->
    
    
    <!-- objectius-->
    <section><a id="seccio-objectius"></a>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="titular-form marbot45">Objectius i estatuts</h1>
            </div>
            <div class="clearfix"></div>
          
            <div class="col-sm-4 text-center object">
                <img src="img/quisom-icon1.png">
                <div class="sep"></div>
                <p>Fomentar el desenvolupament de la pràctica continuada de l’activitat física i esportiva</p>
            </div> 
            <div class="col-sm-4 text-center object">
                <img src="img/quisom-icon2.png">
                <div class="sep"></div>
                <p>Fomentar l'ús del Català i d'Internet com a mitja de comunicació</p>
            </div> 
            <div class="col-sm-4 text-center object">
                <img src="img/quisom-icon3.png">
                <div class="sep"></div>
                <p>Aconseguir el màxim nombre d'associats</p>
            </div> 
            <div class="clearfix"></div>
            <div class="col-sm-12 text-center">
                Pots consultar els <span class="blauverd"><strong>estatuts</strong></span> descarregant el PDF en el següent link: <a class="desc-pdf" href="estatuts-2011.pdf" target="_blank">Descarregar Pdf</a>
            </div>
            
       </div> 
    </div>
    </section>
    <!-- //objectius-->
    
    
    <!-- targeta i sabatilles -->    
    <section><a id="seccio-associat"></a>
    <div class="container-fluid ">
        
            <div class="container">
            <div class="bgTargeta">
          <div class="row">
                
                    <div class="col-sm-4 col-sm-offset-2">
                        <h1 class="titular-blanc text-center"><strong>Com associar-m’hi?</strong></h1>
                        <p class="text-center text-light">Si t’agradaria ser soci de Corredors.cat i participar en aquest projecte, pots fer-ho en el aquest enllaç:</p>
                       <p class="text-center"> <a class="btn btn-white btn-lg" href="form-soci.php">Assòcia't</a></p>
                    </div>
                    <div class="col-sm-6 tarjanom"><img src="img/targeta.png">
                    </div>
                </div>
            </div>
        </div>
        </div>
        
        <div class="container-fluid bgblau"><a id="seccio-sabatilles"></a>
        <div class="container">
            <div class="row">
                <div class="col-sm-5 col-sm-offset-1 sabatilles"><img src="img/logo-sabatilles2.png"  alt="Sabatilles Solidàries">                
    
                </div> 
                <div class="col-sm-6">
               <h1 class="titular-form marbot45">Sabatilles solidàries</h1>
               <p><em>Sabatilles Solidàries</em> es una de les activitats més conegudes de Corredors.cat, que es desenvolupa durant els dos dies que dura la fira del corredor de la Marató de Barcelona. </p>
               <p>Aquest projecte consisteix en la recollida de sabatilles usades i en bon estat general, per a ser entregades a entitats que ajuden gent necessitada.</p>
               <a class="btn btn-default btn-lg" href="sabatilles">Coneix el projecte</a>
                </div>
            </div>    
        </div>
    </div>
    </section>
     <!-- //targeta i sabatilles -->
     
     
     <!-- comissions-->
    <section class="marbot80"><a id="seccio-comissions"></a>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="titular-form marbot45">Comissions</h1>
                <p>Per organitzar-nos ho fem per comissions, l'objectiu és dur a terme una activitat, un servei, etc ja sigui permanent, puntual o temporal. Qualsevol associat/ada pot plantejar-ne una implicant-se en ella, si cal es dotarà d'una bústia de correu i/o subfòrum. Comissions actives actualment:</p>
                <div class="taula-comi">
                    <table width="100%" border="0" class="comissio" style="min-width:500px">
                      <tr>
                        <th width="26%">Comissió</th>
                        <th width="38%">Objectiu</th>
                        <th width="38%">Membres</th>
                      </tr>
            
                    @foreach($comissions as $comission)
                          <tr>
                            <td>{{ $comission->commission }} </td>
                            <td>{{ $comission->objective }}</td>
                            <td>{{ $comission->members }}</td>
                          </tr>
                    @endforeach
        
                    </table>
                </div>
            </div>
        </div>
    </div>
     </section>
     <!-- //comissions-->
    
@include('layouts.footer')     
