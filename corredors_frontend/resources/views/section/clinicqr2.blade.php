@include('layouts.header')
@include('layouts.menu')

<body>

    <div class="container-fluid">
    
        <div class="top-ranking top-clinics">
            <div class="container">
                <h1>Clínics QR2</h1>
            </div>
        </div>
            
        
<!-- CLINIC--> 
           <section class="serveis"> 
            <div class="container martop30"> 
                <p>Des de la comissió esportiva de l’associació i amb col·laboració amb empreses/marques de material esportiu interessades, organitzarem els “CLINIC QR2!” </p>
                <p>L’objectiu principal és per una banda apropar les principals marques comercials especialistes en running  als nostres associats, i per altra banda, crear una eina suficientment atractiva a la nova web, amb la finalitat d’augmetar el tràfic i indirectament augmentar el nostre número d’associats. </p>
                <p>Mitjançant acords amb diferents marques esportives especialistes en running, que desitgen realitzar una acció comercial/promocional dels seus productes oferim la plataforma de Corredors.cat i la base de dades dels nostres associats a canvi d’informar als nostres associats de les principals novetats en material de running.</p>
                
                <h1 class="titular-form marbot45 martop45">En qué consisteixen?</h1>
                <p>Es tracta de muntar aproximadament <b>cada trimestre o semestre un event en forma d’entrenament grupal</b> on una marca esportiva especialista en productes de running, fa una presentació dels seus productes (ja siguin sabatilles, roba tècnica, accesoris, etc.)  i ofereix la possibilitat de provar aquests articles a un número determinat d’associats de Corredors.cat.</p>
                <p>L’event consistirà en un entrenament grupal, prova de material, regal d’algún article als participants per part de la marca i avituallament i/o esmorzar i/o festa, concert, etc. facilitat per la marca esportiva.</p>

                <p>&nbsp;</p>
                
            <div class="cap-asso">
                <div class="cap-asso-tit">
                    <p><strong>Que cal fer per participar?</strong></p>
                    <p class="text-light"><span>Per poder assistir a l’event, s’ha de ser associat i els mateixos  s’hauràn inscrit previament en l’apartat corresponent de “CLINIC QR2”  fins que s’assoleixi el número màxim de participants limitat per la marca esportiva. Parlem d’events on normalment i haurà un topall de 50 associats, que seràn els primers que s’inscriguin.</span></p>
                   <div class="clearfix"></div>
                   <a class="btn btn-white btn-lg" href="form-soci.php">ASSOCIA’T</a>
                </div>
                
            </div>
        @foreach ( $clinicqr2 as $clinic )

            <!--bloc  CLINIC 1-->
            <div class="repte clinic">
                <h2 class="text-center">{{ $clinic->title_clinic }}</h2>
                <h1 class="text-center text-uppercase">{{ $clinic->title }}</h1>
                
                <?php echo $clinic->description ?>
                
                <div class="blocrepte blocclinic">
                    <div class="col-sm-5">
                        <div class="img-circle"><img src="img/proves1.jpg" width="100%" height="auto" class="img-circle perfil_entrev"></div>
                    </div>
                    <div class="col-sm-7">
                        <p class="tit-dades">Data</p>
                        <p class="dades-repte">{{ substr($clinic->date,0,10) }}</p>
                        <p class="tit-dades">Hora</p>
                        <p class="dades-repte">{{ substr($clinic->hour,0,5) }}h</p>
                        <p class="tit-dades">Lloc de trobada</p>
                        <p class="dades-repte">{{ $clinic->place }}</p>
                        <p class="tit-dades ">Número de places</p>
                        <p class="dades-repte">{{ $clinic->number_seats }}</p>

                    </div>
                </div>
                <a class="btn btn-default btn-lg pull-right marbot60" href="javascript:;">apuntar-me al clínic</a>
            </div>
            <!--//bloc  CLINIC 1-->

        @endforeach 
   
            </div><!-- //container -->
        </section>
        <!-- //CLINIC-->    
    </div>

    
@include('layouts.footer')     
