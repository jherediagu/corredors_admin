@include('layouts.header')
@include('layouts.menu')

<body>

   
   
     <div class="container-fluid">
    
        <div class="top-ranking top-cistella">
            <div class="container">
                <h1>La meva cistella</h1>
            </div>
        </div> 
            
        
        <!-- CISTELLA--> 
           <section class="serveis cistella"> 
            <div class="container padbot30"> 

                <div class="taula taula-cistella">
                      <table width="100%" border="0" style="min-width:500px" class="taula-cistella">
                         <thead>
                          <tr>
                            <th width="38%" align="left" class="top-taula-prod">Productes</th>
                            <th width="15%" align="center">Preu / u.</th>
                            <th width="22%" align="center">Quantitat</th>
                            <th width="15%" align="center">Suma producte</th>
                            <th width="10%" align="center">&nbsp;</th>
                          </tr>
                          </thead>
                      <tbody>
                      <tr>
                        <td class="thum-prod" width="38%" align="left">
                            <div class="thum-prod-img" style="background-image:url(img/prod1.jpg)"><img src="img/tr-q.png"></div>
                            <div class="pull-left cistella-nomprod">Nom del producte<br><span>Disponible</span></div>
                        </td>
                        <td width="15%" align="center">10,00€</td>
                        <td width="22%" align="center">
                            <div class="variar">
                                <a href="javascript:;"><span class="glyphicon glyphicon-minus-sign tar mes-menys" aria-hidden="true"></span></a>
                            </div>
                            <input name="" type="text" class="form-variar" value="2">
                            <div class="variar">
                                <a href="javascript:;"><span class="glyphicon glyphicon-plus-sign tar mes-menys" aria-hidden="true"></span></a>
                            </div>
                        </td>
                        <td width="15%" align="center"><strong>20,00€</strong></td>
                        <td width="10%" align="center"><a href="javascript:;"><img src="img/icon-delete.png" width="30" height="30"></a></td>
                      </tr>
                       <tr>
                        <td class="thum-prod" width="38%" align="left">
                            <div class="thum-prod-img" style="background-image:url(img/prod6.jpg)"><img src="img/tr-q.png"></div>
                            <div class="pull-left cistella-nomprod">Nom del producte<br><span>Disponible</span></div>
                        </td>
                        <td width="15%" align="center">10,00€</td>
                        <td width="22%" align="center">
                            <div class="variar">
                                <a href="javascript:;"><span class="glyphicon glyphicon-minus-sign tar mes-menys" aria-hidden="true"></span></a>
                            </div>
                            <input name="" type="text" class="form-variar" value="2">
                            <div class="variar">
                                <a href="javascript:;"><span class="glyphicon glyphicon-plus-sign tar mes-menys" aria-hidden="true"></span></a>
                            </div>
                        </td>
                        <td width="15%" align="center"><strong>20,00€</strong></td>
                        <td width="10%" align="center"><a href="javascript:;"><img src="img/icon-delete.png" width="30" height="30"></a></td>
                      </tr>
                       <tr>
                        <td class="thum-prod" width="38%" align="left">
                            <div class="thum-prod-img" style="background-image:url(img/prod7.jpg)"><img src="img/tr-q.png"></div>
                            <div class="pull-left cistella-nomprod">Nom del producte<br><span class="gris">En Reserva</span></div>
                        </td>
                        <td width="15%" align="center">10,00€</td>
                        <td width="22%" align="center">
                            <div class="variar">
                                <a href="javascript:;"><span class="glyphicon glyphicon-minus-sign tar mes-menys" aria-hidden="true"></span></a>
                            </div>
                            <input name="" type="text" class="form-variar" value="2">
                            <div class="variar">
                                <a href="javascript:;"><span class="glyphicon glyphicon-plus-sign tar mes-menys" aria-hidden="true"></span></a>
                            </div>
                        </td>
                        <td width="15%" align="center"><strong>20,00€</strong></td>
                        <td width="10%" align="center"><a href="javascript:;"><img src="img/icon-delete.png" width="30" height="30"></a></td>
                      </tr>
                       <tr>
                        <td class="thum-prod" width="38%" align="left">
                            <div class="thum-prod-img" style="background-image:url(img/prod3.jpg)"><img src="img/tr-q.png"></div>
                            <div class="pull-left cistella-nomprod">Nom del producte<br><span>Disponible</span></div>
                        </td>
                        <td width="15%" align="center">10,00€</td>
                        <td width="22%" align="center">
                            <div class="variar">
                                <a href="javascript:;"><span class="glyphicon glyphicon-minus-sign tar mes-menys" aria-hidden="true"></span></a>
                            </div>
                            <input name="" type="text" class="form-variar" value="2">
                            <div class="variar">
                                <a href="javascript:;"><span class="glyphicon glyphicon-plus-sign tar mes-menys" aria-hidden="true"></span></a>
                            </div>
                        </td>
                        <td width="15%" align="center"><strong>20,00€</strong></td>
                        <td width="10%" align="center"><a href="javascript:;"><img src="img/icon-delete.png" width="30" height="30"></a></td>
                      </tr>
                      </tbody>
                      <tfoot> 
                        <tr>
                            <th colspan="5" align="right" class="bottom-taula-prod">
                                    <div class="col-md-10 col-sm-9 col-xs-8">Import total<br><span>(IVA inclòs)</span></div>
                                    <div class="col-md-2 col-sm-3 col-xs-4">80,00€</div>
                            </th>
                        </tr>
                      </tfoot>
                    </table>

                </div> <!-- //taula TOT-->
                
                
               <a href="botiga-pagament.php" class="marbot15"><button type="button" class="btn btn-default btn-lg  pull-right iniciar-pagament" style="margin-bottom:20px">Iniciar pagament</button></a>
                <a href="botiga.php" class="inici_sessio padesp text-uppercase pull-right">Continuar comprant</a>
                <a href="javascript:;" class="inici_sessio padesp text-uppercase pull-right">Buidar cistella</a>
                
            </div><!-- //container -->
        </section>
        <!-- //CISTELLA-->
        
        
        
        
        
        <!-- CISTELLA BUIDA--> 
           <section class="serveis cistella"> 
            <div class="container padbot30"> 
            
                <p><small><em>(Exemple cistella buida:)</em></small></p>

                <div class="taula taula-cistella">
                      <table width="100%" border="0" style="min-width:500px" class="taula-cistella">
                         <thead>
                          <tr>
                            <th width="38%" align="left" class="top-taula-prod">Productes</th>
                            <th width="15%" align="center">Preu / u.</th>
                            <th width="20%" align="center">Quantitat</th>
                            <th width="15%" align="center">Suma producte</th>
                            <th width="10%" align="center">&nbsp;</th>
                          </tr>
                        </thead>
                    </table>
                </div> <!-- //taula TOT-->
                
                <div class="text-center cistella-buida">
                    <p><img src="img/icon-cistella3.png" width="57" height="57"></p>
                    <p>No has afegit cap<br>producte a la cistella</p>
                </div>
                <div class="taula taula-cistella">
                      <table width="100%" border="0" style="min-width:500px" class="taula-cistella">
                      <tfoot> 
                        <tr>
                            <th colspan="5" align="right" class="bottom-taula-prod desactivat">
                                  <div class="col-md-10 col-sm-9 col-xs-8">Import total<br><span>(IVA inclòs)</span></div>
                                    <div class="col-md-2 col-sm-3 col-xs-4">0€</div>
                            </th>
                        </tr>
                      </tfoot>
                    </table>

                </div> <!-- //taula TOT-->
                <a href="botiga-pagament.php" class="marbot15"><button type="button" class="btn btn-default btn-lg  pull-right iniciar-pagament desactivat" style="margin-bottom:20px">Iniciar pagament</button></a>
                <a href="botiga.php" class="inici_sessio padesp text-uppercase pull-right">Continuar comprant</a>
                <a href="javascript:;" class="inici_sessio padesp text-uppercase pull-right desactivat">Buidar cistella</a>

                
            </div><!-- //container -->
        </section>
        <!-- //CISTELLA BUIDA-->
        
        
   
        
        
        
        
        
        
    </div>

@include('layouts.footer')     
    