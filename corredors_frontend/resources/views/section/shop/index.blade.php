@include('layouts.header')
@include('layouts.menu')

<body>

   
    <div class="container-fluid">
    
        <!--<div class="top-ranking top-botiga">
            <div class="container">
                <h1>La meva cistella</h1>
            </div>
        </div>-->
            
        
        <!-- BOTIGA--> 
           <section class="serveis botiga"> 
            <div class="container"> 

                <div class="cap-asso hidden-xs">
                    <div class="cap-asso-tit">
                        <p>Benvinguts a La Botiga de Corredors.cat!</p>
                        <p class="subtit-botiga">Desde la Botiga podeu gestionar tots els pagaments relacionats amb Corredors.cat, ja sigui la compra de material esportiu de l’associació, la quota anual d’associat, els esdeveniments, així com les inscripcions a curses que estiguin disponibles.<br><br>
    Per poder comprar a La Botiga, has de ser soci de Corredors.cat. Si encara no ho ets, pots associar-te i gaudir dels avantatges mitjantçant el següent link:</p>
                       <div class="clearfix"></div>
                       <a class="btn btn-white btn-lg" href="abonat">ASSOCIA’T</a>
                    </div> 
                </div>
            </div>
            <div class="container"> 
                
                <div class="cap-cistella">
                    <a  href="botiga-cistella.php"><div class="items-cistella"><span>3</span><br>La meva cistella</div></a>
                </div>
                
                <div class="row">
                    <div class="col-sm-3">
                        <p><strong>Filtre per categories</strong></p>
                        <div class="clearfix"></div>
                        <div class="panel-group">
                            <div class="panel panel-default">
                              <div class="panel-heading">
                                <h4 class="panel-title">
                                  <a data-toggle="collapse" href="#collapse1" class="collapsed">Categoria 1</a>
                                </h4>
                              </div>
                              <div id="collapse1" class="panel-collapse collapse">
                                <div class="panel-body"><a href="javascrip:;">Subcategoria 1-1</a></div>
                                <div class="panel-body"><a href="javascrip:;">Subcategoria 1-2</a></div>
                              </div>
                            </div>
                            <div class="panel panel-default">
                              <div class="panel-heading">
                                <h4 class="panel-title">
                                  <a data-toggle="collapse" href="#collapse2" class="collapsed">Categoria 2</a>
                                </h4>
                              </div>
                              <div id="collapse2" class="panel-collapse collapse">
                                <div class="panel-body"><a href="javascrip:;">Subcategoria 2-1</a></div>
                                <div class="panel-body"><a href="javascrip:;">Subcategoria 2-2</a></div>
                                <div class="panel-body"><a href="javascrip:;">Subcategoria 3-2</a></div>
                              </div>
                            </div>
                            <div class="panel panel-default">
                              <div class="panel-heading">
                                <h4 class="panel-title">
                                  <a data-toggle="collapse" href="#collapse3" class="collapsed">Categoria 3</a>
                                </h4>
                              </div>
                              <div id="collapse3" class="panel-collapse collapse">
                                <div class="panel-body"><a href="javascrip:;">Subcategoria 3-1</a></div>
                                <div class="panel-body"><a href="javascrip:;">Subcategoria 3-2</a></div>
                              </div>
                            </div>
                          </div>
                        
                    </div>
                    <div class="col-sm-9">
                        <p>
                            <a class="filtre-prods" href="javascript:;">Popularitat <span class="caret"></span></a>
                            <a class="filtre-prods" href="javascript:;">Novetat <span class="caret"></span></a>
                            <a class="filtre-prods" href="javascript:;">Preu <span class="caret"></span></a>
                            <div class="clearfix"></div>
                        </p>
                        <div class="clearfix"></div>
                        <div class="row">
                            <!--BLOC PRODUCTE  -->
                            <div class="col-md-4 col-sm-6">
                                <div class="bloc-prod">
                                    <div class="imat-prod" style="background-image:url(img/prod1.jpg)"><a href="javascript:;" data-toggle="modal" data-target="#myProduct"><img src="img/tr-q.png" width="100%" height="auto"></a></div>
                                    <div class="producte">
                                        <h1><div>Nom del producte</div></h1>
                                        <table width="100%" border="0">
                                          <tr>
                                            <td class="prod-preu">100€</td>
                                            <td class="prod-dispon">A partir del:<br><span>13/09/15</span></td>
                                            <td align="right"><a href="javascript:;"><img src="img/icon-cistellaB.png"></a></td>
                                          </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <!--//BLOC PRODUCTE  -->
                            <!--BLOC PRODUCTE  -->
                            <div class="col-md-4 col-sm-6">
                                <div class="bloc-prod">
                                    <div class="imat-prod" style="background-image:url(img/prod6.jpg)"><a href="javascript:;" data-toggle="modal" data-target="#myProduct"><img src="img/tr-q.png" width="100%" height="auto"></a></div>
                                    <div class="producte">
                                        <h1><div>Nom del producte nom del producte</div></h1>
                                        <table width="100%" border="0">
                                          <tr>
                                            <td class="prod-preu">100€</td>
                                            <td class="prod-dispon">A partir del:<br><span>13/09/15</span></td>
                                            <td align="right"><a href="javascript:;"><img src="img/icon-cistellaB.png"></a></td>
                                          </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <!--//BLOC PRODUCTE  -->
                            <!--BLOC PRODUCTE  -->
                            <div class="col-md-4 col-sm-6">
                                <div class="bloc-prod">
                                    <div class="imat-prod" style="background-image:url(img/prod7.jpg)"><a href="javascript:;" data-toggle="modal" data-target="#myProduct"><img src="img/tr-q.png" width="100%" height="auto"></a></div>
                                    <div class="producte">
                                        <h1><div>Nom del producte</div></h1>
                                        <table width="100%" border="0">
                                          <tr>
                                            <td class="prod-preu">100€</td>
                                            <td class="prod-dispon2">Disponible</td>
                                            <td align="right"><a href="javascript:;"><img src="img/icon-cistellaB.png"></a></td>
                                          </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <!--//BLOC PRODUCTE  -->
                            <!--BLOC PRODUCTE  -->
                            <div class="col-md-4 col-sm-6">
                                <div class="bloc-prod">
                                    <div class="imat-prod" style="background-image:url(img/prod3.jpg)"><a href="javascript:;" data-toggle="modal" data-target="#myProduct"><img src="img/tr-q.png" width="100%" height="auto"></a></div>
                                    <div class="producte">
                                        <h1><div>Nom del producte</div></h1>
                                        <table width="100%" border="0">
                                          <tr>
                                            <td class="prod-preu">100€</td>
                                            <td class="prod-dispon">A partir del:<br><span>13/09/15</span></td>
                                            <td align="right"><a href="javascript:;"><img src="img/icon-cistellaB.png"></a></td>
                                          </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <!--//BLOC PRODUCTE  -->
                            <!--BLOC PRODUCTE  -->
                            <div class="col-md-4 col-sm-6">
                                <div class="bloc-prod">
                                    <div class="imat-prod" style="background-image:url(img/prod1.jpg)"><a href="javascript:;" data-toggle="modal" data-target="#myProduct"><img src="img/tr-q.png" width="100%" height="auto"></a></div>
                                    <div class="producte">
                                        <h1><div>Nom del producte</div></h1>
                                        <table width="100%" border="0">
                                          <tr>
                                            <td class="prod-preu">100€</td>
                                            <td class="prod-dispon">A partir del:<br><span>13/09/15</span></td>
                                            <td align="right"><a href="javascript:;"><img src="img/icon-cistellaB.png"></a></td>
                                          </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <!--//BLOC PRODUCTE  -->
                            <!--BLOC PRODUCTE  -->
                            <div class="col-md-4 col-sm-6">
                                <div class="bloc-prod">
                                    <div class="imat-prod" style="background-image:url(img/prod5.jpg)"><a href="javascript:;" data-toggle="modal" data-target="#myProduct"><img src="img/tr-q.png" width="100%" height="auto"></a></div>
                                    <div class="producte">
                                        <h1><div>Nom del producte nom del producte</div></h1>
                                        <table width="100%" border="0">
                                          <tr>
                                            <td class="prod-preu">100€</td>
                                            <td class="prod-dispon2">Disponible</td>
                                            <td align="right"><a href="javascript:;"><img src="img/icon-cistellaB.png"></a></td>
                                          </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <!--//BLOC PRODUCTE  -->
                            <!--BLOC PRODUCTE  -->
                            <div class="col-md-4 col-sm-6">
                                <div class="bloc-prod">
                                    <div class="imat-prod" style="background-image:url(img/prod4.jpg)"><a href="javascript:;" data-toggle="modal" data-target="#myProduct"><img src="img/tr-q.png" width="100%" height="auto"></a></div>
                                    <div class="producte">
                                        <h1><div>Nom del producte</div></h1>
                                        <table width="100%" border="0">
                                          <tr>
                                            <td class="prod-preu">100€</td>
                                            <td class="prod-dispon">A partir del:<br><span>13/09/15</span></td>
                                            <td align="right"><a href="javascript:;"><img src="img/icon-cistellaB.png"></a></td>
                                          </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <!--//BLOC PRODUCTE  -->
                            <!--BLOC PRODUCTE  -->
                            <div class="col-md-4 col-sm-6">
                                <div class="bloc-prod">
                                    <div class="imat-prod" style="background-image:url(img/prod2.jpg)"><a href="javascript:;" data-toggle="modal" data-target="#myProduct"><img src="img/tr-q.png" width="100%" height="auto"></a></div>
                                    <div class="producte">
                                        <h1><div>Nom del producte</div></h1>
                                        <table width="100%" border="0">
                                          <tr>
                                            <td class="prod-preu">100€</td>
                                            <td class="prod-dispon">A partir del:<br><span>13/09/15</span></td>
                                            <td align="right"><a href="javascript:;"><img src="img/icon-cistellaB.png"></a></td>
                                          </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <!--//BLOC PRODUCTE  -->
                            <!--BLOC PRODUCTE  -->
                            <div class="col-md-4 col-sm-6">
                                <div class="bloc-prod">
                                    <div class="imat-prod" style="background-image:url(img/prod3.jpg)"><a href="javascript:;" data-toggle="modal" data-target="#myProduct"><img src="img/tr-q.png" width="100%" height="auto"></a></div>
                                    <div class="producte">
                                        <h1><div>Nom del producte</div></h1>
                                        <table width="100%" border="0">
                                          <tr>
                                            <td class="prod-preu">100€</td>
                                            <td class="prod-dispon2">Disponible</td>
                                            <td align="right"><a href="javascript:;"><img src="img/icon-cistellaB.png"></a></td>
                                          </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <!--//BLOC PRODUCTE  -->
                            
                            <div class="clearfix"></div>
                            
                            <div class="pagines">
                                <div class="pagines-dins">
                                    <a href="javascript:;"><i class="fa fa-chevron-left"></i></a>
                                    <strong>Notícies:</strong> Pàgina <span class="tar">1 de 3</span>
                                    <a href="javascript:;"><i class="fa fa-chevron-right"></i></a>
                                </div>
                            </div>
                            
                        </div><!--//row  -->
                    </div><!--//col-sm-9 productes  -->
                </div><!--//row  -->
            </div><!-- //container -->
        </section>
        <!-- //BOTIGA-->
      
    </div>

@include('layouts.footer')     
    
    
    
       <!-- Modal 2 politica de privacitat -->
    <div class="modal fade" id="myProduct" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog myProduct" role="document">
        <div class="modal-content">
          <div class="modal-header" style="padding-top:15px">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="clearfix"></div>
          <div class="modal-body">
          
              <form class="form-associat">
                    <div class="row">
                        <div class="col-sm-7" id="sliderSortejos">
                            <ul class="bxslider4" style="height:auto">
                                <li style="background:url(img/prod7.jpg) no-repeat center center; background-size:contain"><img src="img/tr-q.png" width="100%" height="auto"></li>
                                <li style="background:url(img/prod7-2.jpg) no-repeat center center; background-size:contain"><img src="img/tr-q.png" width="100%" height="auto"></li>
                                <li style="background:url(img/prod7-3.jpg) no-repeat center center; background-size:contain"><img src="img/tr-q.png" width="100%" height="auto"></li>
                                <li style="background:url(img/prod7-4.jpg) no-repeat center center; background-size:contain"><img src="img/tr-q.png" width="100%" height="auto"></li>
                            </ul>
                            <div class="clearfix"></div>
                            <div id="bx-pager">
                              <a data-slide-index="0" href="javascript:;" style="background:url(img/prod7.jpg) no-repeat center center; background-size:contain"><img src="img/tr-q.png"></a>
                              <a data-slide-index="1" href="javascript:;" style="background:url(img/prod7-2.jpg) no-repeat center center;background-size:contain"><img src="img/tr-q.png"></a>
                              <a data-slide-index="2" href="javascript:;" style="background:url(img/prod7-3.jpg) no-repeat center center;background-size:contain"><img src="img/tr-q.png"></a>
                              <a data-slide-index="3" href="javascript:;" style="background:url(img/prod7-4.jpg) no-repeat center center;background-size:contain"><img src="img/tr-q.png"></a>
                            </div>
                             <div class="clearfix"></div>
                        </div>
                        <div class="col-sm-5">
                            <p class="tit-preu-modal">100€</p>
                            <div class="tit-prova2">Nom del Producte</div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis </p>
                            
                            <label>Nickname</label>
                            <input name="" type="text" class="form-control" value="El teu Nickname">
                            
                           <!-- <label>Opcions del Producte</label><br>
                            <div class="select-style" style="margin-top:5px">
                                <select class="form-control">
                                  <option selected="selected" disabled="disabled">Selecciona color</option>
                                  <option>Color 1</option>
                                  <option>Color 2</option>
                                  <option>Color 3</option>
                                  <option>Color 4</option>
                                </select>
                            </div>-->
                            <div class="clearfix"></div>
                            <label>Talla/Número</label><br>
                            <div class="select-style" style="margin-top:5px">
                                <select class="form-control">
                                  <option selected="selected" disabled="disabled">Selecciona talla/núm.</option>
                                  <option>36</option>
                                  <option>37</option>
                                  <option>38</option>
                                  <option>39</option>
                                  <option>40</option>
                                  <option>41</option>
                                  <option>42</option>
                                  <option>43</option>
                                  <option>44</option>
                                </select>
                            </div>
                            <div class="clearfix"></div>
                            <label>Quantitat</label><br>
                            <div class="row">
                                <div class="col-xs-2 mes-menys text-right"><a href="javascript:;"><span class="glyphicon glyphicon-minus-sign tar" aria-hidden="true"></span></a></div>
                                <div class="col-xs-4"><input name="" type="text" class="form-control text-center" value="x1"></div>
                                <div class="col-xs-2 mes-menys text-left"><a href="javascript:;"><span class="glyphicon glyphicon-plus-sign tar" aria-hidden="true"></span></a></div>
                            </div>
                            
                            <button type="submit" class="btn btn-default btn-lg  marbot30"  data-dismiss="modal" style="width:100%">Afegir a la cistella</button>
                            
                        </div>
                        <div class="col-sm-12  text-justify marbot30">
                          <h2>Informació</h2>
                          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error.</p>
                        
                          <h2>Disponibilitat del material </h2>
                          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                            <ol>
                                <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit</li>
                                <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit</li>
                                <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit</li>
                                <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit</li>
                            </ol>
                        </div>

                    </div>
                    
                    <a href="javascript:;" class="inici_sessio padesp text-uppercase pull-right"  data-dismiss="modal">Torna enrera</a>
                    
                    <!--<button type="button" class="btn tancar_modal  btn-lg pull-left" data-dismiss="modal">Tancar</button>  -->    
                </form>         

          </div>
        </div>
      </div>
      
   
    </div> 
    <!-- //Modal -->
    
      <script>  
    
     $(document).ready(function() { 
         bxslider4= $('.bxslider4').bxSlider({
              //mode: 'horizontal',
              //slideWidth: 247,
              //infiniteLoop: true,
              pager: true,
              controls: true,
              auto:true,
              pause:4000,
              speed:1000,
              minSlides:1,
              maxSlides:1,
              moveSlides:1,
              responsive:true,
              pagerCustom: '#bx-pager'
            }); 
    
        $('.imat-prod a').click(function(){
            setTimeout(function(){
               bxslider4.reloadSlider();
            },500);
        });
    
    
    }); 
    </script> 
       
    