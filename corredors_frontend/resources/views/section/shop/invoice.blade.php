@include('layouts.header')
@include('layouts.menu')

<body>
     <div class="container-fluid">
    
        <div class="top-ranking top-pagament">
            <div class="container">
                <h1>Pagament</h1>
            </div>
        </div> 
            
        
        <!-- RESUM CISTELLA--> 
           <section class="serveis cistella"> 
            <div class="container padbot30"> 
            
            <h2>Resum de compra</h2>

                <div class="taula taula-cistella resum">
                      <table width="100%" border="0" style="min-width:500px"  class="taula-cistella">
                         <thead>
                          <tr>
                            <th width="30%" align="left" style="text-align:left">Productes</th>
                            <th width="15%" align="center">Preu / u.</th>
                            <th width="10%" align="center">Quantitat</th>
                            <th width="15%" align="center">Suma producte</th>
                            <th width="30%" align="right">Recollida de reserves</th>
                          </tr>
                          </thead>
                      <tbody>
                      <tr>
                        <td width="30%" align="left">Nom del producte</td>
                        <td width="15%" align="center">10,00€</td>
                        <td width="10%" align="center">x2</td>
                        <td width="15%" align="center">20,00€</td>
                        <td width="30%" align="right">&nbsp;</td>
                      </tr>
                       <tr>
                        <td width="30%" align="left">Nom del producte</td>
                        <td width="15%" align="center">10,00€</td>
                        <td width="10%" align="center">x2</td>
                        <td width="15%" align="center">20,00€</td>
                        <td width="30%" align="right">&nbsp;</td>
                      </tr>
                       <tr>
                        <td width="30%" align="left">Nom del producte</td>
                        <td width="15%" align="center">10,00€</td>
                        <td width="10%" align="center">x2</td>
                        <td width="15%" align="center">20,00€</td>
                        <td width="30%" align="right"> <span class="vermell">A partir del 13/09/15</span></td>
                      </tr>
                      <tr>
                        <td width="30%" align="left">Nom del producte</td>
                        <td width="15%" align="center">10,00€</td>
                        <td width="10%" align="center">x2</td>
                        <td width="15%" align="center">20,00€</td>
                        <td width="30%" align="right">&nbsp;</td>
                      </tr>
                      </tbody>
                      <tfoot> 
                                                <tr>
                            <th colspan="5" align="right" class="bottom-taula-prod">
                                    <div class="col-md-10 col-sm-9 col-xs-8 blauverd">Import total<br><span>(IVA inclòs)</span></div>
                                    <div class="col-md-2 col-sm-3 col-xs-4">80,00€</div>
                            </th>
                        </tr>

                      </tfoot>
                    </table>

                </div> <!-- //taula TOT-->
                
                
               <a href="botiga-pagament.php"><button type="button" class="btn btn-default btn-lg  pull-right" style="margin-bottom:20px">Efectuar pagament</button></a>
                <a href="botiga-cistella.php" class="inici_sessio padesp text-uppercase pull-right">Tornar Enrera</a>
                
            </div><!-- //container -->
        </section>
        <!-- // RESUM CISTELLA-->
     
    </div>

@include('layouts.footer')     
    