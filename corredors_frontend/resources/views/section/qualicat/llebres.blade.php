@include('layouts.header')
@include('layouts.menu')

<body>
  
    
    <div class="container-fluid">
    
        <div class="top-ranking top-llebres">
            <div class="container">
                <h1>Servei de llebres </h1>
            </div>
        </div>           
        
        <!-- LLEBRES--> 
        <section class="serveis"> 
            <div class="container"> 
            
                <h1 class="titular-form marbot45 martop45">Servei de Llebres a Corredors.cat</h1>
                <p>L’associació esportiva <strong>Corredors.Cat</strong> ofereix el servei de llebres als organitzadors de curses populars. Lo que en un principi va néixer com un servei ofert a curses organitzades per Coredors.cat i veient la demanda d’altres organitzadors, vàrem decidir des de l’associació formalitzar aquest servei. Aquest servei està coordinat per una comissió de 3 persones que rep les peticions dels administradors e intenta buscar entre els associats les persones més adients al ritme i el circuit.</p>
                <p><strong>Corredors.Cat</strong> ofereix als organitzadors de curses populars diferents temps objectius depenent de la distancia de la cursa (5Kms, 10 Kms, Mitja Marató, Maratest i Marató) i les necessitats de l’organitzador. Aquest servei es un servei prestat pels propis associats de l’associació sense cap remuneració excepte de la inscripció a la cursa per part de l’organitzador de la mateixa. Corredors.cat si que carrega un preu pel servei/llebre depenent de la distancia de la cursa. Aquest diners es reverteixen en benefici per als associats en forma de descompte per la cursa (TPV intern per a associats o regal de inscripcions a les curses).</p>
                <p>Algunes de les curses on hem realitzat aquest servei han sigut grans curses com Nassos, la Mercè , Maratest, Mitja de Terrassa, però també en altres mes populars com Cursa de Poble Nou, el Papiol i d’altres.</p>

                <p>&nbsp;</p>

            </div><!-- //container -->
        </section>      
        
        <!-- llebres TEMPS OBJECTIU-->
        <div class="bgblau ">
            <section class="serveis-llebres"> 
                <div class="container"> 
                    <div class="temps-objectiu">
                        <div class="tipus-entreno llebres llebre1">
                            <div class="tipus-llebre-tit">5 Kms</div>
                            <div class="tipus-entreno-data blauverd">Des de Sub 18’ a sub 30’</div>
                        </div>
                        <div class="tipus-entreno llebres llebre2">
                            <div class="tipus-llebre-tit">10 Kms</div>
                            <div class="tipus-entreno-data blauverd">Des de Sub 35’ a sub 65’</div>
                        </div>
                        <div class="tipus-entreno llebres llebre3">
                            <div class="tipus-llebre-tit">Mitja Marató </div>
                            <div class="tipus-entreno-data blauverd">Des de Sub 1:20h a sub 2h</div>
                        </div>
                        <div class="tipus-entreno llebres llebre4">
                            <div class="tipus-llebre-tit">Maratest (30km) </div>
                            <div class="tipus-entreno-data blauverd">Des de Sub 2:45h a sub 4h temps marató</div>
                        </div>
                        <div class="tipus-entreno llebres llebre5">
                            <div class="tipus-llebre-tit">Marató</div>
                            <div class="tipus-entreno-data blauverd">Des de Sub 2:45h a sub 4h</div>
                        </div>
                    </div>
                </div>
            </section>
        </div> 
        <!-- //llebres TEMPS OBJECTIU-->       
         
         
         
         <div class="bgblau2">
            <section class="entrena-inf"> 
                <div class="container text-center"> 
                    <p><span>I ara què?</span></p>
                    <p>Si estàs interessat en el serveis de llebres de Corredors.cat o tens algun tipus de consulta que fer-nos, no dubtis en contactar amb nosaltres:</p>
                    <p><a href="mailto:llebres@corredors.cat" class="tar3">llebres@corredors.cat</a></p>
                </div>
            </section>
        </div>
        
        
        <!-- llebres CURSES REALITZADES gràfica-->
        <div class="bgblau ">
            <section class="serveis-llebres"> 
                <div class="container"> 
                    <div class="entrenador grafica">
                        <h1 class="titular-form marbot45">Curses realitzades</h1>
                        <div class="taula">
                            <!-- gràfica (valors a la funció)-->
                            <div id="chart-container"></div>
                            <!-- //gràfica-->
                        </div>
                    </div>  
                </div>
            </section>
        </div> 
        <!-- //llebres CURSES REALITZADES-->     
                
                
                
                
                
     
    </div>

    
    <script src="{{ asset('js/fusioncharts.js') }}"></script>
    <script src="{{ asset('js/fusioncharts.charts.js') }}"></script>
    
    <script>
   FusionCharts.ready(function () {
    var revenueChart = new FusionCharts({
        type: 'mscolumn2d',
        renderAt: 'chart-container',
        width: '100%',
        height: '440',
        dataFormat: 'json',
        dataSource: {
            "chart": {
               // "caption": "Comparison of Quarterly Revenue",
                "xAxisname": "Distància cursa",
                //"yAxisName": "Núm. curses",
               // "numberPrefix": "n.",
                "plotFillAlpha" : "80",
                "labelFontColor": "333333",
                "labelFontSize": "15",

                //Cosmetics
                "paletteColors" : "#86c06a,#af60a2,#5972b6",
                "baseFontColor" : "#339999",
                "baseFont" : "'Open Sans', sans-serif",
                "captionFontSize" : "16",
                "subcaptionFontSize" : "16",
                "subcaptionFontBold" : "0",
                "showBorder" : "0",
                "bgColor" : "#ffffff",
                "showShadow" : "0",
                "canvasBgColor" : "#e9f7f7",
                "canvasBorderAlpha" : "0",
                "divlineAlpha" : "0",
                "divlineColor" : "#999999",
                "divlineThickness" : "1",
                "divLineIsDashed" : "1",
                "divLineDashLen" : "1",
                "divLineGapLen" : "1",
                "usePlotGradientColor" : "0",
                "showplotborder" : "0",
                "valueFontColor" : "#ffffff",
                "placeValuesInside" : "0",
                "rotateValues": "0",
                "valueFontColor": "#339999",
                "valueFontSize": "12",
                "valueFontBold": "1",
                "valueFontItalic": "0",
                "showHoverEffect" : "1",
                "showXAxisLine" : "1",
                "xAxisLineThickness" : "1",
                "xAxisLineColor" : "#999999",
                "alternateHGridColor" : "#ffffff",
                "showAlternateHGridColor" : "1",
                "alternateHGridAlpha" : "100",
                "numdivlines": "8",
                "legendBgAlpha" : "0",
                "legendBorderAlpha" : "0",
                "legendShadow" : "0",
                "legendItemFontSize" : "15",
                "legendItemFontColor" : "#666666",
                "legendPosition": "bottom"
            },
            "categories": [
                {
                    "category": [
                        { "label": "5km" },
                        { "label": "10km" },
                        { "label": "Mitja M." },
                        { "label": "Maratest" },
                        { "label": "Marató" },
                        { "label": "Grand Total" }
                    ]
                }
            ],
            "dataset": [
                {
                    "seriesname": "2014",
                    "data": [
                        { "value": "2" }, 
                        { "value": "15" }, 
                        { "value": "3" }, 
                        { "value": "0" }, 
                        { "value": "1" }, 
                        { "value": "21" }
                    ]
                }, 
                {
                    "seriesname": "2015",
                    "data": [
                        { "value": "4" }, 
                        { "value": "18" }, 
                        { "value": "6" }, 
                        { "value": "2" }, 
                        { "value": "5" }, 
                        { "value": "35" }
                    
                    ]
                }, 
                {
                    "seriesname": "2016",
                    "data": [
                        { "value": "3" }, 
                        { "value": "12" }, 
                        { "value": "2" }, 
                        { "value": "4" }, 
                        { "value": "10" }, 
                        { "value": "31" }
                    ]
                }
            ]

           
        }
    });
    
    revenueChart.render();
});

  
  </script>
        
    
@include('layouts.footer')     
