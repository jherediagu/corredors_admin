@include('layouts.header')
@include('layouts.menu')

<body>

    
    <div class="container-fluid">
    
      <div class="top-ranking top-qualicat">
            <div class="container">
              <h1>Benvinguts a Qualicat</h1>
            </div>
        </div>           
        
        <!-- QUALICAT CERCADOR--> 
        <section class="qualicat"> 
            <div class="container"> 
                <h1 class="titular-form cerca-curses"><div><img src="img/icon-lupa-blanc.png" alt="cercador"> Cercador de curses</div></h1>
                <form>
                  <div class="row">
                      <div class="col-sm-12">
                          <div class="cerca-form cerc1">
                               <label>Nom de la cursa</label>
                                 <div class="select-style">
                                    <select class="form-control">
                                      <option selected="selected" disabled="disabled">Selecciona una cursa</option>
                                      <option>Nom de la cursa 1</option>
                                      <option>Nom de la cursa 2</option>
                                      <option>Nom de la cursa 3</option>
                                      <option>Nom de la cursa 4</option>
                                      <option>Nom de la cursa 5</option>
                                      <option>Nom de la cursa 6</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                          <div class="cerca-form cerc2">
                              <label>Campionat / agrupació / Lliga</label>
                                 <div class="select-style">
                                    <select class="form-control">
                                      <option selected="selected" disabled="disabled">Selecciona una cursa</option>
                                      <option>Nom de la cursa 1</option>
                                      <option>Nom de la cursa 2</option>
                                      <option>Nom de la cursa 3</option>
                                      <option>Nom de la cursa 4</option>
                                      <option>Nom de la cursa 5</option>
                                      <option>Nom de la cursa 6</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                          <div class="cerca-form cerc3">
                              <label>Tipus de cursa</label>
                                 <div class="select-style">
                                     <select class="form-control">
                                      <option selected="selected" disabled="disabled">Selecciona una modalitat</option>
                                      <option>Nom de la modalitat 1</option>
                                      <option>Nom de la modalitat 2</option>
                                      <option>Nom de la modalitat 3</option>
                                      <option>Nom de la modalitat 4</option>
                                      <option>Nom de la modalitat 5</option>
                                      <option>Nom de la modalitat 6</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                          <div class="cerca-form cerc4">
                              <label>On?</label>
                                 <div class="select-style">
                                    <select class="form-control">
                                      <option selected="selected" disabled="disabled">Selecciona una localitat</option>
                                      <option>Nom de la Localitat 1</option>
                                      <option>Nom de la Localitat 2</option>
                                      <option>Nom de la Localitat 3</option>
                                      <option>Nom de la Localitat 4</option>
                                      <option>Nom de la Localitat 5</option>
                                      <option>Nom de la Localitat 6</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                          <div class="cerca-form cerc5">
                              <label>Quan?</label>
                                <input name="" type="text"  class="form-control" placeholder="dd/mm/aaaa">
                            </div>
                        </div>
                        
                        <div class="col-lg-12">
                          <button type="submit" class="btn  btn-default pull-right btn-lg">Cercar</button>
                      <!--<a href="javascript:;" class="inici_sessio padesp text-uppercase pull-right">Nova cerca</a>-->
                        </div>
                        
                        
                     </div>
                </form>
            </div><!-- //container -->
        </section>   
           
        <!-- QUALICAT RESULTATS--> 
         <div class="bgblau">
          <section class="qualicat curses"> 
                <div class="container"> 
                  <h1 class="titular-form marbot30">Resultats de la recerca: </h1>
                    <div class="row">
                      <div class="col-md-3 col-sm-4">
                          <a href="serveis-qualicat-enquesta.php">
                                <div class="imat-noti" style="background-image:url(img/cursa.jpg)">
                                    <img src="img/tr-q.png" class="imat-cursa">
                                    <div class="peu_foto"><div>Nom de la cursa</div></div>
                                    <div class="logo-qualicat"><img src="img/logo-qualicat.png" width="104" height="auto"></div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3 col-sm-4">
                          <a href="serveis-qualicat-enquesta.php">
                                <div class="imat-noti" style="background-image:url(img/cursa.jpg)">
                                    <img src="img/tr-q.png" class="imat-cursa">
                                    <div class="peu_foto"><div>Nom de la cursa</div></div>
                                    <div class="logo-qualicat"><img src="img/logo-qualicat.png" width="104" height="auto"></div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3 col-sm-4">
                          <a href="serveis-qualicat-enquesta.php">
                                <div class="imat-noti" style="background-image:url(img/cursa.jpg)">
                                    <img src="img/tr-q.png" class="imat-cursa">
                                    <div class="peu_foto"><div>Nom de la cursa</div></div>
                                    <div class="logo-qualicat"><img src="img/logo-qualicat.png" width="104" height="auto"></div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3 col-sm-4">
                          <a href="serveis-qualicat-enquesta.php">
                                <div class="imat-noti" style="background-image:url(img/cursa.jpg)">
                                    <img src="img/tr-q.png" class="imat-cursa">
                                    <div class="peu_foto"><div>Nom de la cursa nom de la cursa</div></div>
                                    <div class="logo-qualicat"><img src="img/logo-qualicat.png" width="104" height="auto"></div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3 col-sm-4">
                          <a href="serveis-qualicat-enquesta.php">
                                <div class="imat-noti" style="background-image:url(img/cursa.jpg)">
                                    <img src="img/tr-q.png" class="imat-cursa">
                                    <div class="peu_foto"><div>Nom de la cursa</div></div>
                                    <div class="logo-qualicat"><img src="img/logo-qualicat.png" width="104" height="auto"></div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3 col-sm-4">
                          <a href="serveis-qualicat-enquesta.php">
                                <div class="imat-noti" style="background-image:url(img/cursa.jpg)">
                                    <img src="img/tr-q.png" class="imat-cursa">
                                    <div class="peu_foto"><div>Nom de la cursa</div></div>
                                    <div class="logo-qualicat"><img src="img/logo-qualicat.png" width="104" height="auto"></div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3 col-sm-4">
                          <a href="serveis-qualicat-enquesta.php">
                                <div class="imat-noti" style="background-image:url(img/cursa.jpg)">
                                    <img src="img/tr-q.png" class="imat-cursa">
                                    <div class="peu_foto"><div>Nom de la cursa</div></div>
                                    <div class="logo-qualicat"><img src="img/logo-qualicat.png" width="104" height="auto"></div>
                                </div>
                            </a>
                        </div>

                    
                    </div>
                    <a href="javascript:;" class="text-uppercase"><strong>+ carregar més resultats</strong></a>
                </div><!-- //container -->
            </section>
        </div>
        
        <!-- QUALICAT VALORACIONS--> 
        <section class="qualicat valora"> 
            <div class="container"> 
                <h1 class="titular-form">Les curses més valorades</h1>
                <div class="taula">
                <table width="100%" border="0">
                  <tr>
                    <td class="valor">9,8</td>
                    <td class="valor-cursa">XXVII Cursa de Muntanya pícnic Sant Antoni<br>
                    <span>Cursa de muntanya | 10000  |  Barcelona</span></td>
                    <td class="valor-pastanaga"><img src="img/icon-pastanaga.png"><img src="img/icon-pastanaga.png"><img src="img/icon-pastanaga.png"><img src="img/icon-pastanaga.png"><span><img src="img/icon-pastanaga.png"></span></td>
                    <td><a href="serveis-qualicat-enquesta.php" class="text-uppercase"><strong class="semibold">valorar cursa</strong></a></td>
                  </tr>
                  <tr>
                    <td class="valor">9,0</td>
                    <td class="valor-cursa">XXVII Cursa de Muntanya pícnic Sant Antoni<br>
                    <span>Cursa de muntanya | 10000  |  Barcelona</span></td>
                    <td class="valor-pastanaga"><img src="img/icon-pastanaga.png"><img src="img/icon-pastanaga.png"><img src="img/icon-pastanaga.png"><span><img src="img/icon-pastanaga.png"></span><span><img src="img/icon-pastanaga.png"></span></td>
                    <td><a href="serveis-qualicat-enquesta.php" class="text-uppercase"><strong class="semibold">valorar cursa</strong></a></td>
                  </tr>
                  <tr>
                    <td class="valor">8,1</td>
                    <td class="valor-cursa">XXVII Cursa de Muntanya pícnic Sant Antoni<br>
                    <span>Cursa de muntanya | 10000  |  Barcelona</span></td>
                    <td class="valor-pastanaga"><img src="img/icon-pastanaga.png"><img src="img/icon-pastanaga.png"><span><img src="img/icon-pastanaga.png"></span><span><img src="img/icon-pastanaga.png"></span><span><img src="img/icon-pastanaga.png"></span></td>
                    <td><a href="serveis-qualicat-enquesta.php" class="text-uppercase"><strong class="semibold">valorar cursa</strong></a></td>
                  </tr>
                  <tr>
                    <td class="valor">7,5</td>
                    <td class="valor-cursa">XXVII Cursa de Muntanya pícnic Sant Antoni<br>
                    <span>Cursa de muntanya | 10000  |  Barcelona</span></td>
                    <td class="valor-pastanaga"><img src="img/icon-pastanaga.png"><span><img src="img/icon-pastanaga.png"></span><span><img src="img/icon-pastanaga.png"></span><span><img src="img/icon-pastanaga.png"></span><span><img src="img/icon-pastanaga.png"></span></td>
                    <td><a href="serveis-qualicat-enquesta.php" class="text-uppercase"><strong class="semibold">valorar cursa</strong></a></td>
                  </tr>
                  <tr>
                    <td class="valor">7,0</td>
                    <td class="valor-cursa">XXVII Cursa de Muntanya pícnic Sant Antoni<br>
                    <span>Cursa de muntanya | 10000  |  Barcelona</span></td>
                    <td class="valor-pastanaga"><span><img src="img/icon-pastanaga.png"></span><span><img src="img/icon-pastanaga.png"></span><span><img src="img/icon-pastanaga.png"></span><span><img src="img/icon-pastanaga.png"></span><span><img src="img/icon-pastanaga.png"></span></td>
                    <td><a href="serveis-qualicat-enquesta.php" class="text-uppercase"><strong class="semibold">valorar cursa</strong></a></td>
                  </tr>
                </table>
                </div>
                <a href="javascript:;" class="text-uppercase carrega-valoracions"><strong class="semibold">+ carregar més valoracions</strong></a>
                
            </div><!-- //container -->
        </section>  
        
        
    </div>
    
@include('layouts.footer')     
