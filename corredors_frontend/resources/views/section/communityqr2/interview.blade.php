@include('layouts.header')
@include('layouts.menu')

<body>
    
       <!-- container fluid-->
    <div class="container-fluid bgblack">
        <div class="container">
          <div class="pull-right">
              <audio id="player2" src="media/Weinland_-_Autumn_Blood_Instrumental.mp3" type="audio/mp3" controls></audio>
            </div>
        </div>
    </div>
   
  <!-- container fluid-->
    <div class="container-fluid bgtar2">
        <div class="container">
          <div class="entrevista">
              <form>
                    <div class="col-md-8 col-md-offset-4">
                        <div class="row">
                            <div class="hidden-xs col-sm-2">
                                <img src="{{ asset('img/lupa.png') }}" width="26" class="pull-right" style="margin: 7px 0 0 0;">
                            </div>
                            <div class="col-sm-5">
                                <div class="select-style pull-right" style="width:100%; background-color:#ffb84d;margin-left:15px">
                                    <select class="form-control pull-right">
                                      <option selected="selected" disabled="disabled">Data de publicació</option>
                                      <option>Nom de l'entrevistat 1</option>
                                      <option>Nom de l'entrevistat 2</option>
                                      <option>Nom de l'entrevistat 3</option>
                                      <option>Nom de l'entrevistat 4</option>
                                      <option>Nom de l'entrevistat 5</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-5">
                                <div class="select-style pull-right" style="width:100%; background-color:#ffb84d;margin-left:15px">
                                    <select class="form-control pull-right">
                                      <option selected="selected" disabled="disabled">Selecciona una entrevista</option>
                                      <option>Nom de l'entrevistat 1</option>
                                      <option>Nom de l'entrevistat 2</option>
                                      <option>Nom de l'entrevistat 3</option>
                                      <option>Nom de l'entrevistat 4</option>
                                      <option>Nom de l'entrevistat 5</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        
                        
                        
                    </div>
                </form>
                <div class="clearfix"></div>
              <div class="row">
                  <div class="col-sm-4">
                      <div class="img-circle"><img src="img/entrevista-perfil.jpg" width="100%" height="auto" class="img-circle perfil_entrev"></div>
                        <p class="subjecte text-center">Sóc en...Danili</p>
                    </div>
                    
                    <div class="col-sm-8">
                      <div class="titular-prods emtrev">Entrevistem...<br><span>10/03/2016</span></div>
                        <h1 class="nomSubjecte">Daniel Justribó i Àvila</h1>
                        <p class="anysSubjecte">46 anys</p>
                        <p>“Vaig néixer a Barcelona, al barri del Clot, on hi vaig viure fins que em vaig emancipar. Molts dels meus records estan lligats en aquest barri i segueixo entrenant-me al seu parc, malgrat que des de fa tretze anys visc al Guinardó.</p>
                        <p>M’agrada llegir, escriure i conèixer. Sóc curiós de mena i em puc meravellar de petites coses que sovint passen desapercebudes. Hi ha gent que diu que sóc cerebral però per dins em bull la passió.</p>
                        <p>Vaig llicenciar-me en biologia i a hores d’ara miro de redirigir la meva carrera professional cap a la comunicació científica.”</p>
                    </div>
               </div>
            </div>
        </div>        
    </div>
    <!-- //container fluid-->
    
    <!-- entrevista-->
    <section class="questions">
      <div class="container">
            <div class="submenu_entrev text-center">
                <ul>
                    <li class="entrev1"><a href="#seccio-entrevista" class="ancla">L’entrevista</a></li>
                    <li class="entrev2"><a href="#seccio-rapides" class="ancla">Preguntes ràpides</a></li>
                    <li class="entrev3"><a href="#seccio-personals" class="ancla">Preguntes personals</a></li>
                    <li class="entrev5"><a href="#seccio-imatges" class="ancla">Imatges</a></li>
                    <li class="entrev4"><a href="#seccio-comentaris"  class="ancla">Comentaris</a></li>
                </ul>
            </div>
            <div class="clearfix"></div>
      
          <div class="row">            
                <div class="col-sm-12 marbot45"><img src="img/entrev1.jpg" width="100%" height="auto"></div>
                
                <div class="col-sm-12"><a id="seccio-entrevista"></a>
                
                    <h1 class="titular-form marbot45">L’entrevista</h1>
                    
                    <h2>Quina peça musical vols que acompanyi aquesta entrevista?</h2>
                    <p>Tinc centenars de vinils, CDs i altres formats. La música sona constantment a casa. De vegades he pensat, tot i que no n’estic segur, que preferiria quedar-me cec a ser sord, perquè sense música tindria al capdavall una ceguesa pitjor. Partint d’això, haver d’escollir una peça per a aquesta entrevista ja depèn només del dia i de com bufi el vent.</p>
                     <p> Sempre he tingut fascinació per aquells grups o solistes que el temps ha oblidat, malgrat que en el seu dia van editar obres de qualitat. Un bon exemple és el de la Karen Dalton, una índia nord-americana amb un timbre únic que va morir ja fa uns anys i de qui en els darrers s’ha recuperat el seu treball. Ella em porta records forts d’una parella amb qui compartia moments de bromes i paròdies màgiques. Passa pocs cops a la vida, i quan escolto aquesta cantant - i en especial el Something on your mind, composat per Dino Valenti  - sempre penso en aquell follet de mirada inquisidora i dentetes de conill, i somric.</p>
                     
                    <h2>Per què corres?</h2>
                     <p>Córrer em fa sentir complet. Jo, com a ment, entenc millor el meu cos, veig les seves limitacions, però també descobreixo el seu vigor. Les primeres les accepto i el segon em sorprèn encara. Córrer també em permet fruir de la llum del sol, de l’aigua de pluja i del vent d’una manera més sensual.</p>
                      
                    <h2>Pots recordar quina va ser la teva primera cursa?</h2>
                    <p>Vaig apuntar-me a la Corte Inglés de 1987 amb uns companys de COU. Aquesta va ser la oportunitat que em va obrir un camí, perquè jo abans - des dels catorze anys - m’havia limitat a córrer al meu aire els caps de setmana per una urbanització. Gairebé sense entrenar-me vaig poder fer-la a cinc minuts el quilòmetre vestit en banyador, samarreta de bàsquet, sabatilles de carrer, mitjons gruixuts i una cinta al cap. És curiós que mai no m’ha cridat gaire fer curses i encara avui les selecciono molt. En 28 anys n’hauré fetes només unes 85 i d’elles 20 maratons.</p>
                      
                    <h2>Tindràs moltes anècdotes al llarg de les teves curses. N'hi ha alguna que ens vulguis explicar?</h2>
                    <p>Potser la més dramàtica va ser quan, fa cosa de tres anys, tornava corrent del Parc del Clot i pujava pel carrer on hi visc. Era a cent metres de casa i feia molta ventada. Tanta, que un test de fang va caure d’una finestra i va petar a molt pocs centímetres davant meu. Allò contra el terra va sonar veritablement fort i el test va quedar esmicolat. Va anar de mil·lèsimes que no m’hagués pogut fer molt mal.</p>
                    <p>Aquest incident em va fer reflexionar sobre mi, el que tenia i volia a la meva vida, i va fer-me acabar de decidir per deixar una feina que no em motivava i mirar cap a una altra banda. D’aquella voluntat també va sortir el temps per poder escriure el llibre que vaig editar l’any passat - una experiència dura, però que era necessària - per donar tribut a gent que ho mereixia.</p>
                </div>
                <div class="clearfix"></div>
                
           </div> <!-- //row-->
        </div><!-- //container-->
    </section>
    <!-- //entrevista 1-->
    
  <!-- preguntes ràpides-->
    <section class="questions"><a id="seccio-rapides"></a>
        <div class="container">     
                <div class="row bgclock">
                    <div class="col-sm-4 col-xs-2 clock text-center"><img src="img/icon-entrev.png"></div>
                    <div class="col-sm-8 col-xs-10  bgblanc">
                        <div class="titular-form marbot45">Preguntes ràpides</div>
                        
                        <h2>Dolç / salat / àcid</h2>
                        <p>Associo la pluja amb el fred i acostumo a patir molt amb el dors de les mans i els avantbraços. Prefereixo el vent, si és suau, pel seu tacte, i si és fort, pel seu poder.</p>
                        
                        <h2>Mar / muntanya / asfalt</h2>
                        <p>Associo la pluja amb el fred i acostumo a patir molt amb el dors de les mans i els avantbraços. Prefereixo el vent, si és suau, pel seu tacte, i si és fort, pel seu poder.</p>
                        
                        <h2>Vent / pluja</h2>
                        <p>Associo la pluja amb el fred i acostumo a patir molt amb el dors de les mans i els avantbraços. Prefereixo el vent, si és suau, pel seu tacte, i si és fort, pel seu poder.</p>
                        
                        <h2>Fred / calor</h2>
                        <p>Associo la pluja amb el fred i acostumo a patir molt amb el dors de les mans i els avantbraços. Prefereixo el vent, si és suau, pel seu tacte, i si és fort, pel seu poder.</p>
                        
                        <h2>Amb / sense crono</h2>
                        <p>Associo la pluja amb el fred i acostumo a patir molt amb el dors de les mans i els avantbraços. Prefereixo el vent, si és suau, pel seu tacte, i si és fort, pel seu poder.</p>
                        
                        <h2>Sol / acompanyat</h2>
                        <p>Associo la pluja amb el fred i acostumo a patir molt amb el dors de les mans i els avantbraços. Prefereixo el vent, si és suau, pel seu tacte, i si és fort, pel seu poder.</p>
                        
                        <h2>A sobre / a sota</h2>
                        <p>Associo la pluja amb el fred i acostumo a patir molt amb el dors de les mans i els avantbraços. Prefereixo el vent, si és suau, pel seu tacte, i si és fort, pel seu poder.</p>

                        <h2>Aire lliure / gimnàs</h2>
                        <p>Associo la pluja amb el fred i acostumo a patir molt amb el dors de les mans i els avantbraços. Prefereixo el vent, si és suau, pel seu tacte, i si és fort, pel seu poder.</p>

                        <h2>Planificació / anarquia</h2>
                        <p>Associo la pluja amb el fred i acostumo a patir molt amb el dors de les mans i els avantbraços. Prefereixo el vent, si és suau, pel seu tacte, i si és fort, pel seu poder.</p>

                        <h2>Fermentats / destil·lat</h2>
                        <p>Associo la pluja amb el fred i acostumo a patir molt amb el dors de les mans i els avantbraços. Prefereixo el vent, si és suau, pel seu tacte, i si és fort, pel seu poder.</p>
                     </div>
                </div>
                <div class="clearfix"></div>
                
           </div> 
        </div>
    </section>
    <!-- //preguntes ràpides-->
    
    
    <!-- preguntes personals-->
    <section class="questions"><a id="seccio-personals"></a>
      <div class="container">
          <div class="row">                   
                <div class="col-sm-12">
                    <h1 class="titular-form marbot45">Preguntes personals</h1>
                    
                    <h2>Explica'ns el teu nick. Per què danili?</h2>
                    <p>"Danil" equival a Daniel des que era petit, quan un nen de l’escola que es deia Cassi Baldoví m’hi va batejar després que ens inventéssim un personatge que s’anomenava Danili Caradanutti. El meu amic era molt popular i darrere d’ell tothom em va començar a dir així. Quan ho vaig explicar a casa, el meu pare sorprès em va dir que ell també havia anat a classe amb un Cassi Baldoví. Va resultar que ambdós pares i fills vam anar a l’escola plegats en col·legis diferents. Més tard, el sobrenom el vaig utilitzar com a casteller i ara al fòrum. Hi ha més gent que em diu danili que no pas Daniel.</p>
                     <h2>Explica'ns el teu nick. Per què danili?</h2>
                    <p>"Danil" equival a Daniel des que era petit, quan un nen de l’escola que es deia Cassi Baldoví m’hi va batejar després que ens inventéssim un personatge que s’anomenava Danili Caradanutti. El meu amic era molt popular i darrere d’ell tothom em va començar a dir així. Quan ho vaig explicar a casa, el meu pare sorprès em va dir que ell també havia anat a classe amb un Cassi Baldoví. Va resultar que ambdós pares i fills vam anar a l’escola plegats en col·legis diferents. Més tard, el sobrenom el vaig utilitzar com a casteller i ara al fòrum. Hi ha més gent que em diu danili que no pas Daniel.</p>
                    <h2>Explica'ns el teu nick. Per què danili?</h2>
                    <p>"Danil" equival a Daniel des que era petit, quan un nen de l’escola que es deia Cassi Baldoví m’hi va batejar després que ens inventéssim un personatge que s’anomenava Danili Caradanutti. El meu amic era molt popular i darrere d’ell tothom em va començar a dir així. Quan ho vaig explicar a casa, el meu pare sorprès em va dir que ell també havia anat a classe amb un Cassi Baldoví. Va resultar que ambdós pares i fills vam anar a l’escola plegats en col·legis diferents. Més tard, el sobrenom el vaig utilitzar com a casteller i ara al fòrum. Hi ha més gent que em diu danili que no pas Daniel.</p>
                    <h2>Explica'ns el teu nick. Per què danili?</h2>
                    <p>"Danil" equival a Daniel des que era petit, quan un nen de l’escola que es deia Cassi Baldoví m’hi va batejar després que ens inventéssim un personatge que s’anomenava Danili Caradanutti. El meu amic era molt popular i darrere d’ell tothom em va començar a dir així. Quan ho vaig explicar a casa, el meu pare sorprès em va dir que ell també havia anat a classe amb un Cassi Baldoví. Va resultar que ambdós pares i fills vam anar a l’escola plegats en col·legis diferents. Més tard, el sobrenom el vaig utilitzar com a casteller i ara al fòrum. Hi ha més gent que em diu danili que no pas Daniel.</p>
                </div>
                <div class="clearfix"></div>
                
           </div> <!-- //row-->
        </div><!-- //container-->
    </section>
    <!-- //preguntes personals-->
    
  
    <section class="questions"><a id="seccio-imatges"></a>
      <div class="container">
          <div class="row">
              <div class="col-50-1">
                  <div class="imatges_entrev imat-vert" style="background-image:url(img/entrev2.jpg)">
                      <img src="img/img-vert.gif" width="100%" height="auto">
                        <div class="peu_foto">Aquí es mostra el nom de la foto<br>29/10/2014</div>
                    </div>
                </div>
                <div class="col-50-2">
                    <div class="imatges_entrev imat-horit" style="background-image:url(img/entrev3.jpg)">
                        <img src="img/img-horit.gif" width="100%" height="auto">
                        <div class="peu_foto">Aquí es mostra el nom de la foto<br>29/10/2014</div>
                    </div>
                    <div class="imatges_entrev imat-horit" style="background-image:url(img/entrev4.jpg)">
                        <img src="img/img-horit.gif" width="100%" height="auto">
                        <div class="peu_foto">Aquí es mostra el nom de la foto<br>29/10/2014</div>
                    </div>
                </div> 
                <div class="clearfix"></div>
            
                             
                <div class="col-sm-12"><a id="seccio-imatges"></a>
                  <h1 class="titular-form marbot45 martop45">La peça musical de l’entrevista</h1>
                    <div class="embed-responsive embed-responsive-16by9">
                      <iframe class="embed-responsive-item" width="100%" src="https://www.youtube.com/embed/3YFg5zHUdy4" frameborder="0" allowfullscreen></iframe>
                    </div>
                    
                    
                </div>
                
            </div>
        </div>
    </section>
    
    
<!-- container fluid-->
    <div class="container-fluid bgcrema">    
        <div class="container">
            <div class="row">                   
                <div class="col-sm-12 text-center">
                    Moltes gràcies<br>
                    Danili!
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>
    
    
    
    <section class="questions"><a id="seccio-comentaris"></a>
      <div class="container">
          <div class="row">                   
                <div class="col-sm-12">
                    <h1 class="titular-form marbot45">Comentaris</h1>
                    <form action="" class="comentaris">
                        <textarea name="" cols="" rows="" class="form-control caixa_coment" placeholder="Escriu un comentari"></textarea>
                        <div class="btn btn-default btn-lg pull-right marbot45">Publicar comentari</div>
                    </form>
                    
                    <!-- bloc comentari -->
                    <div class="bloc-comment">
                      <div class="row">
                          <div class="col-sm-2 col-xs-2">
                              <div class="pull-right img-circle text-center"><img src="img/perfil.jpg" width="36" height="36" class="img-circle"></div>
                            </div>
                            <div class="col-sm-10 col-xs-10">
                              <p class="tit_comment"><strong>Sonia</strong> va escriure el 4 d’Agost a les 18:45</p>
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
                              <div class="pull-left num-comment">30</div>
                                <a class="pull-left like" href=""><img src="img/icon-like.png" width="26" height="26"></a>
                                <a class="pull-left mostraresposta" href=""><img src="img/icon-responder.png" width="26" height="26"></a>
                                <div class="clearfix"></div>
                                <div class="resposta">
                                  <form action="" class="comentaris">
                                <textarea name="" cols="" rows="" class="form-control caixa_coment" placeholder="Respòn al comentari"></textarea>
                                        <div class="btn btn-default pull-right">Publicar resposta</div>
                                    </form>
                                </div>
                                
                                <!-- respostes -->
                                <div class="row">
                                    <div class="col-sm-1 col-xs-2">
                                        <div class="img-circle"><img src="img/perfil.jpg" width="36" height="36" class="img-circle"></div>
                                    </div>
                                    <div class="col-sm-11 col-xs-10">
                                        <p class="tit_comment"><strong>Sonia</strong> va escriure el 4 d’Agost a les 18:45</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                                        <div class="pull-left num-comment">30</div>
                                        <a class="pull-left like" href=""><img src="img/icon-like.png" width="26" height="26"></a>
                                    </div>
                                    <div class="clearfix"></div>
                                    
                                    <div class="col-sm-1 col-xs-2">
                                        <div class="img-circle"><img src="img/perfil.jpg" width="36" height="36" class="img-circle"></div>
                                    </div>
                                    <div class="col-sm-11 col-xs-10">
                                        <p class="tit_comment"><strong>Sonia</strong> va escriure el 4 d’Agost a les 18:45</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                                        <div class="pull-left num-comment">30</div>
                                        <a class="pull-left like" href=""><img src="img/icon-like.png" width="26" height="26"></a>
                                    </div>
                                    <div class="clearfix"></div>
                                    
                                   <!--VEURE MES RESPOSTES  , desplegar x ajax --> 
                                   <div class="col-sm-10 col-sm-offset-2 text-uppercase">
                                      <a href="javascript:;"><strong>+ Veure més respostes</strong></a>
                                   </div> 
                                   <!--//veure més respostes --> 

                                </div>
                                <!--// respostes -->
                            </div>
                        </div>
                    </div>
                    <!--// bloc comentari -->
                    
                    
                    <!-- bloc comentari -->
                    <div class="bloc-comment">
                      <div class="row">
                          <div class="col-sm-2 col-xs-2">
                              <div class="pull-right img-circle text-center"><img src="img/perfil.jpg" width="36" height="36" class="img-circle"></div>
                            </div>
                            <div class="col-sm-10 col-xs-10">
                              <p class="tit_comment"><strong>Sonia</strong> va escriure el 4 d’Agost a les 18:45</p>
                              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
                              <div class="pull-left num-comment">30</div>
                                <a class="pull-left like" href=""><img src="img/icon-like.png" width="26" height="26"></a>
                                <a class="pull-left mostraresposta" href=""><img src="img/icon-responder.png" width="26" height="26"></a>
                                <div class="clearfix"></div>
                                <div class="resposta">
                                  <form action="" class="comentaris">
                                <textarea name="" cols="" rows="" class="form-control caixa_coment" placeholder="Respòn al comentari"></textarea>
                                        <div class="btn btn-default pull-right">Publicar resposta</div>
                                    </form>
                                </div>
                                
                                <!-- respostes
                                
                                // respostes -->
                            </div>
                        </div>
                    </div>
                    <!--// bloc comentari -->
                    
                    
                    <!--VEURE MES COMENTARIS, desplegar x ajax -->
                    <div class="row marbot15">
                      <div class="col-sm-10 col-sm-offset-2 text-uppercase"><a href="javascript:;"><strong>+ Veure més comentaris</strong></a></div>  
                    </div>
                    <!--// veure més comentaris-->
                    
                    <div class="row marbot15 text-right">
                        <div class="col-sm-12 paginacio-entrevista"><a href="#"><img src="img/arrowleft.png"></a><strong>Entrevista:</strong> 1 de 3<a href="#"><img src="img/arrowright.png"></a></div>  
                    </div>
                    

                </div>
            
            </div>
        </div>
    </section>
    
   
@include('layouts.footer')     
    
    <script src="js/mediaelement-and-player.min.js"></script>
  <link rel="stylesheet" href="js/mediaelementplayer.min.css" />

    
    <script>
    $('audio,video').mediaelementplayer();
  </script>
    

    
