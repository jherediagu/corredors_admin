@include('layouts.header')
@include('layouts.menu')

<body>
    
  <div class="container-fluid padbot60">
    
      <div class="top-ranking">
            <div class="container">
              <h1>Ranking</h1>
            </div>
        </div>
            
        
        <!-- SOCI RANKING--> 
           <section class="questions associat ranking"> 
            <div class="container padbot30"> 
            
            
              <div class="row">
                  <form action="" class="form-associat">
                        <div class="col-sm-12">
                          <h2>Introdueix les teves marques</h2>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <label>Nickname<span>*</span></label>
                            <input name="" type="text" class="form-control" placeholder="Nickname">
                        </div>
                      <div class="col-md-8 col-sm-8">
                            <label>Nom de la cursa<span>*</span></label>
                            <div class="select-style">
                                <select class="form-control">
                                  <option selected="selected" disabled="disabled">Selecciona una cursa</option>
                                  <option>Nom de la cursa 1</option>
                                  <option>Nom de la cursa 2</option>
                                  <option>Nom de la cursa 3</option>
                                  <option>Nom de la cursa 4</option>
                                  <option>Nom de la cursa 5</option>
                                  <option>Nom de la cursa 6</option>
                                </select>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-4 col-xs-6">
                            <label>Posició<span>*</span></label>
                            <input name="" type="text" class="form-control" placeholder="0000">
                        </div>
                        <div class="col-md-4 col-xs-6">
                            <label>Marca<span>*</span></label>
                            <input name="" type="text" class="form-control" placeholder="00:00:00">
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-default btn-lg  pull-right">afegir marca al rànking</button>
                        </div>
                    </form>
                
                </div>
                <!-- //row  -->
                
                
              <h2 class="titol-taula">El meu rànking</h2>  
                <div class="taula">
                  <div class="taula-top">
                        <table width="100%" border="0" style="min-width:500px">
                         <thead>
                          <tr>
                            <th width="10%"><a href="javascript:;">R <span class="caret"></span></a></th>
                            <th width="30%"><a href="javascript:;">Cursa <span class="caret"></span></a></th>
                            <th width="25%"><a href="javascript:;">Categoria <span class="caret"></span></a></th>
                            <th width="20%"><a href="javascript:;">Marca <span class="caret"></span></a></th>
                            <th width="15%"><a href="javascript:;">Any <span class="caret"></span></a></th>
                          </tr>
                          </thead>
                          </table><!-- //taula 1 -->
                      </div>
                      <div id="scrollranking">

                      <table width="100%" border="0" style="min-width:500px">
                      <tbody>
                      <tr>
                        <td class="ranking" width="10%">1</td>
                        <td width="30%">Nom de la cursa</td>
                        <td width="25%">800</td>
                        <td width="20%">00:00:00</td>
                        <td width="15%">2015</td>
                      </tr>
                      <tr>
                        <td class="ranking">2</td>
                        <td>Nom de la cursa</td>
                        <td>10 k</td>
                        <td>00:00:00</td>
                        <td>2015</td>
                      </tr>
                      <tr>
                        <td class="ranking">3</td>
                        <td>Nom de la cursa</td>
                        <td>Mitja Marató</td>
                        <td>00:00:00</td>
                        <td>2015</td>
                      </tr>
                      <tr>
                        <td class="ranking">4</td>
                        <td>Nom de la cursa</td>
                        <td>Mitja Marató</td>
                        <td>00:00:00</td>
                        <td>2015</td>
                      </tr>
                      <tr>
                        <td class="ranking">5</td>
                        <td>Nom de la cursa</td>
                        <td>Mitja Marató</td>
                        <td>00:00:00</td>
                        <td>2015</td>
                      </tr>
                      <tr>
                        <td class="ranking">6</td>
                        <td>Nom de la cursa</td>
                        <td>Mitja Marató</td>
                        <td>00:00:00</td>
                        <td>2015</td>
                      </tr>
                      <tr>
                        <td class="ranking">6</td>
                        <td>Nom de la cursa</td>
                        <td>Mitja Marató</td>
                        <td>00:00:00</td>
                        <td>2015</td>
                      </tr>
                      <tr>
                        <td class="ranking">6</td>
                        <td>Nom de la cursa</td>
                        <td>Mitja Marató</td>
                        <td>00:00:00</td>
                        <td>2015</td>
                      </tr>
                      <tr>
                        <td class="ranking">6</td>
                        <td>Nom de la cursa</td>
                        <td>Mitja Marató</td>
                        <td>00:00:00</td>
                        <td>2015</td>
                      </tr>
                      <tr>
                        <td class="ranking">6</td>
                        <td>Nom de la cursa</td>
                        <td>Mitja Marató</td>
                        <td>00:00:00</td>
                        <td>2015</td>
                      </tr>
                      <tr>
                        <td class="ranking">6</td>
                        <td>Nom de la cursa</td>
                        <td>Mitja Marató</td>
                        <td>00:00:00</td>
                        <td>2015</td>
                      </tr>
                      </tbody>
                    </table>
                    </div><!-- //taula 2-->
                </div> <!-- //taula TOT-->
          </div><!-- //container -->
        </section>
        <!-- //SOCI RANKING-->
        
        
   
        
        
        
        
        
        
    </div>
    
@include('layouts.footer')     
