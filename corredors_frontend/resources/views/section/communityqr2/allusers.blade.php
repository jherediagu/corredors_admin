@include('layouts.header')
@include('layouts.menu')

<body>
    
    
    
    <div class="container-fluid padbot60">
    
      <div class="top-ranking top-llistasocis">
            <div class="container">
              <h1>Llista de Socis</h1>
            </div>
        </div>
            
        
        <!-- SOCI RANKING--> 
           <section class="questions "> 
            <div class="container padbot30"> 
            
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-6">
                      <div class="cercasocis"> 
                          <a href="javascript:;"><img src="img/icon-lupa.png" width="40" height="40" class="pull-left"></a>
                            <input name="" type="text" class="cercador form-control pull-left" placeholder="Cerca Socis" style="width:80%">
                        </div>
                    </div>
                </div>

                <div class="taula taulasocis">
                        <table width="100%" border="0" style="min-width:500px">
                         <thead>
                          <tr>
                            <th width="10%"><img src="img/icon-corredors.png" width="40" height="auto"></th>
                            <th width="30%"><a href="javascript:;">Nom <span class="caret"></span></a></th>
                            <th width="20%"><a href="javascript:;">Nickname <span class="caret"></span></a></th>
                            <th width="15%"><a href="javascript:;">Núm. Soci <span class="caret"></span></a></th>
                            <th width="25%">&nbsp;</th>
                          </tr>
                          </thead>
                      <tbody>
          @foreach ($users as $user)
                      <tr>
                        <td width="10%"><img src="img/perfil.png" class="img-circle pull-left" width="40" height="40"></td>
                        <td width="30%">{{ $user->name }} {{ $user->last_name }}</td>
                        <td width="20%">{{ $user->nickname }}</td>
                        <td width="15%">000000</td>
                        <td width="25%"><a href="javascript:;" class="semibold envia-miss-privat">ENVIAR MISSATGE PRIVAT</a></td>
                      </tr>

          @endforeach
                    
                      </tbody>
                    </table>
                    
                </div> <!-- //taula TOT-->
                
                <a href="javascript:;"><b>+ VEURE MÉS SOCIS</b></a>
          </div><!-- //container -->
        </section>
        <!-- //SOCI RANKING-->
        
        
   
        
        
        
        
        
        
    </div>

    
@include('layouts.footer')     
