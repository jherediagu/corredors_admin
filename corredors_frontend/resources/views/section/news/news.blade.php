@include('layouts.header')
@include('layouts.menu')

    <div class="container-fluid padbot60">
    
        <div class="top-ranking top-notis">
            <div class="container">
                <h1>Noticies</h1>
            </div>
        </div>
            
        
        <!-- NOTICIES--> 
           <section class="noticies"> 
            <div class="container padbot30">
            
            <div class="row">
                  <div class="col-sm-6 col-sm-offset-6">
                    <div class="cercasocis"> 
                        <a href="javascript:;"><img src="img/icon-lupa.png" width="40" height="40" class="pull-left"></a>
                          <input name="" type="text" class="cercador form-control pull-left" placeholder="Cercar notícies..." style="width:80%">
                      </div>
                  </div>
              </div>
            
        @foreach ($news as $new)    
                 <!--bloc noticia -->
                <div class="submenu_entrev datanot">
                    <div class="data-noti">Dijous, 28 de gener de 2016</div>
                </div>
                    
                <div class="noticiesbloc">
                    <h1>{{ $new->title }}<br>
                   <span>{{ $new->subtitle }}</span></h1>
                   <div class="imat-noti-top">
                        <img src="img/noticia1.jpg">
                   </div>
                   <!-- Go to www.addthis.com/dashboard to customize your tools -->
                    <div class="addthis_sharing_toolbox pull-right"></div>
                    <div class="clearfix"></div>
                    <?=$new->content?>
                    
                </div>
                   <!--//bloc noticia -->
                   
        @endforeach        
                   
                <div class="pagines"><div class="pagines-dins">
                    <a href="javascript:;"><i class="fa fa-chevron-left"></i></a>
                    <strong>Notícies:</strong> Pàgina <span class="tar">1 de 3</span>
                    <a href="javascript:;"><i class="fa fa-chevron-right"></i></a>
                </div></div>
            
            </div><!-- //container 2-->
        </section>
        <!-- //NOTICIES-->
        
    </div>

    @include('layouts.footer')     
        
    <!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-56a9e713162f1d68" async></script>
       

        