     <!--footer -->
    <div class="container-fluid bgnegre">
    	<div class="container">
        	<div class="row">
            	<div class="col-sm-6">
                	<h1 class="foot">Associació Esportiva Corredors.cat</h1>
                    <p><a href="mailto:info@corredors.cat">info@corredors.cat</a></p>
                </div>
                <div class="col-sm-6 xarxes">
           	    	<p>
                    	<a href="https://twitter.com/corredors" target="_blank" class="twitter"></a>
                        <a href="https://www.facebook.com/CorReDorsCat" target="_blank" class="face"></a>
                    </p>   
                </div>
                
                <div class="clearfix"></div>
                
                <div class="col-sm-3 col-xs-6 links-footer">
                	<h5><a href="qui-som.php">Qui som</a></h5>
                   	<a href="qui-som.php#seccio-historia">Història</a><br />
                   	<a href="qui-som.php#seccio-objectius">Objectius i estatuts</a><br />
                    <a href="form-soci.php">Com associar-m’hi?</a><br />
                    <a href="vitrina-trofeus.php">Vitrina de Trofeus</a><br />
                    <a href="qui-som.php#seccio-comissions">Comissions</a><br />
                    <a href="form-soci.php">Assòcia't</a>
                    </div>
                <div class="col-sm-3 col-xs-6 links-footer">
                	<h5>Serveis</h5>
                    <a href="serveis-reptes.php">Reptes</a><br />
                <a href="serveis-clinics.php">Clínics QR2</a><br />
                <a href="serveis-sabatilles.php">Sabatilles</a><br />
                <a href="serveis-entrenadors.php">Entrenadors</a><br />
                <a href="serveis-qualicat.php">Qualicat</a><br />
                <a href="serveis-llebres.php">Llebres</a><br />
                <a href="serveis-proves.php">Proves materials</a>	
                </div>
                <div class="col-sm-2 col-xs-4 links-footer">
                	<h5>Comunitat QR2</h5>
                    <a href="comunitat-ranking.php">Rànking</a><br />
                	<a href="comunitat-llista.php">Llista socis</a><br />
                	<a href="comunitat-entrevista.php">Entrevista</a>
                </div>
                <div class="col-sm-2 col-xs-4 links-footer">
                	<h5>Actualitat</h5>
                	<a href="noticies.php">Notícies</a><br />
                    <a href="en-xarxa.php">En xarxa</a><br />
                    <a href="forum.php" target="_blank">Fòrum</a>
                </div>
                <div class="col-sm-2 col-xs-4 links-footer">
                	<h5>...i molt més</h5>
                    <a href="galeria.php">Galeria</a><br />
                    <a href="enllasos.php">Enllaços</a><br />
                    <a href="botiga.php">Botiga</a>
                </div>
                <div class="clearfix"></div>
                
                <div class="col-sm-12 copyright">
                © 2015<br />created by <a href="" target="_blank">INDICA Agencia creativa</a>
                </div>
                
            </div>
        </div>
    </div>
    <!--//footer -->
    
    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title titular-form" id="myModalLabel">Accés d’usuaris</h4>
          </div>
          <div class="modal-body">
          
         {!! Form::open(['route' => 'auth/login', 'class' => 'form']) !!}
   
            <!-- <form action="perfil.php" class="form-horizontal"> -->
                <label>Usuari</label>


                {!! Form::text('nickname', '', ['class'=> 'form-control']) !!}

<!--                 <input name="" type="text" class="form-control">
 -->                <p>&nbsp;</p>
                <label>Contrasenya</label>


                {!! Form::password('password', ['class'=> 'form-control']) !!}

                <!--<input name="" type="password" class="form-control">-->
                <p><a href="javascript:;"><small>Has oblidat la contrasenya?</small></a></p>
                <p>&nbsp;</p>            
              

                {!! Form::submit('Accedir',['class' => 'btn  btn-default pull-right btn-lg']) !!}
       
               <!--  <button type="submit" class="btn  btn-default pull-right btn-lg">Accedir</button> -->
                <a href="abonat" class="inici_sessio padesp text-uppercase pull-right">Crea un nou usuari</a>
             </form>
            
            {!! Form::close() !!}

          </div>
        </div>
      </div>
    </div> 
    <!-- //Modal -->
    
    
   <!-- Modal 2 politica de privacitat -->
    <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title titular-form" id="myModalLabel">Politica de privacitat</h4>
          </div>
          <div class="modal-body">
          <p>www.corredors.cat garanteix la confidencialitat de les dades personals que es faciliten a través de les pàgines d'aquest web en el termes establerts per la Llei orgànica 15/1999, de 13 de desembre, de protecció de dades de caràcter personal (LOPD), implementant les mesures de seguretat necessàries i adequades al tipus de dades personals, d'acord amb el Reial decret 1720/2007, de 21 de desembre, pel qual s'aprova el Reglament de desenvolupament de la LOPD</p>
          <p>Les dades que es requereixin seran les estrictament necessàries, adequades i pertinents per a la finalitat per a la qual es recullin i seran sotmeses a un tractament automatitzat. La finalitat de la recollida de dades podrà ser comercial i/o tècnica.</p>
          <p>Tanmateix, si voleu exercir els drets d'accés, rectificació, cancel·lació i oposició sobre alguna de les vostres dades personals (incloses imatges o gravacions de veu), que poguessin aparèixer en alguna pàgina de www.corredors.cat, podeu enviar un mail a <a href="mailto:mail@corredors.cat">mail@corredors.cat</a>.</p>          
          <button type="button" class="btn tancar_modal  btn-lg" data-dismiss="modal">Close</button>        
          </div>
        </div>
      </div>
    </div> 
    <!-- //Modal -->
    

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="{{ asset('js/ie10-viewport-bug-workaround.js') }}"></script>
        
    <link rel="stylesheet" href="{{ asset('js/jquery.bxslider-responsive.css') }}">
    <script type="text/javascript" src="{{ asset('js/jquery.bxslider-rahisified.js') }}"></script>  
    
    <link rel="stylesheet" type="text/css" href="{{ asset('js/shadowbox.css') }}">
	<script type="text/javascript" src="{{ asset('js/shadowbox.js') }}"></script> 
    
    <script type="text/javascript" src="{{ asset('js/enscroll-0.6.1.min.js') }}"></script>     

        <script type="text/javascript" src="{{ asset('js/jquery.autotab.min.js') }}"></script>    
  
    <script type="text/javascript">
  
	$(document).ready(function() {

        $('input.cuentatot').autotab();

        /*SLIDER GALERIA*/
        $('.bxslider').bxSlider({
          mode: 'horizontal',
          //slideWidth: 990,
          infiniteLoop: true,
          pager: false,
          controls: true,
          auto:true,
          pause:4000,
          speed:1000
        });
		
		/*SLIDER GALERIA*/
		$('.bxslider').bxSlider({
		  mode: 'horizontal',
		  //slideWidth: 990,
		  infiniteLoop: true,
		  pager: false,
		  controls: true,
		  auto:true,
		  pause:4000,
		  speed:1000
		});
		
		/*SLIDER PRODUCTES ant*/
		$('.bxslider2ANT').bxSlider({
		  mode: 'horizontal',
		  slideWidth: 247,
		  infiniteLoop: true,
		  pager: false,
		  controls: true,
		  auto:true,
		  pause:4000,
		  speed:1000,
		  minSlides:1,
		  maxSlides:4,
		  moveSlides:1
		});
		
		
		/*SLIDER PRODUCTES nou responsive*/
		$('.bxslider2').bxSlider({
		  //slideWidth: 990,
		  infiniteLoop: true,
		  autoReload: true,
		  slideMargin: 30,
		  pager:false,
			 breaks: [{
				screen: 0,
				slides: 1,
				pager: false
			}, {
				screen: 460,
				slides: 1
			},
			{
				screen: 768,
				slides: 2
			}, {
				screen: 991,
				slides: 3
			}, {
				screen: 1200,
				slides: 4
			}]
		
		});	
		
			
		
		/*SHADOWBOX*/							
		Shadowbox.init({
			players:  ['img','iframe'],
			overlayColor: "#000",
			overlayOpacity: 0.8,
			continuous:true,
			displayCounter:true,
			autoplayMovies:true,
			slideshowDelay:4
			});
		window.onload = Shadowbox.init;	
		
		/*SCROLL*/
		

		if ($(window).width()>768){
			$('#scrollranking').enscroll({
				showOnHover: false,
				verticalTrackClass: 'track3',
				verticalHandleClass: 'handle3',
				pollChanges:true
			});
		
			$('#scrollforum').enscroll({
                showOnHover: false,
                verticalTrackClass: 'track3',
                verticalHandleClass: 'handle3',
                pollChanges:true
            });
            $('#scrollbox3').enscroll({
				showOnHover: false,
				verticalTrackClass: 'track3',
				verticalHandleClass: 'handle3',
				pollChanges:true
			});
		}

        /* DESPLEGA MENÚ SUPERIOR */
        $('.dropdown').hover(
            function() {
                $( '.navbar-fixed-top' ).height($( '.navbar-fixed-top' ).height()+$(this).children('ul').height()+20);
            }, function() {
                $( '.navbar-fixed-top' ).css('height','auto');
            }
        );
		
	});	
		
	
	$(document).ready(function() {
		
		/*CANVIA CLASSE CADA CLIK*/
		$('.boto').click(function(e){
		  e.preventDefault();
		  $(this).toggleClass('obert');
		});
		
		/*PLEGA DESPLEGA UN SOL ELEMENT*/
		$(".login-logejat").hover(function(event){
			event.preventDefault();
			$('.subperfil').slideToggle();
		});	
		
		/*PLEGA DESPLEGA UN SOL ELEMENT-v2*/
		$(".mostra-filtre").click(function(event){
			event.preventDefault();
			$('.filtre-xarxes').slideToggle();
		});	
		$(".filtre-xarxes").click(function(event){
			event.preventDefault();
			$('.filtre-xarxes').slideUp();
		});	
		
		
		/*PLEGA DESPLEGA respostes*/
		$(".mostraresposta").click(function(event){
			event.preventDefault();
			$(this).siblings('.resposta').slideToggle();
		});	
				

		//nos desplazamos entre todos los divs
		if($(window).width()>768){
			
		$('a.ancla').click(function(e){
		e.preventDefault();
		enlace  = $(this).attr('href');
		$('html, body').animate({
			scrollTop: $(enlace).offset().top - 150
		}, 1000);
		});

		}else{
			$('a.ancla').click(function(e){
		e.preventDefault();
		enlace  = $(this).attr('href');
		$('html, body').animate({
			scrollTop: $(enlace).offset().top - 65
		}, 1000);
		});
		
		}
				
		
		//vamos al principio o al final de la página
		$('a.arriba').click(function(e){
		e.preventDefault();
		volver  = $(this).attr('href');
		$('html, body').animate({
			scrollTop: $(volver).offset().top
		}, 2000);
		});
		//pasando la cantidad de pixeles que queremos a scrollTop
		$('.prueba').click(function(){
			$('html, body').animate({scrollTop:100}, 2000); return false;
		});
		
		
	});
	</script>
    
	<script src="js/cookietool.js"></script>
    <link href="js/cookietool.css" rel="stylesheet">
    <script>CookieTool.API.ask();</script>


       <link href="{{ asset('css/flatWeatherPlugin.css') }}" rel="stylesheet">
        <!-- include a copy of jquery (if you haven't already)
        <script src="js/jquery-2.1.1.min.js"></script>
        <!-- include flatWeatherPlugin.js -->
        <script src="{{ asset('js/jquery.flatWeatherPlugin.js') }}"></script>  

        <script type="text/javascript">
        function recalculaScroll(){
            $('#scrollforum').height($('.heightForum1').height()+$('.heightForum2').height()-128);
            $('#scrollbox3').height($('.heightScrollbox').height()-124);
        };
        $(document).ready(function() {
            //ex. simple view, openweathermap weather
            var example2 = $("#example-2").flatWeatherPlugin({
                location: "Barcelona, ES",
                country: "Spain",
                api: "yahoo",
                view : "forecast",
                forecast: 3,
                render: true
            });
            recalculaScroll();
        });
        $(window).load(function(){ 
            recalculaScroll();
        });
        $(window).resize(function(){
            recalculaScroll();
        });
        </script>
    
    
  </body>
</html>
