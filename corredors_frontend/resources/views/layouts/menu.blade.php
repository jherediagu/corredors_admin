@if (Auth::check())

<div class="content-fixed hidden-xs">
  <div class="container" style=" position:relative">
      <div class="login-logejat ">
            <a href="javascript:;">
                <div class="pull-right img-circle"><img src="img/perfil.jpg" width="36" height="36" class="img-circle" /></div>
                <div class="pull-right login_nom">{{ Auth::user()->nickname }}</div>
                <div class="notific img-circle">18</div>
            </a>
            <div class="clearfix"></div>
            <div class="subperfil">
                <ul>
                    <li><a href="perfil">El meu perfil</a></li>
                    <li><a href="perfil-missatges.php">Missatges <span class="tar pull-right">(18)</span></a></li>
                    <li><a href="{{route('auth/logout')}}">Tancar sessió</a></li>
                </ul>
            </div>
        </div>
  </div>
</div>

<nav class="navbar-fixed-top">
 <div class="container" style="position:relative">
        <a class="logo" href="{{URL::to('/')}}"><img src="img/logo-corredors.png" alt="corredors.cat"></a>
        <div class="login  hidden-xs">&nbsp;</div>

@else


 <nav class="navbar-fixed-top">
 <div class="container" style="position:relative">
        <a class="logo" href="{{URL::to('/')}}"><img src="img/logo-corredors.png" alt="corredors.cat"></a>
        <div class="login hidden-xs"><!-- Button trigger modal -->
            <button type="button" class="inici_sessio" data-toggle="modal" data-target="#myModal">
            INICIAR SESSIÓ<!--<span class="glyphicon glyphicon-log-in"></span>-->
            </button>
        </div>

@endif

       <!-- <div class="pull-right inici_sessio logejat">Nom Usuari <a href="javascript:;"><span class="glyphicon glyphicon-log-out"></span></a>
        </div>-->
        <div class="clearfix"></div>
       
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button> 
        </div>
        <div id="navbar" class="navbar-collapse collapse pull-right">
          <ul class="nav navbar-nav">
            <li class="dropdown">

      <a href="qui-som" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Qui som <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="qui-som#seccio-associacio">L'associació</a></li>
                <li><a href="qui-som#seccio-historia">Història</a></li>
                <li><a href="qui-som#seccio-objectius">Objectius i Estatuts</a></li>
                <li><a href="qui-som#seccio-associat">Com associar-m’hi?</a></li>
                <li><a href="qui-som#seccio-sabatilles">Sabatilles Solidàries</a></li>
                <li><a href="qui-som#seccio-comissions">Comissions</a></li>
              </ul>

            </li>
            <li><a href="abonat">Assòcia't</a></li>
            <li class="dropdown">
              <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Serveis <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="reptes">Reptes</a></li>
                <li><a href="clinicqr2">Clínics QR2</a></li>
                <li><a href="sabatilles">Sabatilles</a></li>
                <li><a href="entrenadors">Entrenadors</a></li>
                <li><a href="qualicat">Qualicat</a></li>
                <li><a href="llebres">Llebres</a></li>
                <li><a href="proves-materials">Proves materials</a></li>
              </ul>
            </li>
            <li><a href="http://webs8.javajan.com/corredors_codoforum/">Fòrum</a></li>
            <li class="dropdown">
              <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Comunitat QR2<span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="ranking">Rànking</a></li>
                <li><a href="comunitat-llista">Llista socis</a></li>
                <li><a href="comunitat-entrevista">Entrevista</a></li>

              </ul>
            </li>
            <li class="dropdown">
              <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Notícies<span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="noticies">Notícies</a></li>
                <li><a href="http://entorn.javajan.com/corredors/en-xarxa.php">En xarxa</a></li>
              </ul>
            </li>
            <li><a href="botiga">Botiga</a></li>
            <li class="hide visible-xs"><button type="button" class="inici_sessio semibold" data-toggle="modal" data-target="#myModal">
            INICIAR SESSIÓ<!--<span class="glyphicon glyphicon-log-in"></span>-->
            </button></li>
                       
          </ul>
        </div><!--/.nav-collapse -->
         
      </div>
      </nav>