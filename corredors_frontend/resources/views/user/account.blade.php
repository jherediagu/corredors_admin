@include('layouts.header')
@include('layouts.menu')

<body class="bg-associat">    

 <!-- DADES ASSOCIAT/REGISTRAT-->
    <div class="container-fluid padbot60">
    
         <div class="container compte cap-perfil">         
            <div class="cap-perfil-tit"><img src="img/perfil.jpg" class="img-circle"><br>
            {{ Auth::user()->nickname }}
            </div>
           <div class="cap-perfil-num"> núm. soci&nbsp;&nbsp; 00000 <small class="hidden-xs"><em>(Només apareix en perfil ASSOCIAT:)</em></small></div>
           <a class="edit-dades" href="editar-perfil"></a>
           <div class="clearfix"></div>
        </div>
        
        <section class="questions associat">
            <div class="container compte">
                <div class="row">            
                    <div class="col-sm-12">
                        <table width="100%" border="0" class="dades-perfil">
                          <tr>
                            <td><strong>Nickname</strong></td>
                            <td>{{ Auth::user()->nickname }}</td>
                          </tr>
                          <tr>
                            <td><strong>E-mail</strong></td>
                            <td>{{ Auth::user()->email }}</td>
                          </tr>
                          <tr>
                            <td><strong>Nom</strong></td>
                            <td>{{ Auth::user()->name }}</td>
                          </tr>
                          <tr>
                            <td><strong>Cognoms</strong></td>
                            <td>{{ Auth::user()->lastname }}</td>
                          </tr>
                          <tr>
                            <td><strong>Data de naixement</strong></td>
                            <td>{{ Auth::user()->birthday }}</td>
                          </tr>
                          <tr>
                            <td><strong>DNI o passaport</strong></td>
                            <td>{{ Auth::user()->dni }}</td>
                          </tr>
                          <tr>
                            <td><strong>Adreça</strong></td>
                            <td>{{ Auth::user()->address }}</td>
                          </tr>
                          <tr>
                            <td><strong>Codi Postal</strong></td>
                            <td>{{ Auth::user()->postal_code }}</td>
                          </tr>
                          <tr>
                            <td><strong>Ciutat</strong></td>
                            <td>{{ Auth::user()->city }}</td>
                          </tr>
                          <tr>
                            <td><strong>Província</strong></td>
                            <td>{{ Auth::user()->province }}</td>
                          </tr>
                          <tr>
                            <td><strong>Telèfon de contacte</strong></td>
                            <td>{{ Auth::user()->telephone }}</td>
                          </tr>
                        </table>

                    </div>
                    <div class="clearfix"></div>
               </div> <!-- //row-->
            </div><!-- //container 1--> 
           </section><!-- //seccio DADES -->
            
    @if( Auth::user()->account_type == 0)        
           <!-- seccio targeta USUARI--> 
           <section class="questions associat"> 
            <div class="container compte2">            
                <h2 class="tit-ass text-center">Associa’t</h2>
                <h3 class="subtit-ass text-center">Fes-te soci/a i <span>gaudeix dels avantatges</span></h3>
                
                <div class="targeta"><img src="img/tr-targ.gif" width="100%" height="auto">
                    <div class="soci_num">
                        <p><span class="validfins">Vàlid fins  28-2-2016</span></p>
                        <span class="soci_nom">Antoni Tarradellas Casademunt</span><br>
                        SOCI/A NÚM: 721
                    </div>
                </div>

                <div class="text-center">
                    <a href="form-soci.php"><button type="button" class="btn btn-default btn-lg" style="float:none; margin:0 auto">associar-me</button></a>
                </div>
            </div><!-- //container 2-->

        </section>
        <!-- //seccio targeta USUARI-->
        
      
    @else
            <!-- seccio targeta SOCI--> 
           <section class="questions associat"> 
            <div class="container compte2">            
                <h2 class="tit-ass text-center">Carnet de Soci/a</h2>
                <div class="targeta"><img src="img/tr-targ.gif" width="100%" height="auto">
                    <div class="soci_num">
                        <p><span class="validfins">Vàlid fins  28-2-2016</span></p>
                        <span class="soci_nom">{{ Auth::user()->name }} {{ Auth::user()->lastname }}</span><br>
                        SOCI/A NÚM: 0000
                    </div>
                </div>
                <h2 class="tit-ass text-center"><small>Porta el teu carnet sempre amb tú <br class="hidden-xs">i els teus dispositius mòbils</small></h2>
                <div class="row">
                    <div class="col-sm-4">
                        <a class="dispositiu android" href="" target="_blank">Passbook android</a>
                    </div>
                    <div class="col-sm-4">
                        <a class="dispositiu ios" href="" target="_blank">Passbook ios</a>
                    </div>
                    <div class="col-sm-4">
                        <a class="dispositiu pdf" href="" target="_blank">descàrrega pdf</a>
                    </div>
                </div>
            </div><!-- //container 2-->
        </section>
        <!-- //seccio targeta SOCI-->
        
        
    @endif

           <!-- SOCI RANKING--> 
           <section class="questions associat"> 
            <div class="container compte2 padbot30"> 
              <h2 class="titol-taula">El meu rànking</h2>  
                <div class="taula">
                  <table width="100%" border="0">
                     <thead>
                      <tr>
                        <th><a href="javascript:;">R <span class="caret"></span></a></th>
                        <th><a href="javascript:;">Cursa <span class="caret"></span></a></th>
                        <th><a href="javascript:;">Categoria <span class="caret"></span></a></th>
                        <th><a href="javascript:;">Marca <span class="caret"></span></a></th>
                        <th><a href="javascript:;">Any <span class="caret"></span></a></th>
                      </tr>
                      </thead>
                      <tbody>
                      <tr>
                        <td class="ranking">1</td>
                        <td>Nom de la cursa</td>
                        <td>800</td>
                        <td>00:00:00</td>
                        <td>2015</td>
                      </tr>
                      <tr>
                        <td class="ranking">2</td>
                        <td>Nom de la cursa</td>
                        <td>10 k</td>
                        <td>00:00:00</td>
                        <td>2015</td>
                      </tr>
                      <tr>
                        <td class="ranking">3</td>
                        <td>Nom de la cursa</td>
                        <td>Mitja Marató</td>
                        <td>00:00:00</td>
                        <td>2015</td>
                      </tr>
                      <tr>
                        <td class="ranking">4</td>
                        <td>Nom de la cursa</td>
                        <td>Mitja Marató</td>
                        <td>00:00:00</td>
                        <td>2015</td>
                      </tr>
                      <tr>
                        <td class="ranking">5</td>
                        <td>Nom de la cursa</td>
                        <td>Mitja Marató</td>
                        <td>00:00:00</td>
                        <td>2015</td>
                      </tr>
                      <tr>
                        <td class="ranking">6</td>
                        <td>Nom de la cursa</td>
                        <td>Mitja Marató</td>
                        <td>00:00:00</td>
                        <td>2015</td>
                      </tr>
                      </tbody>
                    </table>
                    
                </div> 
                <a href="javascript:;" class="inici_sessio boto-taula padesp text-uppercase pull-right">AFEGIR NOVA MARCA</a>       
            </div><!-- //container 2-->
        </section>
        <!-- //SOCI RANKING-->
        
      
        <!-- SOCI SORTEIGS--> 
           <section class="questions associat"> 
            <div class="container compte2 padbot30"> 
              <h2 class="titol-taula">Els meus sorteigs</h2>          
                <div class="taula">
                  <table width="100%" border="0">
                     <thead>
                      <tr>
                        <th>Nom del sorteig</th>
                        <th>Núm.</th>
                        <th class="text-right">Resolució</th>
                      </tr>
                      </thead>
                      <tbody>
                      <tr>
                        <td>Nom del sorteig</td>
                        <td>0000</td>
                        <td align="right" class="no_sortejat">Manquen 8 dies pel sorteig!</td>
                      </tr>
                      <tr>
                        <td>Nom del sorteig</td>
                        <td>0000</td>
                        <td align="right" class="no_sortejat">Manquen 18 dies pel sorteig!</td>
                      </tr>
                      <tr>
                        <td>Nom del sorteig</td>
                        <td>0000</td>
                        <td align="right" class="no_sortejat">Manquen 28 dies pel sorteig!</td>
                      </tr>
                      <tr>
                        <td>Nom del sorteig</td>
                        <td>0000</td>
                        <td align="right" class="sortejat">Núm. 00224</td>
                      </tr>
                      <tr>
                        <td>Nom del sorteig</td>
                        <td>0000</td>
                        <td align="right" class="sortejat">Núm. 00333</td>
                      </tr>

                      </tbody>
                    </table>
                </div>
                <a href="javascript:;" class="inici_sessio boto-taula padesp text-uppercase pull-right">NOVA PARTICIPACIÓ</a> 
                
            </div><!-- //container 2-->
        </section>
        <!-- //SOCI SORTEIGS-->
        
        
        
        
        
    </div>
    
@include('layouts.footer')
