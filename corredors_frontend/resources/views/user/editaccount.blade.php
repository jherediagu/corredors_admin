@include('layouts.header')
@include('layouts.menu')

<body class="bg-associat perfil-editar">    


    <!-- formulari associar-se-->
    <div class="container-fluid padbot60">
    
    <div class="container compte cap-perfil-edit">         
            <div class="cap-perfil-tit"><img src="img/icon-perfil-edit.png" width="80" height="80"><br>
            {{ Auth::user()->nickname }}
            </div>
           <div class="cap-perfil-num"> núm. soci&nbsp;&nbsp; 00000 <small><em>(Només apareix en perfil ASSOCIAT:)</em></small></div>
           <div class="clearfix"></div>
        </div>
      <section class="questions associat ">
        <div class="container compte">
              <div class="row">


                {!! Form::open(['route' => 'auth/edit', 'class' => 'form-associat']) !!}
                    
                        <!--<div class="col-sm-12">
                          <h2>Dades d’associat</h2>
                        </div>-->
                    
                        <label class="col-sm-4">Nickname<span>*</span></label>
                        <div class="col-sm-6">
                            <input name="nickname" type="text" class="form-control" placeholder="{{ Auth::user()->nickname }}">
                        </div>
                        
                        <label class="col-sm-4">E-mail<span>*</span></label>
                        <div class="col-sm-6">
                            <input name="email" type="email" class="form-control" placeholder="{{ Auth::user()->email }}">
                        </div>
                        
                       <!-- <div class="col-sm-12">
                          <h2>Informació personal</h2>
                        </div>-->
                    
                        <label class="col-sm-4">Nom<span>*</span></label>
                        <div class="col-sm-6">
                            <input name="name" type="text" class="form-control" placeholder="{{ Auth::user()->name }}">
                        </div>
                        
                        <label class="col-sm-4">Cognoms<span>*</span></label>
                        <div class="col-sm-6">
                            <input name="lastname" type="text" class="form-control" placeholder="{{ Auth::user()->lastname }}">
                        </div>
                        
                        <label class="col-sm-4">Data de naixement<span>*</span></label>
                        <div class="col-sm-6">
                            <input name="birthday" type="text" class="form-control" placeholder="{{ Auth::user()->birthday }}">
                        </div>
                        
                        <label class="col-sm-4">DNI<span>*</span></label>
                        <div class="col-sm-6">
                            <input name="dni" type="text" class="form-control" placeholder="{{ Auth::user()->dni }}">
                        </div>
                        
                        <label class="col-sm-4">Adreça<span>*</span></label>
                        <div class="col-sm-6">
                            <input name="address" type="text" class="form-control" placeholder="{{ Auth::user()->address }}">
                        </div>
                        
                        <label class="col-sm-4">Codi Postal<span>*</span></label>
                        <div class="col-sm-6">
                            <input name="postal_code" type="text" class="form-control" placeholder="{{ Auth::user()->postal_code }}">
                        </div>
                        
                        <label class="col-sm-4">Ciutat<span>*</span></label>
                        <div class="col-sm-6">
                            <input name="city" type="text" class="form-control" placeholder="{{ Auth::user()->city }}">
                        </div>
                        
                        <label class="col-sm-4">Província<span>*</span></label>
                        <div class="col-sm-6">
                            <input name="province" type="text" class="form-control" placeholder="{{ Auth::user()->province }}">
                        </div>
                         
                        <label class="col-sm-4">Telèfon de contacte</label>
                        <div class="col-sm-6">
                            <input name="telephone" type="text" class="form-control" placeholder="{{ Auth::user()->telephone }}">
                        </div>
                        <div class="clearfix"></div>
                        
                        <div class="col-sm-12">
                        <p style="border-top:dotted #999999 1px; margin-top:30px; padding:15px 0 0 0; text-align:center">
                          <small><em>(Dades només apareixen en perfil ASSOCIAT:)</em></small>
                        </p>
                          <h2>Informació de pagament de la quota anual</h2>
                            
                        </div>
                        
                        <label class="col-sm-4">Nom del titular<span>*</span></label>
                        <div class="col-sm-6">
                            <input name="titular_name" type="text" class="form-control" placeholder="{{ Auth::user()->titular_name }}">
                        </div>
                        
                        <label class="col-sm-4">Cognoms del titular<span>*</span></label>
                        <div class="col-sm-6">
                            <input name="titular_lastname" type="text" class="form-control" placeholder="{{ Auth::user()->titular_lastname }}">
                        </div>
                        <div class="clearfix"></div>
                       
                        <label class="col-sm-12">Número de compte bancari IBAN<span>*</span></label>
                        <div class="clearfix"></div>
                        <div class="col-md-2 col-sm-2  col-xs-4">
                           <input name="IBAN_country_code" type="text"  class="form-control cuenta cuentatot"placeholder="{{ Auth::user()->IBAN_country_code }}"
                           maxlength="4" size="4" style="width:81px;margin-right:23px">
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-4">
                            <input name="IBAN_cheque_number" type="text" class="form-control cuenta cuentatot" placeholder="{{ Auth::user()->IBAN_cheque_number }}" maxlength="4" size="4" style="width:81px;margin-right:23px">
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-4">
                            <input name="IBAN_bank_code" type="text" class="form-control cuenta cuentatot" placeholder="{{ Auth::user()->IBAN_bank_code }}" maxlength="4" size="4" style="width:81px;margin-right:23px">
                        </div>
                        <div class="col-md-1 col-sm-1 col-xs-3">
                            <input name="IBAN_sort_code" type="text" class="form-control cuentatot" placeholder="{{ Auth::user()->IBAN_sort_code }}" maxlength="2" size="2" style="width:41px;margin-right:23px">
                        </div>
                        <div class="col-md-5 col-sm-5  col-xs-9">
                            <input name="IBAN_account_number" type="text" class="form-control cuentatot" placeholder="{{ Auth::user()->IBAN_account_number }}" maxlength="10" size="10" style="width:201px;margin-right:0">
                        </div>
                        <div class="clearfix"></div>
                        <label class="col-sm-12">Concepte<span>*</span></label>
                        <div class="col-sm-12">
                            <input name="concept" type="text" class="form-control" placeholder="{{ Auth::user()->concept }}">
                          <p><small><span class="blauverd">*</span> Dades obligatòries</small></p>
                          <div class="clearfix"></div>
                           <!-- <input name="" type="checkbox" value="" class="checknou"><label class="col-sm-12">He llegit i accepto <a href="javascript:;"  data-toggle="modal" data-target="#myModal2">les condicions</a></label>-->
                            <p style="border-bottom:dotted #999999 1px; margin-bottom:30px; text-align:center">
                              <small><em>(Fins aquí dades extres, només editar perfil ASSOCIAT)</em></small>
                            </p>
                            <p>&nbsp;</p>
                            <div class="clearfix"></div>
                            <a href="javascript:history.back()" class="inici_sessio padesp text-uppercase pull-left">Tornar enrera</a>

                            {!! Form::submit('Guardar canvis',['class' => 'btn btn-default btn-lg pull-right']) !!}

                      </div>

                        {{ csrf_field() }}



                    {!! Form::close() !!}
              
                {!! Form::open(['route' => 'auth/edit', 'class' => 'form-associat']) !!}
                    <div class="col-sm-12">
                      <h2>Contrasenya</h2>
                    </div>
                    
                    <label class="col-sm-4">Password<span>*</span></label>
                    <div class="col-sm-6">
                      <input name="" type="text" class="form-control" placeholder="Password">
                    </div>
                    
                    <label class="col-sm-4">Nou password<span>*</span></label>
                    <div class="col-sm-6">
                      <input name="password" type="password" class="form-control" placeholder="Password">
                    </div>
                    
                    
                    <label class="col-sm-4">Confirmar nou password<span>*</span></label>
                      <div class="col-sm-6">
                    <input name="confirm_password" type="password" class="form-control" placeholder="Confirmar password">
                    </div>
                    
                    <p>&nbsp;</p>
                    <div class="clearfix"></div>
                    
                    <div class="col-sm-12">
                        <a href="javascript:history.back()" class="inici_sessio padesp text-uppercase pull-left">cancel·lar</a>

                        {!! Form::submit('Guardar contrasenya',['class' => 'btn btn-default btn-lg  marbot45  pull-right']) !!}

                    </div>
                    
                    {{ csrf_field() }}

                    {!! Form::close() !!}

        </div><!-- //row-->         
      </div><!-- //container 1--> 
    </section><!-- //seccio dades -->
            
            
            
           <section class="questions associat "> 
            <div class="container compte2">            
                <h2 class="tit-ass text-center">Associa’t</h2>
                <h3 class="subtit-ass text-center">Fes-te soci/a i <span>gaudeix dels avantatges</span></h3>
                
                <div class="targeta"><img src="img/tr-targ.gif" width="100%" height="auto">
                    <div class="soci_num">
                        <p><span class="validfins">Vàlid fins  28-2-2016</span></p>
                        <span class="soci_nom">Antoni Tarradellas Casademunt</span><br>
                        SOCI/A NÚM: 721
                    </div>
                </div>

                <div class="text-center">
                  <a href="form-soci.php"><button type="button" class="btn btn-default btn-lg" style="float:none; margin:0 auto">associar-me</button></a>
                </div>
            </div><!-- //container 2-->

        </section><!-- //seccio targeta -->
    </div>
    
    
@include('layouts.footer')
