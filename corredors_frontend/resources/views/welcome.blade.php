@include('layouts.header')
@include('layouts.menu')

<body class="home"> 

    <!-- container fluid-->
    <div class="container-fluid">
      <div id="home-principal">
        <div class="container text-center">
            <h1>Evoluciones?</h1>
            <p>Associació Esportiva Corredors.cat</p>
            <a href="abonat"><div class="btn btn-default btn-lg">Assòcia't</div></a>
        </div>
      </div>
    </div>
    <!-- //container fluid-->
    
    <!-- container info-->
   <!-- <div class="container">-->
        <div class="row">
            <div class="col-sm-12">
                <div class="pager text-center" id="home-banner"><img src="{{ asset('img/banner.png') }}" ></div>
            </div>
            <div class="clearfix"></div>
            
            <!--forum / noticies /en xarxa -->
            <div class="col-md-6">
                <h1 class="titular deu">
                <div class="row">
                    <div class="col-xs-2 col-sm-1 lupa"><a href="javascript:;"><img src="{{ asset('img/icon-lupa.png') }}" width="40" height="40"></a>
                        <!--<a href="javascript:;"><span class="glyphicon glyphicon-search tar"></span></a>-->
                    </div>
                    <div class="col-xs-10 col-sm-10">
                        <input name="" type="text" class="cercador form-control" placeholder="Cerca al fòrum...">
                  </div>
                  </div>
                </h1>
                <div class="forum"><div id="scrollforum">
                
                    <a href="javascript:;" target="_blank">
                        <h3 class="tipus tecnic">
                            <div class="">
                            <img src="img/perfil.png" class="img-circle pull-left" width="40" height="40">
                            <strong>Sonia</strong> va escriure el 4 d’Agost a les 18:45<br>
                            <span class="tar">Cursa solidaria contra el càncer</span>
                            </div>
                        </h3>
                    </a>
                    <a href="javascript:;" target="_blank">
                        <h3 class="tipus tecnic">
                            <div class="">
                            <img src="img/perfil.png" class="img-circle pull-left" width="40" height="40">
                            <strong>Sonia</strong> va escriure el 4 d’Agost a les 18:45<br>
                            <span class="tar">Cursa solidaria contra el càncer</span>
                            </div>
                        </h3>
                    </a>
                    <a href="javascript:;" target="_blank">
                        <h3 class="tipus cursa">
                            <div class="">
                            <img src="img/perfil.png" class="img-circle pull-left" width="40" height="40">
                            <strong>Sonia</strong> va escriure el 4 d’Agost a les 18:45<br>
                            <span class="tar">Cursa solidaria contra el càncer</span>
                            </div>
                        </h3>
                    </a>
                    <a href="javascript:;" target="_blank">
                        <h3 class="tipus tecnic">
                            <div class="">
                            <img src="img/perfil.png" class="img-circle pull-left" width="40" height="40">
                            <strong>Sonia</strong> va escriure el 4 d’Agost a les 18:45<br>
                            <span class="tar">Cursa solidaria contra el càncer</span>
                            </div>
                        </h3>
                    </a>
                    <a href="javascript:;" target="_blank">
                        <h3 class="tipus cursa">
                            <div class="">
                            <img src="img/perfil.png" class="img-circle pull-left" width="40" height="40">
                            <strong>Sonia</strong> va escriure el 4 d’Agost a les 18:45<br>
                            <span class="tar">Cursa solidaria contra el càncer</span>
                            </div>
                        </h3>
                    </a>
                    <a href="javascript:;" target="_blank">
                        <h3 class="tipus cursa">
                            <div class="">
                            <img src="img/perfil.png" class="img-circle pull-left" width="40" height="40">
                            <strong>Sonia</strong> va escriure el 4 d’Agost a les 18:45<br>
                            <span class="tar">Cursa solidaria contra el càncer</span>
                            </div>
                        </h3>
                    </a>
                    <a href="javascript:;" target="_blank">
                        <h3 class="tipus cursa">
                            <div class="">
                            <img src="img/perfil.png" class="img-circle pull-left" width="40" height="40">
                            <strong>Sonia</strong> va escriure el 4 d’Agost a les 18:45<br>
                            <span class="tar">Cursa solidaria contra el càncer</span>
                            </div>
                        </h3>
                    </a>
                    <a href="javascript:;" target="_blank">
                        <h3 class="tipus cursa">
                            <div class="">
                            <img src="img/perfil.png" class="img-circle pull-left" width="40" height="40">
                            <strong>Sonia</strong> va escriure el 4 d’Agost a les 18:45<br>
                            <span class="tar">Cursa solidaria contra el càncer</span>
                            </div>
                        </h3>
                    </a>
                    <a href="javascript:;" target="_blank">
                        <h3 class="tipus cursa">
                            <div class="">
                            <img src="img/perfil.png" class="img-circle pull-left" width="40" height="40">
                            <strong>Sonia</strong> va escriure el 4 d’Agost a les 18:45<br>
                            <span class="tar">Cursa solidaria contra el càncer</span>
                            </div>
                        </h3>
                    </a>
                    <a href="javascript:;" target="_blank">
                        <h3 class="tipus cursa">
                            <div class="">
                            <img src="img/perfil.png" class="img-circle pull-left" width="40" height="40">
                            <strong>Sonia</strong> va escriure el 4 d’Agost a les 18:45<br>
                            <span class="tar">Cursa solidaria contra el càncer</span>
                            </div>
                        </h3>
                    </a>
                    <a href="javascript:;" target="_blank">
                        <h3 class="tipus cursa">
                            <div class="">
                            <img src="img/perfil.png" class="img-circle pull-left" width="40" height="40">
                            <strong>Sonia</strong> va escriure el 4 d’Agost a les 18:45<br>
                            <span class="tar">Cursa solidaria contra el càncer</span>
                            </div>
                        </h3>
                    </a>
                    <a href="javascript:;" target="_blank">
                        <h3 class="tipus cursa">
                            <div class="">
                            <img src="img/perfil.png" class="img-circle pull-left" width="40" height="40">
                            <strong>Sonia</strong> va escriure el 4 d’Agost a les 18:45<br>
                            <span class="tar">Cursa solidaria contra el càncer</span>
                            </div>
                        </h3>
                    </a>
                </div></div>
                <h1 class="titular taronja hidden-xs"><p>Últims posts al Fòrum<p></h1>
            </div> 
            <div class="col-md-6">
                <div class="row">
                    <div class="col-sm-12 heightForum1">
                        <h1 class="titular temps"><div id="example-2"></div></h1>
                        <div class="destacat" style="background-image:url('{{ asset('img/destacat4.jpg') }}')"><img src="img/tr-d.png">
                            <a href="comunitat-entrevista.php">
                                <h2><p>Entrevistem<br><span>Nom Entrevistat</span></p></h2>
                            </a>
                        </div>
                    </div>
                    
                    <div class="col-md-6 col-xs-6 heightForum2">
                <div class="destacat" style="background-image:url('{{ asset('img/home-qualicat.png') }}')"><img src="img/tr-q.png">
                    <a href="serveis-qualicat.php">
                        <h2><p>Qualicat</p></h2>
                    </a>
                </div>
            </div>
            <div class="col-md-6 col-xs-6">
                <div class="destacat" style="background-image:url('{{ asset('img/home-lliga-correcat.png') }}')"><img src="img/tr-q.png">
                    <a href="serveis-correcat.php">
                        <h2><p>Lliga Correcat</p></h2>
                    </a>
                </div>
            </div>
                </div>    
            </div>
            <div class="clearfix"></div><!--//fi forum i noticies -->
            
            <div class="col-md-6 col-xs-12">
                <div class="destacat" style="background-image:url('{{ asset('img/home-en-xarxa.png') }}')"><img src="img/tr-d.png">
                    <a href="noticies-en-xarxa.php">
                        <h2><p>En xarxa</p></h2>
                    </a>
                </div>
            </div>
            
            
            
            <div class="col-md-3 col-xs-6">
                <div class="destacat" style="background-image:url('{{ asset('img/home-clinics.png') }}')"><img src="img/tr-q.png">
                    <a href="serveis-clinics.php">
                        <h2><p>Clínics QR2</p></h2>
                    </a>
                </div>
            </div>
            <div class="col-md-3 col-xs-6">
                <div class="destacat" style="background-image:url('{{ asset('img/home-ranquing.png') }}')"><img src="img/tr-q.png">
                    <a href="comunitat-ranking.php">
                        <h2><p>Rànking</p></h2>
                    </a>
                </div>
            </div>
            <div class="clearfix"></div><!--//fi accessos directes -->
            
            
            <!--sabatilles / entrevista / calendari -->
            <div class="col-md-6">
                <div class="row">
                    <div class="col-sm-12 heightScrollbox">
                        <div class="destacat" style="background-image:url('{{ asset('img/home-sabatilles-solidaries.png') }}')"><img src="img/tr-d.png">
                            <a href="serveis-sabatilles.php">
                                <h2><p>Sabatilles solidàries</p></h2>
                                <!--<div class="logo-sab"><img src="img/logo-sabatilles.png" alt="Sabatilles solidàries"></div>-->
                            </a>
                        </div>

                        <div class="destacat" style="background-image:url('{{ asset('img/destacat4.jpg') }}')"><img src="img/tr-d.png">
                            <a href="noticies.php">
                                <h2 class="entrev"><p>Noticies</p></h2>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>    
            </div>
            <div class="col-md-6">
                <div class="calendari">
                    <h1 class="mes">
                        <a href="javascript:;"><div class="col-xs-2"><img src="img/arrowleft.png" width="40" height="40"></div></a>
                        <div class="col-xs-8 text-center text-uppercase mescalend">Novembre</div>
                        <a href="javascript:;"><div class="col-xs-2 text-right"><img src="img/arrowright.png" width="40" height="40"></div></a>
                    </h1>
                    <div id="scrollbox3">
                       <a href="javascript:;"><div class="row calend">
                            <div class="col-xs-1 text-center tar"><strong>02</strong></div>
                            <div class="col-xs-11">
                                <span class="negre">XXVII Cursa de Muntanya pícnic Sant Antoni</span><br>
                                <span class="gris">Cursa de muntanya | 10.000  |  Barcelona</span>
                            </div>
                        </div></a>
                       <a href="javascript:;"><div class="row calend">
                            <div class="col-xs-1 text-center tar"><strong>02</strong></div>
                            <div class="col-xs-11">
                                <span class="negre">XXVII Cursa de Muntanya pícnic Sant Antoni</span><br>
                                <span class="gris">Cursa de muntanya | 10.000  |  Barcelona</span>
                            </div>
                        </div></a>
                        <a href="javascript:;"><div class="row calend">
                            <div class="col-xs-1 text-center tar"><strong>02</strong></div>
                            <div class="col-xs-11">
                                <span class="negre">XXVII Cursa de Muntanya pícnic Sant Antoni</span><br>
                                <span class="gris">Cursa de muntanya | 10.000  |  Barcelona</span>
                            </div>
                        </div></a>
                       <a href="javascript:;"><div class="row calend">
                            <div class="col-xs-1 text-center tar"><strong>02</strong></div>
                            <div class="col-xs-11">
                                <span class="negre">XXVII Cursa de Muntanya pícnic Sant Antoni</span><br>
                                <span class="gris">Cursa de muntanya | 10.000  |  Barcelona</span>
                            </div>
                        </div></a>
                        <a href="javascript:;"><div class="row calend">
                            <div class="col-xs-1 text-center tar"><strong>02</strong></div>
                            <div class="col-xs-11">
                                <span class="negre">XXVII Cursa de Muntanya pícnic Sant Antoni</span><br>
                                <span class="gris">Cursa de muntanya | 10.000  |  Barcelona</span>
                            </div>
                        </div></a>
                        <a href="javascript:;"><div class="row calend">
                            <div class="col-xs-1 text-center tar"><strong>02</strong></div>
                            <div class="col-xs-11">
                                <span class="negre">XXVII Cursa de Muntanya pícnic Sant Antoni</span><br>
                                <span class="gris">Cursa de muntanya | 10.000  |  Barcelona</span>
                            </div>
                        </div></a>
                        <a href="javascript:;"><div class="row calend">
                            <div class="col-xs-1 text-center tar"><strong>02</strong></div>
                            <div class="col-xs-11">
                                <span class="negre">XXVII Cursa de Muntanya pícnic Sant Antoni</span><br>
                                <span class="gris">Cursa de muntanya | 10.000  |  Barcelona</span>
                            </div>
                        </div></a>
                        <a href="javascript:;"><div class="row calend">
                            <div class="col-xs-1 text-center tar"><strong>02</strong></div>
                            <div class="col-xs-11">
                                <span class="negre">XXVII Cursa de Muntanya pícnic Sant Antoni</span><br>
                                <span class="gris">Cursa de muntanya | 10.000  |  Barcelona</span>
                            </div>
                        </div></a>
                        <a href="javascript:;"><div class="row calend">
                            <div class="col-xs-1 text-center tar"><strong>02</strong></div>
                            <div class="col-xs-11">
                                <span class="negre">XXVII Cursa de Muntanya pícnic Sant Antoni</span><br>
                                <span class="gris">Cursa de muntanya | 10.000  |  Barcelona</span>
                            </div>
                        </div></a>
                        <a href="javascript:;"><div class="row calend">
                            <div class="col-xs-1 text-center tar"><strong>02</strong></div>
                            <div class="col-xs-11">
                                <span class="negre">XXVII Cursa de Muntanya pícnic Sant Antoni</span><br>
                                <span class="gris">Cursa de muntanya | 10.000  |  Barcelona</span>
                            </div>
                        </div></a>
                        <a href="javascript:;"><div class="row calend">
                            <div class="col-xs-1 text-center tar"><strong>02</strong></div>
                            <div class="col-xs-11">
                                <span class="negre">XXVII Cursa de Muntanya pícnic Sant Antoni</span><br>
                                <span class="gris">Cursa de muntanya | 10.000  |  Barcelona</span>
                            </div>
                        </div></a>
                        <a href="javascript:;"><div class="row calend">
                            <div class="col-xs-1 text-center tar"><strong>02</strong></div>
                            <div class="col-xs-11">
                                <span class="negre">XXVII Cursa de Muntanya pícnic Sant Antoni</span><br>
                                <span class="gris">Cursa de muntanya | 10.000  |  Barcelona</span>
                            </div>
                        </div></a>
                    </div>
                </div>
                <h1 class="titular taronja"><p>Calendari curses</p></h1>
            </div>
            <div class="clearfix"></div><!--//fi sabatilles / entrevista / calendari -->

            
            <!--Llebres / entrenadors / Reptes  -->
            <div class="col-md-6">
                <div class="destacat" style="background-image:url('{{ asset('img/home-llebres.png') }}')"><img src="img/tr-d.png">
                    <a href="serveis-llebres.php">
                        <h2><p>LLebres</p></h2>
                    </a>
                </div>
            </div>
            
            <div class="col-md-6">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="destacat" style="background-image:url('{{ asset('img/home-entrenadors.png') }}')"><img src="img/tr-q.png">
                            <a href="serveis-entrenadors.php">
                                <h2><p>Entrenadors</p></h2>
                            </a>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="destacat" style="background-image:url('{{ asset('img/home-reptes.png') }}')"><img src="img/tr-q.png">
                            <a href="serveis-reptes.php">
                                <h2><p>Reptes</p></h2>
                            </a>
                        </div>
                    </div>
                </div>    
            </div>
            <div class="clearfix"></div>
            <!--///Llebres / entrenadors / Reptes  -->
                       
            
            <!--Proves / enllaços / galeria  -->
            <div class="col-md-6">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="destacat" style="background-image:url('{{ asset('img/destacat1.jpg') }}')"><img src="img/tr-q.png">
                            <a href="serveis-proves.php">
                                <h2><p>Proves materials</p></h2>
                            </a>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="destacat" style="background-image:url('{{ asset('img/home-enllacos.png') }}')"><img src="img/tr-q.png">
                            <a href="enllasos.php">
                                <h2><p>Enllaços</p></h2>
                            </a>
                        </div>
                    </div>
                </div>    
            </div>
            <div class="col-md-6">
                <div class="destacat" style="background-image:url('{{ asset('img/destacat2.jpg') }}')">
                    <ul class="bxslider">
                        <li style="background-image:url('{{ asset('img/destacat5.jpg') }}')"><img src="img/tr-d.png"></li>
                        <li style="background-image:url('{{ asset('img/destacat4.jpg') }}')"><img src="img/tr-d.png"></li>
                        <li style="background-image:url('{{ asset('img/destacat3.jpg') }}')"><img src="img/tr-d.png"></li>
                        <li style="background-image:url('{{ asset('img/destacat2.jpg') }}')"><img src="img/tr-d.png"></li>
                        <li style="background-image:url('{{ asset('img/destacat1.jpg') }}')"><img src="img/tr-d.png"></li>
                    </ul>
                    <a href="galeria.php">
                        <h2><p>Galeria</p></h2>
                    </a>
                </div>
            </div>
            <div class="clearfix"></div>
            <!--///Proves / enllaços / galeria  -->
     
       </div> 
    <!--</div>
     //container info-->
    
    <!-- Slider productes -->
    <div class="container-fluid bgnegre70">
        <div class="col-sm-12" style="padding:0 30px">
            <div class="titular-prods">Destacats de la botiga oficial</div>
    
           <!-- <ul class="bxslider2">-->
          <ul class="bxslider2">
                <a href="javascript:;" class="nounder"><li>
                    <div class="imatge-prod" style="background-image:url('{{ asset('img/prod1.jpg') }}')"><img src="img/tr-q.png"></div>
                    <div class="info-prod disponible">
                        <h4>Nom del producte</h4>
                        <span class="preu">10€</span> | <span class="estat">Disponible</span>
                    </div>
                </li></a>
                <a href="javascript:;" class="nounder"><li>
                    <div class="imatge-prod" style="background-image:url('{{ asset('img/prod2.jpg') }}')"><img src="img/tr-q.png"></div>
                        <div class="info-prod disponible">
                        <h4>Nom del producte</h4>
                        <span class="preu">100€</span> | <span class="estat">Disponible</span>
                    </div>
                </li></a>
                <a href="javascript:;" class="nounder"><li>
                    <div class="imatge-prod" style="background-image:url('{{ asset('img/prod3.jpg') }}')"><img src="img/tr-q.png"></div>
                    <div class="info-prod reservat">
                        <h4>Nom del producte</h4>
                        <span class="preu">10€</span> | <span class="estat reser">Reservat</span>
                    </div>
                </li></a>
                <a href="javascript:;" class="nounder"><li>
                    <div class="imatge-prod" style="background-image:url('{{ asset('img/prod4.jpg') }}')"><img src="img/tr-q.png"></div>
                    <div class="info-prod disponible">
                        <h4>Nom del producte</h4>
                        <span class="preu">100€</span> | <span class="estat">Disponible</span>
                    </div>
                </li></a>
                <a href="javascript:;" class="nounder"><li>
                    <div class="imatge-prod" style="background-image:url('{{ asset('img/prod5.jpg') }}')"><img src="img/tr-q.png"></div>
                    <div class="info-prod reservat">
                        <h4>Nom del producte</h4>
                        <span class="preu">10€</span> | <span class="estat reser">Reservat</span>
                    </div>
                </li></a>
            </ul>
    
        </div>
    </div>
     <!-- //Slider productes -->
    
@include('layouts.footer')
