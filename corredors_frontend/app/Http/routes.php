<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Pages with only information
Route::get('qui-som', 			'SectionController@whoweare'); 		// Show Who we are?
Route::get('en-xarxa', 			'SectionController@socialnetwork'); // Show Social


Route::get('abonat', 			'UserController@RegisterForm'); 	// Show subscriber form

Route::get('reptes', 			'ChallengesController@index'); 		// Show challenges
Route::get('clinicqr2', 		'ClinicQR2Controller@index'); 		// Show ClinicQR2
Route::get('sabatilles', 		'ShoesController@index'); 			// Show Shoes
Route::get('entrenadors', 		'TrainersController@index'); 		// Show Trainers

// Qualicat
Route::get('qualicat', 			'QualicatController@index'); 		// Show Qualicat

Route::get('llebres', 			'QualicatController@indexLlebres'); // Show Llebres
Route::get('proves-materials',	'TestMaterialController@index');  	// Show Test Material

// News
Route::get('noticies',			'NewsController@index');  			// Show News

// Community QR2
Route::get('ranking',				'RankingController@index');  		// Show Ranking
Route::get('comunitat-llista',		'UserController@allusers');  		// Show All users
Route::get('comunitat-entrevista',	'InterviewController@index');  		// Show All interviews


// Shop
Route::get('botiga',				'ShopController@index');  		// Show Shop
Route::get('botiga-cistella',		'ShopController@indexCart');  	// Show Cart
Route::get('botiga-pagament',		'ShopController@indexInvoice');  // Show Invoice


// Authentication routes...
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', ['as' => 'auth/logout', 'uses' => 'Auth\AuthController@getLogout']);


// Registration routes...
Route::post('auth/register', ['as' => 'auth/register', 'uses' => 'Auth\AuthController@postRegister']); 	// post register
Route::post('auth/login', ['as' =>'auth/login', 'uses' => 'Auth\AuthController@postLogin']); 			// post login


// only logged routes
Route::group(['middleware' => 'auth'], function () {


	Route::get('perfil', 		'UserController@Account');		// user account
	Route::get('editar-perfil', 'UserController@EditAccount');	// edit account

	Route::post('auth/edit', 			['as' =>'auth/edit', 'uses' => 'Usercontroller@update']);	// update data user
	Route::post('auth/editpassword', 	['as' =>'auth/editpassword', 'uses' => 'Usercontroller@editPassword']);	// edit user password


});