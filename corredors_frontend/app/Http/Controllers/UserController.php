<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User as User;

class UserController extends Controller
{
    /**
     * Display subscriber register form.
     *
     * @return \Illuminate\Http\Response
     */
    public function RegisterForm()
    {
        return view('section.subscriber');
    }


    /**
     * Display account.
     *
     * @return \Illuminate\Http\Response
     */
    public function Account()
    {
        return view('user.account');
    }


    /**
     * Display edit account.
     *
     * @return \Illuminate\Http\Response
     */
    public function EditAccount()
    {
        return view('user.editaccount');
    }


    /**
     * Display All users.
     *
     * @return \Illuminate\Http\Response
     */
    public function AllUsers()
    {
        $users = User::all();
        return view('section.communityqr2.allusers')->with('users',$users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
