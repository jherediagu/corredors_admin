<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Comissions as Comissions;



/**
 *
 * This controller is for sections with information only
 *
 */


class SectionController extends Controller
{
    /**
     * Display who we are section.
     *
     * @return \Illuminate\Http\Response
     */
    public function whoweare()
    {
        $comissions = Comissions::all();

        return view('section.whoweare')->with('comissions', $comissions);
    }

    /**
     * Display who we are section.
     *
     * @return \Illuminate\Http\Response
     */
    public function socialnetwork()
    {
        return view('section.news.socialnetwork');
    }

}
