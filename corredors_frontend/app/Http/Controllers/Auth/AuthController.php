<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    private $redirectTo = '/';


    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {

        if (isset($data['IBAN_country_code'])) {

            $account_type = 1;

        }else {

            $account_type = 0;
        }

        return User::create([
            'name'                  => $data['name'],
            'email'                 => $data['email'],
            //new fields
            'nickname'              => $data['nickname'],
            'lastname'              => $data['lastname'],
            'birthday'              => $data['birthday'],
            'address'               => $data['address'],
            'dni'                   => $data['dni'],
            'city'                  => $data['city'],
            'postal_code'           => $data['postal_code'],
            'province'              => $data['province'],
            'telephone'             => $data['telephone'],
            'titular_name'          => $data['titular_name'],
            'titular_lastname'      => $data['titular_lastname'],
            'IBAN_country_code'     => $data['IBAN_country_code'],
            'IBAN_cheque_number'    => $data['IBAN_cheque_number'],
            'IBAN_bank_code'        => $data['IBAN_bank_code'],
            'IBAN_sort_code'        => $data['IBAN_sort_code'],
            'IBAN_account_number'   => $data['IBAN_account_number'],
            'concept'               => $data['concept'],
            'account_type'          => $account_type,

            //password
            'password'              => bcrypt($data['password']),
        ]);
    }
}
