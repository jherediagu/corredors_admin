<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Challenges extends Model
{
   /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'challenges';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id','season_challenge', 'name', 'description', 'person_challange', 'start_date', 'total_time', 'price_registered', 'price_subscribed', 'limit_date', 'link', 'image' ,'created_at'];

}
