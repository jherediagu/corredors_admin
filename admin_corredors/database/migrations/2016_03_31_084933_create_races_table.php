<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('races', function (Blueprint $table) {

            // Races
            $table->increments('id');
            $table->string('name');
            $table->string('location');
            $table->text('description');

            $table->bigInteger('organization'); // table
            $table->bigInteger('type'); // table
            $table->date('date');

            // Timestamps ( created_at / updated_at )
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('races');
    }
}
