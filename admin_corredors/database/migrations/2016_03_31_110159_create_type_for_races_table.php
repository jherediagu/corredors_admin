<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTypeForRacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('races_type', function (Blueprint $table) {

            // Races types ( ex: 5km, 10km, 1000m, etc... )
            $table->increments('id');
            $table->string('name');
            $table->text('description');

            // Timestamps ( created_at / updated_at )
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('races_type');
    }
}
