<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableClinicQr2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clinicqr2', function (Blueprint $table) {

            // Clinicqr2
            $table->increments('id');
            $table->string('title_clinic');
            $table->string('title');
            $table->text('description');
            $table->dateTime('date');
            $table->time('hour');
            $table->string('place');
            $table->bigInteger('number_seats');
            
            // Timestamps ( created_at / updated_at )
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('clinicqr2');
    }
}
