<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('commissions', function (Blueprint $table) {

            // Comissions
            $table->increments('id');
            $table->string('commission');
            $table->text('objective');
            $table->text('members');

            // Timestamps ( created_at / updated_at )
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('commissions');
    }
}
