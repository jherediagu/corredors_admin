<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersCorredorsWithPaymentInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::create('users_corredors', function (Blueprint $table) {

            // Account
            $table->increments('id');
            $table->string('email')->unique();
            $table->string('nickname')->unique();
            $table->string('password', 60);

            // Personal 
            $table->string('name');
            $table->string('lastname');
            $table->date('birthday');
            $table->string('dni');
            $table->string('address');
            $table->string('city');
            $table->integer('postal_code');
            $table->string('province');
            $table->integer('telephone');
            
            // Info payment
            $table->string('titular_name');
            $table->string('titular_lastname');
            $table->integer('IBAN_country_code');
            $table->integer('IBAN_cheque_number');
            $table->integer('IBAN_bank_code');
            $table->integer('IBAN_sort_code');
            $table->integer('IBAN_account_number');
            $table->string('concept');

            // Others
            $table->tinyInteger('legal');
            $table->tinyInteger('account_type');

            // Token & timestamps ( created_at / updated_at )
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users_corredors');
    }
}
