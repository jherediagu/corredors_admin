<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableChallengesUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('challenges', function (Blueprint $table) {

            // Comissions
            $table->increments('id');
            $table->string('season_challenge');
            $table->string('name');
            $table->text('description');
            $table->string('person_challange');
            $table->dateTime('start_date');
            $table->string('total_time');
            $table->decimal('price_registered', 5,2);
            $table->decimal('price_subscribed', 5,2);
            $table->text('limit_date');
            $table->string('link');
            $table->string('image');

            // Timestamps ( created_at / updated_at )
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('challenges');
    }
}



/*

temporada repte     -> season_challenge
nom del repte       -> name
descripcio_repte    -> description
persona_repte       -> person_challange  
data_inici          -> start_date
durada del repte    -> total_time
preu repte associat -> price_registered
preu repte registrat-> price_subscribed
termini inscripcio  -> limit_date
link                -> link
image               -> image

*/

