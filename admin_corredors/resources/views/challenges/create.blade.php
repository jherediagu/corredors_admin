@include('dashboard.layouts.header')
@include('dashboard.layouts.sidemenu')
    <script type="text/javascript">
        $( document ).ready(function() {

            var editor = new Simditor({
              textarea: $('#editor')
              //optional options
            });

            var editor2 = new Simditor({
              textarea: $('#editor2')
              //optional options
            });

            Dropzone.options.myAwesomeDropzone = {
              url: "{{ url('uploadimage-challenge')}}",
              paramName: "file",
              maxFiles: 1,
              maxFilesize: 2, // MB
         
            };
           
           });
    </script>
    
<main id="main-container">

<!-- Page Header -->
    <div class="content bg-gray-lighter">
        <div class="row items-push">
            <div class="col-sm-7">
                <h1 class="page-heading">
                    Crear repte<small> </small>
                </h1>
            </div>
            <div class="col-sm-5 text-right hidden-xs">
                <ol class="breadcrumb push-10-t">
                    <li>Serveis</li>
                    <li>reptes</li>
                    <li><a class="link-effect" href="">Crear repte</a></li>
                </ol>
            </div>
        </div>
    </div>
<!-- Page Header -->

<!-- Page Content -->

<form method="POST" action="{{ URL::to('/') }}/challenges">

    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="block">
                    <ul class="nav nav-tabs nav-justified" data-toggle="tabs">
                        <li class="active"><a href="#btabs-static-justified-home"><i class="fa fa-user"></i> Dades del repte</a></li>
                        <li><a href="#btabs-static-justified-profile"><i class="fa fa-pencil"></i> Tens un repte amb ..</a></li>
                        <li><a href="#btabs-static-justified-settings"><i class="fa fa-cog"></i> Imatge</a></li>
                    </ul>
                
                <div class="block-content tab-content">
                    
                    <div class="tab-pane active" id="btabs-static-justified-home">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="block">
                                <div class="block-header">
                                    <h3 class="block-title">dades del repte</h3>
                                </div>
                                <div class="block-content block-content-narrow">
                                    <div class="form-horizontal push-10-t">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <div class="form-material">
                                                    <input class="form-control" type="text" id="material-text" name="season_challenge" value="">
                                                    <label for="material-text">Temporada del repte</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <div class="form-material">
                                                    <input class="form-control" type="text" id="material-email" name="name" value="">
                                                    <label for="material-email">Nom del repte</label>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <div class="form-material">
                                                    <br><textarea id="editor" autofocus name="description"></textarea>
                                                    <label for="material-email">Descripció del repte</label>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    </div>
                    
                    <div class="tab-pane" id="btabs-static-justified-profile">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="block">
                                <div class="block-header">
                                    <h3 class="block-title">Tens un repte amb ...</h3>
                                </div>
                                <div class="block-content block-content-narrow">
                                    <div class="form-horizontal push-10-t">

                                        <div class="form-group">
                                            <div class="col-xs-12 col-md-12">
                                                <div class="form-material">
                                                    <input class="form-control" type="text" id="material-gridf" name="person_challange" value="">
                                                    <label for="material-gridf">Nom</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-xs-5 col-md-5">
                                                <div class="form-material">
                                                    <input class="form-control" type="date" id="material-gridf" name="start_date" value="" placeholder="00/00/0000">
                                                    <label for="material-gridf">Data d'inici</label>
                                                </div>
                                            </div>
                                            <div class="col-xs-7 col-md-7">
                                                <div class="form-material">
                                                    <input class="form-control" type="text" id="material-gridl" name="total_time" value="">
                                                    <label for="material-gridl">Durada del repte</label>
                                                </div>
                                            </div>
                                        </div> 
                                        <div class="form-group">
                                            <div class="col-xs-3 col-md-3">
                                                <div class="form-material">
                                                    <input class="form-control" type="number" step="0.01" id="material-gridf" name="price_registered" value="">
                                                    <label for="material-gridf">Preu Registrat</label>
                                                </div>
                                            </div>
                                            <div class="col-xs-3 col-md-3">
                                                <div class="form-material">
                                                    <input class="form-control" type="number"  step="0.01" id="material-gridl" name="price_subscribed" value="">
                                                    <label for="material-gridl">Preu Abonat</label>
                                                </div>
                                            </div>
                                            <div class="col-xs-6 col-md-6">
                                                <div class="form-material">
                                                    <input class="form-control" type="text" id="material-gridl" name="link" value="">
                                                    <label for="material-gridl">Enllaç</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <div class="form-material">
                                                    <br><textarea id="editor2" autofocus name="limit_date"></textarea>

                                                    <label for="material-gridf">Termini d'inscripció</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    </div>
                    
                    
                    <div class="tab-pane" id="btabs-static-justified-settings">

                    <!-- DropzoneJS and Tags Input -->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- DropzoneJS -->
                            <!-- For more info and examples you can check out http://www.dropzonejs.com/#usage -->
                            <h2 class="content-heading">Afegir l'imatge del usuari/repte</h2>
                                <div class="dropzone" id="myAwesomeDropzone"></div><br><br>
                            <!-- END DropzoneJS -->
                        </div>
                
                    </div>
                    <!-- END DropzoneJS and Tags Input -->
                    </div>
                    
                </div>
            </div>
    </div>
    
    <div class="col-sm-9" style="padding-bottom: 30px">
        <button class="btn btn-md btn-primary" type="submit">Crear Repte</button>
    </div>

</div>

<input type="hidden" name="_token" value="{{ csrf_token() }}">
</form>
<!-- Page Content -->

</main>



@include('dashboard.layouts.footer')

