@include('dashboard.layouts.header')
@include('dashboard.layouts.sidemenu')

    
<main id="main-container">

<!-- Page Header -->
	<div class="content bg-gray-lighter">
	    <div class="row items-push">
	        <div class="col-sm-7">
	            <h1 class="page-heading">
	                {{ $user->nickname }} <small> {{$user->lastname}}, {{ $user->name }}</small>
	            </h1>
	        </div>
	        <div class="col-sm-5 text-right hidden-xs">
	            <ol class="breadcrumb push-10-t">
	                <li>Usuari</li>
	                <li><a class="link-effect" href="">{{ $user->nickname }}</a></li>
	            </ol>
	        </div>
	    </div>
	</div>
<!-- Page Header -->

<!-- Page Content -->

<form method="POST" action="{{ URL::to('/') }}/users/registered/update/{{ $user->id }}">

	<div class="content">
		<div class="row">
            <div class="col-md-12">
                <div class="block">
                    <ul class="nav nav-tabs nav-justified" data-toggle="tabs">
                        <li class="active"><a href="#btabs-static-justified-home"><i class="fa fa-user"></i> Dades del compte</a></li>
                        <li><a href="#btabs-static-justified-profile"><i class="fa fa-pencil"></i> Dades personals</a></li>
                        <li><a href="#btabs-static-justified-settings"><i class="fa fa-cog"></i> Dades de pagament</a></li>
                        <li><a href="#btabs-static-justified-opcions"><i class="fa fa-cog"></i> Opcions usuari</a></li>
                    </ul>
                
                <div class="block-content tab-content">
                    
                    <div class="tab-pane active" id="btabs-static-justified-home">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="block">
                                <div class="block-header">
                                    <h3 class="block-title">dades del compte</h3>
                                </div>
                                <div class="block-content block-content-narrow">
                                    <div class="form-horizontal push-10-t">
                                        <div class="form-group">
                                            <div class="col-sm-9">
                                                <div class="form-material">
                                                    <input class="form-control" type="text" id="material-text" name="nickname" value="{{ $user->nickname }}">
                                                    <label for="material-text">Nickname</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-9">
                                                <div class="form-material">
                                                    <input class="form-control" type="email" id="material-email" name="email" value="{{ $user->email }}">
                                                    <label for="material-email">Email</label>
                                                </div>
                                            </div>
										</div>
										<div class="form-group has-warning">
                                            <div class="col-sm-9">
                                                <div class="form-material">
                                                <?php if ($user->account_type == 0){ $account = "Registrat"; }else { $account = "Abonat";} ?>
                                                                                           
                                                    <input class="form-control" type="text" id="material-warning" value="{{ $account }}" disabled>
                                                    <label for="material-warning">Tipus de usuari</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    </div>
                    
                    <div class="tab-pane" id="btabs-static-justified-profile">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="block">
                                <div class="block-header">
                                    <h3 class="block-title">dades personals</h3>
                                </div>
                                <div class="block-content block-content-narrow">
                                    <div class="form-horizontal push-10-t">

                                        <div class="form-group">
                                            <div class="col-xs-6">
                                                <div class="form-material">
                                                    <input class="form-control" type="text" id="material-gridf" name="name" value="{{ $user->name }}">
                                                    <label for="material-gridf">Nom</label>
                                                </div>
                                            </div>
                                            <div class="col-xs-6">
                                                <div class="form-material">
                                                    <input class="form-control" type="text" id="material-gridl" name="lastname" value="{{ $user->lastname }}">
                                                    <label for="material-gridl">Cognoms</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-xs-3">
                                                <div class="form-material">
                                                    <input class="form-control" type="date" id="material-gridf" name="birthday" value="{{ $user->birthday }}">
                                                    <label for="material-gridf">Data de naixement</label>
                                                </div>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="form-material">
                                                    <input class="form-control" type="text" id="material-gridl" name="dni" value="{{ $user->dni }}">
                                                    <label for="material-gridl">DNI</label>
                                                </div>
                                            </div>
                                           	<div class="col-xs-5">
                                                <div class="form-material">
                                                    <input class="form-control" type="text" id="material-gridl" name="city" value="{{ $user->city }}">
                                                    <label for="material-gridl">Població</label>
                                                </div>
                                            </div>
                                        </div> 
                                        <div class="form-group">
                                            <div class="col-xs-8">
                                                <div class="form-material">
                                                    <input class="form-control" type="text" id="material-gridf" name="address" value="{{ $user->address }}">
                                                    <label for="material-gridf">Direcció</label>
                                                </div>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="form-material">
                                                    <input class="form-control" type="number" id="material-gridl" name="postal_code" value="{{ $user->postal_code }}">
                                                    <label for="material-gridl">Codi postal</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-xs-7">
                                                <div class="form-material">
                                                    <input class="form-control" type="text" id="material-gridf" name="province" value="{{ $user->province }}">
                                                    <label for="material-gridf">Provincia</label>
                                                </div>
                                            </div>
                                            <div class="col-xs-5">
                                                <div class="form-material">
                                                    <input class="form-control" type="number" id="material-gridl" name="telephone" value="{{ $user->telephone }}">
                                                    <label for="material-gridl">Telèfon</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    </div>
                    
                    
                    <div class="tab-pane" id="btabs-static-justified-settings">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="block">
                                <div class="block-header">
 
                                    <h3 class="block-title">dades de pagament</h3>

                                </div>
                                <div class="block-content block-content-narrow">
                                    <div class="form-horizontal push-10-t">

                                        <div class="form-group">
                                            <div class="col-xs-6">
                                                <div class="form-material">
                                                    <input class="form-control" type="text" id="material-gridf" name="titular_name" value="{{ $user->titular_name }}">
                                                    <label for="material-gridf">Nom del titular</label>
                                                </div>
                                            </div>
                                            <div class="col-xs-6">
                                                <div class="form-material">
                                                    <input class="form-control" type="text" id="material-gridl" name="titular_lastname" value="{{ $user->titular_lastname }}">
                                                    <label for="material-gridl">Cognoms del titular</label>
                                                </div>
                                            </div>
                                        </div>

                                    <h3 class="block-title">Número de compte ( IBAN )</h3>

                                        <div class="form-group has-info" style="padding-top: 30px">
                                            <div class="col-sm-2">
                                                <div class="form-material">
                                                    <input class="form-control" type="number" id="material-info" name="IBAN_country_code" value="{{ $user->IBAN_country_code }}">
                                                    <label for="material-info">IBAN</label>
                                                </div>
                                            </div>
                                            <div class="col-sm-2">
                                                <div class="form-material">
                                                    <input class="form-control" type="number" id="material-info" name="IBAN_cheque_number" value="{{ $user->IBAN_cheque_number }}">
                                                    <label for="material-info">Entitat</label>
                                                </div>
                                            </div>
                                            <div class="col-sm-2">
                                                <div class="form-material">
                                                    <input class="form-control" type="number" id="material-info" name="IBAN_bank_code" value="{{ $user->IBAN_bank_code }}">
                                                    <label for="material-info">Oficina</label>
                                                </div>
                                            </div>
                                            <div class="col-sm-1">
                                                <div class="form-material">
                                                    <input class="form-control" type="number" id="material-info" name="IBAN_sort_code" value="{{ $user->IBAN_sort_code }}">
                                                    <label for="material-info">D.C</label>
                                                </div>
                                            </div>
                                            <div class="col-sm-5">
                                                <div class="form-material">
                                                    <input class="form-control" type="number" id="material-info" name="IBAN_account_number" value="{{ $user->IBAN_account_number }}">
                                                    <label for="material-info">Número de compte</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <div class="form-material">
                                                    <input class="form-control" type="text" id="material-gridf" name="concept" value="{{ $user->concept }}">
                                                    <label for="material-gridf">Concepte</label>
                                                </div>
                                            </div>
                                        </div> 

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                    

                    <div class="tab-pane" id="btabs-static-justified-opcions">
                        <h4 class="font-w300 push-15">Opcions usuari</h4>
                            <p>...</p>
                        </div>
                    </div>
                </div>
            </div>
	</div>
	
	<div class="col-sm-9">
	    <button class="btn btn-md btn-primary" type="submit">Actualitzar</button>
	</div>

</div>

<input type="hidden" name="_token" value="{{ csrf_token() }}">
</form>
<!-- Page Content -->

</main>



@include('dashboard.layouts.footer')
