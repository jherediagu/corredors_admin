@include('dashboard.layouts.header')
@include('dashboard.layouts.sidemenu')

    
<main id="main-container">

<!-- Page Header -->
	<div class="content bg-gray-lighter">
	    <div class="row items-push">
	        <div class="col-sm-7">
	            <h1 class="page-heading">
	                Usuaris Registrats<small> llistat de usuaris registrats sense abonament</small>
	            </h1>
	        </div>
	        <div class="col-sm-5 text-right hidden-xs">
	            <ol class="breadcrumb push-10-t">
	                <li>Usuaris</li>
	                <li><a class="link-effect" href="">Usuaris Registrats</a></li>
	            </ol>
	        </div>
	    </div>
	</div>
<!-- Page Header -->

<!-- Page Content -->
	<div class="content">
		
			<a href="{{ URL::to('/') }}/users/create"><button class="btn btn-success push-10-r push-10" type="button"><i class="fa fa-plus"></i> Afegir usuari</button></a>

		    <div class="block">

	        <div class="block-content">

	            <table id="datatable" class="table table-striped table-hover">
	            <thead>
	                <tr>
	                    <th style="width: 4%;">id</th>
	                    <th style="width: 10%;">nickname</th>
	                    <th>nom</th>
	                    <th>email</th>
	                    <th>població</th>
	                    <th style="width: 10%;">telèfon</th>
	                    <th style="width: 15%;">data registre</th>
	                    <th class="text-center " style="width: 7%;"></th>
	                </tr>
	            </thead>
	            @foreach ($registered as $user)
		            <tr>
		                <td>{{ $user->id }}</td>                
		                <td><b>{{ $user->nickname }}</b></td>
						<td><b>{{ $user->name }}</b></td>
		                <td><b>{{ $user->email }}</b></td>
		                <td>{{ $user->city }}</td>
		                <td>{{ $user->telephone }}</td>
		                <td>{{ $user->created_at }}</td>

		                <td class="text-center">
		                    <a class='btn btn-default btn-xs' data-toggle="tooltip" data-placement="top" title="Editar" href="{{ URL::to('/') }}/users/registered/edit/{{$user->id}}">
		                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
		                    </a>
		                    <a class='btn btn-default btn-xs' data-toggle="tooltip" data-placement="top" title="Eliminar" href="{{ URL::to('/') }}/users/registered/delete/{{$user->id}}">
		                        <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
		                    </a>
		            
		                </td>
		            </tr>
	            @endforeach
	        </table>

	        </div>
	    </div>

	</div>

<!-- Page Content -->

</main>



@include('dashboard.layouts.footer')
