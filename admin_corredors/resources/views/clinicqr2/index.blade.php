@include('dashboard.layouts.header')
@include('dashboard.layouts.sidemenu')

    
<main id="main-container">

<!-- Page Header -->
	<div class="content bg-gray-lighter">
	    <div class="row items-push">
	        <div class="col-sm-7">
	            <h1 class="page-heading">
	                Clínics QR2<small> llistat de Clínics QR2</small>
	            </h1>
	        </div>
	        <div class="col-sm-5 text-right hidden-xs">
	            <ol class="breadcrumb push-10-t">
	                <li>Serveis</li>
	                <li><a class="link-effect" href="">Clínics QR2</a></li>
	            </ol>
	        </div>
	    </div>
	</div>
<!-- Page Header -->

<!-- Page Content -->
	<div class="content">
		
			<a href="{{ URL::to('/') }}/clinicqr2/create"><button class="btn btn-success push-10-r push-10" type="button"><i class="fa fa-plus"></i> Afegir Clínics QR2</button></a>

		    <div class="block">

	        <div class="block-content">

	            <table id="datatable" class="table table-striped table-hover">
	            <thead>
	                <tr>
	                    <th style="width: 4%;">id</th>
	                    <th style="width: 18%;">Títol</th>
	                    <th>Data</th>
	                    <th>Hora</th>
	                    <th style="width: 12%;">Lloc</th>
						<th style="width: 13%;">Nº Places</th>
	                    <th class="text-center " style="width: 7%;"></th>
	                </tr>
	            </thead>
	            @foreach ($clinicqr2 as $clinicqr2)
		            <tr>
		                <td>{{ $clinicqr2->id }}</td>                
						<td><b>{{ $clinicqr2->title }}</b></td>
		                <td><b>{{ $clinicqr2->date }}</b></td>
		                <td>{{ $clinicqr2->hour }}</td>
		                <td>{{ $clinicqr2->place }}</td>
		                <td>{{ $clinicqr2->number_seats }}</td>

		                <td class="text-center">
		                    <a class='btn btn-default btn-xs' data-toggle="tooltip" data-placement="top" title="Editar" href="{{ URL::to('/') }}/clinicqr2/{{$clinicqr2->id}}/edit">
		                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
		                    </a>
		                    <a class='btn btn-default btn-xs' data-toggle="tooltip" data-placement="top" title="Eliminar" href="{{ URL::to('/') }}/clinicqr2/delete/{{$clinicqr2->id}}">
		                        <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
		                    </a>
		            
		                </td>
		            </tr>
	            @endforeach
	        </table>

	        </div>
	    </div>

	</div>

<!-- Page Content -->

</main>



@include('dashboard.layouts.footer')
