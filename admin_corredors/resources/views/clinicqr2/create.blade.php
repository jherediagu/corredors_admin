@include('dashboard.layouts.header')
@include('dashboard.layouts.sidemenu')
    <script type="text/javascript">
        $( document ).ready(function() {

            var editor = new Simditor({
              textarea: $('#editor')
              //optional options
            });

            var editor2 = new Simditor({
              textarea: $('#editor2')
              //optional options
            });
           
           });
    </script>
    
<main id="main-container">

<!-- Page Header -->
    <div class="content bg-gray-lighter">
        <div class="row items-push">
            <div class="col-sm-7">
                <h1 class="page-heading">
                    Crear Clínics QR2<small> </small>
                </h1>
            </div>
            <div class="col-sm-5 text-right hidden-xs">
                <ol class="breadcrumb push-10-t">
                    <li>Serveis</li>
                    <li>reptes</li>
                    <li><a class="link-effect" href="">Crear Clínics QR2</a></li>
                </ol>
            </div>
        </div>
    </div>
<!-- Page Header -->

<!-- Page Content -->

<form method="POST" action="{{ URL::to('/') }}/clinicqr2">

    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="block">
                    <ul class="nav nav-tabs nav-justified" data-toggle="tabs">
                        <li class="active"><a href="#btabs-static-justified-home"><i class="fa fa-user"></i> Dades del Clínics QR2</a></li>
                        <li><a href="#btabs-static-justified-profile"><i class="fa fa-pencil"></i> Data,lloc i imatge</a></li>
                    </ul>
                
                <div class="block-content tab-content">
                    
                    <div class="tab-pane active" id="btabs-static-justified-home">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="block">
                                <div class="block-header">
                                    <h3 class="block-title">Dades del Clínics QR2</h3>
                                </div>
                                <div class="block-content block-content-narrow">
                                    <div class="form-horizontal push-10-t">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <div class="form-material">
                                                    <input class="form-control" type="text" id="material-text" name="title_clinic" value="">
                                                    <label for="material-text">Títol del Clínic QR2</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <div class="form-material">
                                                    <input class="form-control" type="text" id="material-email" name="title" value="">
                                                    <label for="material-email">Títol principal</label>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <div class="form-material">
                                                    <br><textarea id="editor" autofocus name="description"></textarea>
                                                    <label for="material-email">Descripció del Clínic QR2</label>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    </div>
                    
                    <div class="tab-pane" id="btabs-static-justified-profile">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="block">
                                <div class="block-header">
                                    <h3 class="block-title">Data, lloc i imatge</h3>
                                </div>
                                <div class="block-content block-content-narrow">
                                    <div class="form-horizontal push-10-t">
                                        <div class="form-group">
                                            <div class="col-xs-6 col-md-6">
                                                <div class="form-material">
                                                    <input class="form-control" type="date" id="material-gridf" name="date" value="" placeholder="00/00/0000">
                                                    <label for="material-gridf">Data d'inici</label>
                                                </div>
                                            </div>
                                            <div class="col-xs-3 col-md-3">
                                                <div class="form-material">
                                                    <input class="form-control" type="time" id="material-gridl" name="hour" value="" placeholder="00:00">
                                                    <label for="material-gridl">Hora del Clínic QR2</label>
                                                </div>
                                            </div>
                                            <div class="col-xs-3 col-md-3">
                                                <div class="form-material">
                                                    <input class="form-control" type="number" id="material-gridl" name="number_seats" value="">
                                                    <label for="material-gridl">Número de Places</label>
                                                </div>
                                            </div>
                                        </div> 

                                        <div class="form-group">
                                            <div class="col-xs-12 col-md-12">
                                                <div class="form-material">
                                                    <input class="form-control" type="text" id="material-gridf" name="place" value="">
                                                    <label for="material-gridf">Lloc de trobada</label>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    </div>
                    
                </div>
            </div>
    </div>
    
    <div class="col-sm-9" style="padding-bottom: 30px">
        <button class="btn btn-md btn-primary" type="submit">Crear Clínic QR2</button>
    </div>

</div>

<input type="hidden" name="_token" value="{{ csrf_token() }}">
</form>
<!-- Page Content -->

</main>



@include('dashboard.layouts.footer')

