@include('dashboard.layouts.header')
@include('dashboard.layouts.sidemenu')

    
<main id="main-container">

<!-- Page Header -->
	<div class="content bg-gray-lighter">
	    <div class="row items-push">
	        <div class="col-sm-7">
	            <h1 class="page-heading">
	                Crear Cursa<small> </small>
	            </h1>
	        </div>
	        <div class="col-sm-5 text-right hidden-xs">
	            <ol class="breadcrumb push-10-t">
	                <li>Curses</li>
                    <li>Gestió de Curses</li>
	                <li><a class="link-effect" href="">Crear Cursa</a></li>
	            </ol>
	        </div>
	    </div>
	</div>
<!-- Page Header -->

<!-- Page Content -->

<form method="POST" action="{{ URL::to('/') }}/races">

	<div class="content">
		<div class="row">
            <div class="col-md-12">
                <div class="block">
                
                <div class="block-content tab-content">
                    
                    <div class="tab-pane active" id="btabs-static-justified-home">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="block">
                                <div class="block-header">
                                </div>
                                <div class="block-content block-content-narrow">
                                    <div class="form-horizontal push-10-t">
                                        <div class="form-group">
                                            <div class="col-sm-9">
                                                <div class="form-material">
                                                    <input class="form-control" type="text" id="material-text" name="name" value="" placeholder="nom de la cursa">
                                                    <label for="material-text">Nom de la Cursa</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-9">
                                                <div class="form-material">
                                                    <input class="form-control" type="text" id="location" name="" value="" placeholder="nom de la cursa">
                                                    <label for="material-text">Localització :<small> S'autocompleta a mesura que vas escribint</small></label>
                                                </div>
                                            </div>
                                        </div>


                                          <div class="form-group">
                                            <div class="col-xs-9">         
                                        <div class="form-material">
                                        <select name="location" id="content" class="js-select2 form-control">
                                        <option></option>
                                        </select>
                                        <label for="example2-select2">Autoselector</label>

                                        </div>
                                        </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-xs-9">
                                                <div class="form-material">
                                                    {!! Form::select('type', $types , null, ['class' => 'js-select2 form-control']) !!}
                                                    <label for="example2-select2">Tipus de cursa</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-xs-9">
                                                <div class="form-material">
                                                    {!! Form::select('organization', $organizations , null, ['class' => 'js-select2 form-control']) !!}
                                                    <label for="example2-select2">Campionat / agrupació / Lliga</label>
                                                </div>
                                            </div>
                                        </div>

                                    <div class="form-group">
                                        <div class="col-xs-6">
                                                <div class="form-material">
                                                    <input class="form-control" type="date" id="material-gridf" name="date" value="">
                                                    <label for="material-gridf">Data de la cursa</label>
                                                </div>
                                        </div>
                                    </div>

                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <div class="form-material">
                                                    <textarea class="form-control" id="material-textarea-small" name="description" rows="3" placeholder="Escriu el objectiu"></textarea>
                                                    <label for="material-textarea-small">Descripció</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    </div>
                
                </div>
            </div>
	</div>
	
	<div class="col-sm-9">
	    <button class="btn btn-md btn-primary" type="submit">Crear cursa</button>
	</div>

</div>

<input type="hidden" name="_token" value="{{ csrf_token() }}">
</form>


<script type="text/javascript" >

 $("#content").select2({
      placeholder: "Escriu la localitat"
    });

    
$(document).ready(function() {

  var timeoutID = null;

 
  function findLocation(str) {
    console.log('search: ' + str);


 $.ajax({
            url: 'https://maps.googleapis.com/maps/api/place/autocomplete/json?input='+str+'&components=country:ES&types=(cities)&language=ca_CA&key=AIzaSyD_9cfZGraf0IZr0NuOTkZkAsI1ij9u5F8', 
            type: 'GET', 
    crossDomain: true,
    dataType: 'json',
            cache: false,
             success: function(response) { 
            console.log(JSON.parse(JSON.stringify(response.predictions)));
                                 // Limpiamos el select

                    var cursos = $("#content");
                    cursos.find('option').remove();

                    $(response.predictions).each(function(i, v){ // indice, valor
                        cursos.append('<option value="' + v.description + '">' + v.description + '</option>');
                    })
 },
    error: function() { alert('Failed!'); },
         
        });  


  }

  $('#location').keyup(function() {
    clearTimeout(timeoutID);
    var $target = $(this);
    timeoutID = setTimeout(function() { findLocation($target.val()); }, 100); 
  });

});

</script>
<!-- Page Content -->
<!-- 
https://maps.googleapis.com/maps/api/geocode/json?latlng=40.714224,-73.961452&location_type=ROOFTOP&result_type=street_address&key=YOUR_API_KEY

https://maps.googleapis.com/maps/api/place/autocomplete/json?input=Vict&components=country:ES&types=(cities)&language=pt_BR&key=AIzaSyD_9cfZGraf0IZr0NuOTkZkAsI1ij9u5F8


https://maps.googleapis.com/maps/api/geocode/json?address=santa+cruz&components=country:ES&key=AIzaSyD_9cfZGraf0IZr0NuOTkZkAsI1ij9u5F8

AIzaSyD_9cfZGraf0IZr0NuOTkZkAsI1ij9u5F8


https://maps.googleapis.com/maps/api/place/autocomplete/json?input=Ripoll&components=country:ES&types=(cities)&language=es_ES&key=AIzaSyD_9cfZGraf0IZr0NuOTkZkAsI1ij9u5F8
 -->
</main>

@include('dashboard.layouts.footer')
