@include('dashboard.layouts.header')
@include('dashboard.layouts.sidemenu')

    
<main id="main-container">

<!-- Page Header -->
	<div class="content bg-gray-lighter">
	    <div class="row items-push">
	        <div class="col-sm-7">
	            <h1 class="page-heading">
	                {{ $race_type->name }} <small></small>
	            </h1>
	        </div>
	        <div class="col-sm-5 text-right hidden-xs">
	            <ol class="breadcrumb push-10-t">
	                <li>Curses</li>
	                <li><a class="link-effect" href="">{{ $race_type->name }}</a></li>
	            </ol>
	        </div>
	    </div>
	</div>
<!-- Page Header -->

<!-- Page Content -->

<form method="POST" action="{{ URL::to('/') }}/race_types/{{ $race_type->id }}">
<input name="_method" type="hidden" value="PUT">
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="block">
            
                
                <div class="block-content tab-content">
                    
                    <div class="tab-pane active" id="btabs-static-justified-home">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="block">
                                <div class="block-header">
                                </div>
                                <div class="block-content block-content-narrow">
                                    <div class="form-horizontal push-10-t">
                                        <div class="form-group">
                                            <div class="col-sm-9">
                                                <div class="form-material">
                                                    <input class="form-control" type="text" id="material-text" name="name" value="{{ $race_type->name }}" placeholder="nom del tipus de cursa">
                                                    <label for="material-text">Nom</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <div class="form-material">
                                                    <textarea class="form-control" id="material-textarea-small" name="objective" rows="3" placeholder="Escriu descripció">
                                                    {{ $race_type->description }}
                                                    </textarea>
                                                    <label for="material-textarea-small">Descripció</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    </div>
                    
    
                </div>
            </div>
    </div>
    
    <div class="col-sm-9">
        <button class="btn btn-md btn-primary" type="submit">Editar Tipus de cursa</button>
    </div>

</div>

<input type="hidden" name="_token" value="{{ csrf_token() }}">
</form>
<!-- Page Content -->

</main>



@include('dashboard.layouts.footer')
