@include('dashboard.layouts.header')
@include('dashboard.layouts.sidemenu')

    
<main id="main-container">

<!-- Page Header -->
	<div class="content bg-gray-lighter">
	    <div class="row items-push">
	        <div class="col-sm-7">
	            <h1 class="page-heading">
	                Curses<small> llistat de curses</small>
	            </h1>
	        </div>
	        <div class="col-sm-5 text-right hidden-xs">
	            <ol class="breadcrumb push-10-t">
	                <li>Curses</li>
	                <li><a class="link-effect" href="">Gestió de curses</a></li>
	            </ol>
	        </div>
	    </div>
	</div>
<!-- Page Header -->

<!-- Page Content -->
	<div class="content">
		
			<a href="{{ URL::to('/') }}/races/create"><button class="btn btn-success push-10-r push-10" type="button"><i class="fa fa-plus"></i> Afegir Cursa</button></a>

		    <div class="block">

	        <div class="block-content">

	            <table id="datatable" class="table table-striped table-hover">
	            <thead>
	                <tr>
	                    <th style="width: 4%;">id</th>
	                    <th style="width: 20%;">Nom</th>
	                    <th>Localització</th>
	                    <th>Agrupació</th>
						<th>Tipus</th>
	                    <th style="width: 12%;">Data creació</th>
	                    <th class="text-center " style="width: 7%;"></th>
	                </tr>
	            </thead>
	            @foreach ($races as $race)
		            <tr>
		                <td>{{ $race->id }}</td>                
		                <td><b>{{ $race->name }}</b></td>
						<td><b>{{ $race->location }}</b></td>
		                <td><b>{{ $race->organizations->name }}</b></td>
		                <td><b>{{ $race->types->name }}</b></td>
		                <td>{{ $race->created_at }}</td>

		                <td class="text-center">
		                    <a class='btn btn-default btn-xs' data-toggle="tooltip" data-placement="top" title="Editar" href="{{ URL::to('/') }}/races/{{$race->id}}/edit">
		                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
		                    </a>
		                    <a class='btn btn-default btn-xs' data-toggle="tooltip" data-placement="top" title="Eliminar" href="{{ URL::to('/') }}/races/delete/{{$race->id}}">
		                        <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
		                    </a>
		            
		                </td>
		            </tr>
	            @endforeach
	        </table>

	        </div>
	    </div>

	</div>

<!-- Page Content -->

</main>



@include('dashboard.layouts.footer')
