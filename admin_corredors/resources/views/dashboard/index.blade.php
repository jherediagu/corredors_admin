@include('dashboard.layouts.header')
@include('dashboard.layouts.sidemenu')

    
<main id="main-container">

<!-- Page Header -->
	<div class="content bg-gray-lighter">
	    <div class="row items-push">
	        <div class="col-sm-7">
	            <h1 class="page-heading">
	                Dashboard <small>Menu principal</small>
	            </h1>
	        </div>
	        <div class="col-sm-5 text-right hidden-xs">
	            <ol class="breadcrumb push-10-t">
	                <li>Dashboard</li>
	            </ol>
	        </div>
	    </div>
	</div>
    <div class="content bg-gray-lighter">
	<!-- Page Content -->
				<!-- Stats -->
                <div class="content bg-white border-b">
                    <div class="row items-push text-uppercase">
                        <div class="col-xs-6 col-sm-3">
                            <div class="font-w700 text-gray-darker">Usuaris Actuals </div>
                            <div class="text-muted"><small><i class="fa fa-user"></i> Registrats</small></div>
                            <a class="h2 font-w300 text-primary" href="{{ URL::to('/') }}/users/registered">300</a>
                        </div>
                        <div class="col-xs-6 col-sm-3">
                            <div class="font-w700 text-gray-darker">Usuaris Actuals </div>
                            <div class="text-muted"><small><i class="fa fa-user-plus"></i> Abonats</small></div>
                            <a class="h2 font-w300 text-primary" href="{{ URL::to('/') }}/users/subscribed">2.790</a>
                        </div>
                        <div class="col-xs-6 col-sm-3">
                            <div class="font-w700 text-gray-darker">Ventes Botiga</div>
                            <div class="text-muted"><small><i class="si si-calendar"></i> mes actual</small></div>
                            <a class="h2 font-w300 text-primary" href="">3.880 €</a>
                        </div>
                        <div class="col-xs-6 col-sm-3">
                            <div class="font-w700 text-gray-darker">Ventes Botiga</div>
                            <div class="text-muted"><small><i class="si si-calendar"></i> Any actual</small></div>
                            <a class="h2 font-w300 text-primary" href="">200 €</a>
                        </div>
                    </div>
                </div>
                <!-- END Stats -->
	<!-- END Page Content -->
    </div>

    <div class="content bg-gray-lighter">

      <!-- Vertical Tiles -->
                    <h2 class="content-heading">Dades d'aquesta setmana</h2>

                    <!-- Simple Vertical Tiles -->
                    <div class="row">
                        <div class="col-sm-6 col-lg-3">
                            <a class="block block-bordered block-link-hover3" href="javascript:void(0)">
                                <table class="block-table text-center">
                                    <tbody>
                                        <tr>
                                            <td class="bg-gray-lighter border-r" style="width: 50%;">
                                                <div class="push-30 push-30-t">
                                                    <i class="si si-users fa-3x text-black-op"></i>
                                                </div>
                                            </td>
                                            <td style="width: 50%;">
                                                <div class="h1 font-w700"><span class="h2 text-muted">+</span> 20</div>
                                                <div class="h5 text-muted text-uppercase push-5-t">Usuaris</div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </a>
                        </div>
                        <div class="col-sm-6 col-lg-3">
                            <div class="block">
                                <a class="block block-bordered block-link-hover3" href="javascript:void(0)">
                                <table class="block-table text-center">
                                    <tbody>
                                        <tr>
                                            <td class="bg-gray-darker" style="width: 50%;">
                                                <div class="push-30 push-30-t">
                                                    <i class="si si-speech fa-3x text-white-op"></i>
                                                </div>
                                            </td>
                                            <td style="width: 50%;">
                                                <div class="h1 font-w700"><span class="h2 text-muted">+</span>11</div>
                                                <div class="h5 text-muted text-uppercase push-5-t">Comentaris</div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-6 col-lg-3">
                            <a class="block block-bordered block-link-hover3" href="javascript:void(0)">
                                <table class="block-table text-center">
                                    <tbody>
                                        <tr>
                                            <td class="bg-gray-lighter border-r" style="width: 50%;">
                                                <div class="push-30 push-30-t">
                                                    <i class="si si-trophy fa-3x text-black-op"></i>
                                                </div>
                                            </td>
                                            <td style="width: 50%;">
                                                <div class="h1 font-w700"><span class="h2 text-muted">+</span>3</div>
                                                <div class="h5 text-muted text-uppercase push-5-t">Inscripcions</div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </a>
                        </div>
                     <div class="col-sm-6 col-lg-3">
                            <div class="block">
                                <a class="block block-bordered block-link-hover3" href="javascript:void(0)">
                                <table class="block-table text-center">
                                    <tbody>
                                        <tr>
                                            <td style="width: 50%;">
                                                <div class="h1 font-w700"><span class="h2 text-muted">+</span> 10</div>
                                                <div class="h5 text-muted text-uppercase push-5-t">Seguidors</div>
                                            </td>
                                            <td class="bg-info" style="width: 50%;">
                                                <div class="push-30 push-30-t">
                                                    <i class="si si-social-facebook fa-3x text-white-op"></i>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                </a>
                            </div>
                        </div>
                    </div>

                </div>

    <div class="content">


                    <!-- END Simple Vertical Tiles -->
  <!-- Statistics Widgets -->
                    <h2 class="content-heading">Estadístiques ( anuals )</h2>

                    <!-- Jquery Sparkline (initialized in js/pages/base_ui_widgets.js), for more examples you can check out http://omnipotent.net/jquery.sparkline/#s-about -->
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="block">
                                                        <a class="block block-bordered block-link-hover3" href="javascript:void(0)">

                                <div class="block-header">
                                    <ul class="block-options">
                                        <li>
                                            <button type="button" data-toggle="block-option" data-action="refresh_toggle" data-action-mode="demo"><i class="si si-refresh"></i></button>
                                        </li>
                                    </ul>
                                    <h3 class="block-title">Usuaris nous</h3>
                                </div>
                                <div class="block-content block-content-full text-center bg-gray-lighter">
                                    <!-- Sparkline Bar 1 Container -->
                                    <span class="js-widget-line1">12,16,15,16,17,15,14,18</span>
                                </div>
                                <div class="block-content">
                                    <div class="row items-push text-center">
                                        <div class="col-xs-6">
                                            <div class="push-5"><i class="si si-graph fa-2x"></i></div>
                                            <div class="h5 font-w300 text-muted">250 Usuaris</div>
                                        </div>
                                        <div class="col-xs-6">
                                            <div class="push-5"><i class="si si-users fa-2x"></i></div>
                                            <div class="h5 font-w300 text-muted">+ 15% Usuaris</div>
                                        </div>
                                    </div>
                                </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="block">
                                                                                    <a class="block block-bordered block-link-hover3" href="javascript:void(0)">

                                <div class="block-header">
                                    <ul class="block-options">
                                        <li>
                                            <button type="button" data-toggle="block-option" data-action="refresh_toggle" data-action-mode="demo"><i class="si si-refresh"></i></button>
                                        </li>
                                    </ul>
                                    <h3 class="block-title">Forum</h3>
                                </div>
                                <div class="block-content block-content-full text-center bg-gray-lighter">
                                    <!-- Sparkline Bar 2 Container -->
                                    <span class="js-widget-line2">5,6,3,7,2,6,5,6</span>
                                </div>
                                <div class="block-content">
                                    <div class="row items-push text-center">
                                        <div class="col-xs-6">
                                            <div class="push-5"><i class="si si-support fa-2x"></i></div>
                                            <div class="h5 font-w300 text-muted">29 posts nous</div>
                                        </div>
                                        <div class="col-xs-6">
                                            <div class="push-5"><i class="si si-check fa-2x"></i></div>
                                            <div class="h5 font-w300 text-muted">15 Respostes noves</div>
                                        </div>
                                    </div>
                                </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="block">
                                                                                    <a class="block block-bordered block-link-hover3" href="javascript:void(0)">

                                <div class="block-header">
                                    <ul class="block-options">
                                        <li>
                                            <button type="button" data-toggle="block-option" data-action="refresh_toggle" data-action-mode="demo"><i class="si si-refresh"></i></button>
                                        </li>
                                    </ul>
                                    <h3 class="block-title">Vendes</h3>
                                </div>
                                <div class="block-content block-content-full text-center bg-gray-lighter">
                                    <!-- Sparkline Bar 3 Container -->
                                    <span class="js-widget-line3">50,90,80,90,80,70,110,100</span>
                                </div>
                                <div class="block-content">
                                    <div class="row items-push text-center">
                                        <div class="col-xs-6">
                                            <div class="push-5"><i class="si si-wallet fa-2x"></i></div>
                                            <div class="h5 font-w300 text-muted">1.900 €</div>
                                        </div>
                                        <div class="col-xs-6">
                                            <div class="push-5"><i class="si si-graph fa-2x"></i></div>
                                            <div class="h5 font-w300 text-muted">+ 50% Vendes</div>
                                        </div>
                                    </div>
                                </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <!-- END Jquery Sparkline -->

    </div>
                 <!-- Easy Pie Chart -->
                    <!-- END Statistics Widgets -->

</main>
<!-- END Main Container -->



@include('dashboard.layouts.footer')
