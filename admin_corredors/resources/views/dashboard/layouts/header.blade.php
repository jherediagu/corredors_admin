<!DOCTYPE html>
<html>
    <head>

        <meta charset="utf-8">

        <title>Corredors - Admin</title>

        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700%7COpen+Sans:300,400,400italic,600,700">
        <meta name="description" content="Corredors - Admin">

        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700%7COpen+Sans:300,400,400italic,600,700">

        <link rel="stylesheet" href="{{ asset('/js/plugins/datatables/jquery.dataTables.min.css') }}">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.11/css/dataTables.bootstrap.min.css">

        <script src="{{ asset('/js/pages/base_tables_datatables.js') }}"></script>
        <script src="{{ asset('/js/plugins/datatables/jquery.dataTables.min.js') }}"></script>


        <link href="{{ asset('/css/oneui.css') }}" rel="stylesheet" type="text/css" /> 

        <!-- sweetalert -->
        <link href="{{ asset('/sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css" />    
        <script src="{{ asset('/sweetalert/sweetalert.min.js') }}"></script>

        <!-- simditor -->
        <link rel="stylesheet" type="text/css" href="{{ asset('/simditor/styles/simditor.css') }}" rel="stylesheet" type="text/css"/>

        <script type="text/javascript" src="{{ asset('/simditor/scripts/module.js') }}"></script>
        <script type="text/javascript" src="{{ asset('/simditor/scripts/hotkeys.js') }}"></script>
        <script type="text/javascript" src="{{ asset('/simditor/scripts/uploader.js') }}"></script>
        <script type="text/javascript" src="{{ asset('/simditor/scripts/simditor.js') }}"></script>

        <!-- dropzone.js -->
        <link rel="stylesheet" type="text/css" href="{{ asset('/js/plugins/dropzonejs/dropzone.min.css') }}" rel="stylesheet" type="text/css"/>

        <!-- croppie -->
        <script type="text/javascript" src="{{ asset('/js/croppie/croppie.min.js') }}"></script>
        <link rel="stylesheet" type="text/css" href="{{ asset('/js/croppie/croppie.css') }}" rel="stylesheet" type="text/css"/>

        <?php header('Access-Control-Allow-Origin: *'); ?>

        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/css/select2.min.css" rel="stylesheet" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/easy-autocomplete/1.3.3/jquery.easy-autocomplete.min.js"></script>

    </head>
    <body>

@if(Session::has('success'))
    <script type="text/javascript">
    $( document ).ready(function() {
        swal("{{ Session::get('success') }}", "Acepta per continuar", "success")
    });
    </script>      
@endif
