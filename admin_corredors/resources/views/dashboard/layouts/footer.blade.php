
<footer id="page-footer" class="content-mini content-mini-full font-s12 bg-gray-lighter clearfix">
    <div class="pull-left">
        <a class="font-w600" href="http://www.javajan.com" target="_blank">Javajan</a> &copy; <span class="js-year-copy"></span>
    </div>
</footer>


        <!-- Apps Modal -->
        <!-- Opens from the button in the header -->
        <div class="modal fade" id="apps-modal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-sm modal-dialog modal-dialog-top">
                <div class="modal-content">
                    <!-- Apps Block -->
                    <div class="block block-themed block-transparent">
                        <div class="block-header bg-primary-dark">
                            <ul class="block-options">
                                <li>
                                    <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                                </li>
                            </ul>
                            <h3 class="block-title">Navegació</h3>
                        </div>
                        <div class="block-content">
                            <div class="row text-center">
                                <div class="col-xs-6">
                                    <a class="block block-rounded" href="{{ URL::to('/') }}">
                                        <div class="block-content text-white bg-default">
                                            <i class="si si-speedometer fa-2x"></i>
                                            <div class="font-w600 push-15-t push-15">Admin</div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-xs-6">
                                    <a class="block block-rounded" href="http://localhost:8888/newadmincorredors/corredors_admin/corredors_frontend/public/">
                                        <div class="block-content text-white bg-modern">
                                            <i class="si si-rocket fa-2x"></i>
                                            <div class="font-w600 push-15-t push-15">Web</div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END Apps Block -->
                </div>
            </div>
        </div>
        <!-- END Apps Modal -->
</body>

        <script>
            $(document).ready(function () {

                $('#push-info').delay(2000).fadeOut();

                $('#datatable').DataTable( {
                    } );

                $('.fileupload').click(function(e) {
                    $(this).find('input[type="file"]').click();
                });

                $('.fileupload input').click(function(e) {
                    e.stopPropagation();
                });


        });
           $uploadCrop = $('#upload-demo').croppie({
            exif: true,
            viewport: {
                width: 200,
                height: 200,
                type: 'circle'
            },
            boundary: {
                width: 300,
                height: 300
            }
        });


        </script>



        <script src="{{ asset('/js/core/jquery.min.js') }}"></script>
        <script src="{{ asset('/js/core/bootstrap.min.js') }}"></script>
        <script src="{{ asset('/js/core/jquery.slimscroll.min.js') }}"></script>
        <script src="{{ asset('/js/core/jquery.scrollLock.min.js') }}"></script>
        <script src="{{ asset('/js/core/jquery.appear.min.js') }}"></script>
        <script src="{{ asset('/js/core/jquery.countTo.min.js') }}"></script>
        <script src="{{ asset('/js/core/jquery.placeholder.min.js') }}"></script>
        <script src="{{ asset('/js/core/js.cookie.min.js') }}"></script>
        <script src="{{ asset('/js/app.js') }}"></script>

        <!-- Page JS Plugins -->
        <script src="{{ asset('/js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>

        <!-- Page JS Code -->
        <script src="{{ asset('/js/pages/base_pages_login.js') }}"></script>
        
        <!-- dropzone -->
        <script src="{{ asset('/js/plugins/dropzonejs/dropzone.min.js') }}"></script>

        <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"type="text/javascript"></script>



        <!-- Page JS Plugins -->
        <script src="{{ asset('/js/plugins/sparkline/jquery.sparkline.min.js') }}"></script>
        <script src="{{ asset('/js/plugins/easy-pie-chart/jquery.easypiechart.min.js') }}"></script>
        <script src="{{ asset('/js/plugins/slick/slick.min.js') }}"></script>

        <!-- Page JS Code -->
        <script src="{{ asset('/js/pages/base_ui_widgets.js') }}"></script>
        <script>
            $(function () {
                // Init page helpers (Slick Slider + Easy Pie Chart plugins)
                App.initHelpers(['slick', 'easy-pie-chart']);
            });
        </script>


</html>
