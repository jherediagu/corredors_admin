   <div id="page-container" class="sidebar-l sidebar-o side-scroll header-navbar-fixed">
            <!-- Side Overlay-->
            <aside id="side-overlay">
                <!-- Side Overlay Scroll Container -->
                <div id="side-overlay-scroll">
                    <!-- Side Header -->
                    <div class="side-header side-content">
                        <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
                        <button class="btn btn-default pull-right" type="button" data-toggle="layout" data-action="side_overlay_close">
                            <i class="fa fa-times"></i>
                        </button>
                        <span>
                            <i class="fa fa-user"></i>
                            <span class="font-w600 push-10-l">{{ Auth::user()->email }}</span>
                        </span>
                    </div>
                    <!-- END Side Header -->

                    <!-- Side Content -->
                    <div class="side-content remove-padding-t">
                        <!-- Notifications -->
                        <div class="block pull-r-l">
                            <div class="block-header bg-gray-lighter">
                                <ul class="block-options">
                                    <li>
                                        <button type="button" data-toggle="block-option" data-action="refresh_toggle" data-action-mode="demo"><i class="si si-refresh"></i></button>
                                    </li>
                                    <li>
                                        <button type="button" data-toggle="block-option" data-action="content_toggle"></button>
                                    </li>
                                </ul>
                                <h3 class="block-title">Canvis actuals</h3>
                            </div>
                            <div class="block-content">
                                <!-- Activity List -->
                                <ul class="list list-activity">
                                    <li>
                                        <i class="si si-wallet text-success"></i>
                                        <div class="font-w600">Info</div>
                                        <div><a href="javascript:void(0)">enllaç</a></div>
                                        <div><small class="text-muted">time</small></div>
                                    </li>
                                </ul>
                                <div class="text-center">
                                    <small><a href="javascript:void(0)">Obrir mes</a></small>
                                </div>
                                <!-- END Activity List -->
                            </div>
                        </div>
                        <!-- END Notifications -->


                        <!-- Quick Settings -->
                        <div class="block pull-r-l">
                            <div class="block-header bg-gray-lighter">
                                <ul class="block-options">
                                    <li>
                                        <button type="button" data-toggle="block-option" data-action="content_toggle"></button>
                                    </li>
                                </ul>
                                <h3 class="block-title">Opcions</h3>
                            </div>
                            <div class="block-content">
                                <!-- Quick Settings Form -->
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-xs-8">
                                                <div class="font-s13 font-w600">Logout</div>
                                                <div class="font-s13 font-w400 text-muted">Sortir de la sessió</div>
                                            </div>
                                            <div class="col-xs-4 text-right">
                                                <label class="css-input switch switch-sm switch-primary push-10-t">
                                                    <a  class="btn btn-sm btn-primary" href="{{ URL::to('/') }}/logout">Sortir</a>

                                                </label>
                                            </div>
                                        </div>
                                    </div>
    
                                <!-- END Quick Settings Form -->
                            </div>
                        </div>
                        <!-- END Quick Settings -->
                    </div>
                    <!-- END Side Content -->
                </div>
                <!-- END Side Overlay Scroll Container -->
            </aside>
            <!-- END Side Overlay -->

            <!-- Sidebar -->
            <nav id="sidebar">
                <!-- Sidebar Scroll Container -->
                <div id="sidebar-scroll">
                    <!-- Sidebar Content -->
                    <!-- Adding .sidebar-mini-hide to an element will hide it when the sidebar is in mini mode -->
                    <div class="sidebar-content">
                        <!-- Side Header -->
                        <div class="side-header side-content bg-white-op">
                            <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
                            <button class="btn btn-link text-gray pull-right hidden-md hidden-lg" type="button" data-toggle="layout" data-action="sidebar_close">
                                <i class="fa fa-times"></i>
                            </button>
                         
                            <a class="h5 text-white" href="{{ URL::to('/') }}">
                                <i class="fa fa-genderless text-primary"></i> <span class="h4 font-w600 sidebar-mini-hide"> Corredors</span>
                            </a>
                        </div>
                        <!-- END Side Header -->

                        <!-- Side Content -->
                        <div class="side-content">
                            <ul class="nav-main">
                                <li>
                                    <a class="active" href="{{ URL::to('/') }}"><i class="si si-speedometer"></i><span class="sidebar-mini-hide">Dashboard</span></a>
                                </li>
                                <li class="nav-main-heading"><span class="sidebar-mini-hide">Curses</span></li>
                                <li>
                                    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-badge"></i><span class="sidebar-mini-hide">Curses</span></a>
                                    <ul>
                                        <li>
                                            <a href="{{ URL::to('/') }}/races">Gestió Curses</a>
                                        </li>
                                        <li>
                                            <a href="{{ URL::to('/') }}/organizations">Agrupació / Lliga</a>
                                        </li>
                                        <li>
                                            <a href="{{ URL::to('/') }}/race_types">Tipus de cursa</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="nav-main-heading"><span class="sidebar-mini-hide">Principal</span></li>
                                <li>
                                    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-badge"></i><span class="sidebar-mini-hide">Usuaris</span></a>
                                    <ul>
                                        <li>
                                            <a href="{{ URL::to('/') }}/users/registered">Usuaris registrats</a>
                                        </li>
                                        <li>
                                            <a href="{{ URL::to('/') }}/users/subscribed">Usuaris abonats</a>
                                        </li>

                                    </ul>
                                </li>
                                <li>
                                    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-grid"></i><span class="sidebar-mini-hide">Serveis</span></a>
                                    <ul>
                                        <li>
                                            <a href="{{ URL::to('/') }}/challenges">Reptes</a>
                                        </li>
                                        <li>
                                            <a href="{{ URL::to('/') }}/clinicqr2">Clínics QR2</a>
                                        </li>
                                        <li>
                                            <a href="{{ URL::to('/') }}/shoes">Sabatilles</a>
                                        </li>
                                        <li>
                                            <a href="{{ URL::to('/') }}/trainers">Entrenadors</a>
                                        </li>
                                        <li>
                                            <a href="base_tables_datatables.html">Qualicat</a>
                                        </li>
                                          <li>
                                            <a href="base_tables_datatables.html">Llebres</a>
                                        </li>
                                          <li>
                                            <a href="base_tables_datatables.html">Proves materials</a>
                                        </li>


                                    </ul>
                                </li>
                                <li>
                                    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-note"></i><span class="sidebar-mini-hide">Comunitat QR2</span></a>
                                    <ul>
                                        <li>
                                            <a href="base_forms_premade.html">Rànking</a>
                                        </li>
                                        <li>
                                            <a href="base_forms_elements.html">Llista Socis</a>
                                        </li>
                                        <li>
                                            <a href="base_forms_pickers_more.html">Entrevistes</a>
                                        </li>
                                
                                    </ul>
                                </li>
                                <li class="nav-main-heading"><span class="sidebar-mini-hide">Informació</span></li>
                                <li>
                                    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-wrench"></i><span class="sidebar-mini-hide">Qui som</span></a>
                                    <ul>
                                        <li>
                                            <a href="{{ URL::to('/') }}/commissions">Comissions</a>
                                        </li>
                                    </ul>
                                </li>   
                                <li>
                                    <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-wrench"></i><span class="sidebar-mini-hide">Noticies</span></a>
                                    <ul>
                                        <li>
                                            <a href="{{ URL::to('/') }}/news">Noticies</a>
                                        </li>
                                    </ul>
                                </li>
   
           
                                <li class="nav-main-heading"><span class="sidebar-mini-hide">Botiga</span></li>
                                <li>
                                    <a href="frontend_home.html"><i class="si si-rocket"></i><span class="sidebar-mini-hide">Productes</span></a>
                                </li>
                                <li>
                                    <a href="frontend_home.html"><i class="si si-rocket"></i><span class="sidebar-mini-hide">Categories</span></a>
                                </li>
                            </ul>
                        </div>
                        <!-- END Side Content -->
                    </div>
                    <!-- Sidebar Content -->
                </div>
                <!-- END Sidebar Scroll Container -->
            </nav>
            <!-- END Sidebar -->

            <!-- Header -->
            <header id="header-navbar" class="content-mini content-mini-full">
                <!-- Header Navigation Right -->
                <ul class="nav-header pull-right">
                    <li>
                        <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
                        <button class="btn btn-default" data-toggle="layout" data-action="side_overlay_toggle" type="button">
                            <i class="fa fa-tasks"></i>
                        </button>
                    </li>
                </ul>
                <!-- END Header Navigation Right -->

                <!-- Header Navigation Left -->
                <ul class="nav-header pull-left">
                 <li class="hidden-md hidden-lg">
                        <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
                        <button class="btn btn-default" data-toggle="layout" data-action="sidebar_toggle" type="button">
                            <i class="fa fa-navicon"></i>
                        </button>
                    </li>
                    <li class="hidden-xs hidden-sm">
                        <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
                        <button class="btn btn-default" data-toggle="layout" data-action="sidebar_mini_toggle" type="button">
                            <i class="fa fa-ellipsis-v"></i>
                        </button>
                    </li>
                                        <li>
                        <!-- Opens the Apps modal found at the bottom of the page, before including JS code -->
                        <button class="btn btn-default pull-right" data-toggle="modal" data-target="#apps-modal" type="button">
                            <i class="si si-grid"></i>
                        </button>
                    </li>
                
                </ul>
                <!-- END Header Navigation Left -->

                
            </header>
            <!-- END Header -->