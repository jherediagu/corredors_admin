@include('dashboard.layouts.header')
@include('dashboard.layouts.sidemenu')
    <script type="text/javascript">
        $( document ).ready(function() {

            var editor = new Simditor({
              textarea: $('#editor')
              //optional options
            });
           
           });
    </script>
    
<main id="main-container">

<!-- Page Header -->
    <div class="content bg-gray-lighter">
        <div class="row items-push">
            <div class="col-sm-7">
                <h1 class="page-heading">
                    Crear Entrenador<small></small>
                </h1>
            </div>
            <div class="col-sm-5 text-right hidden-xs">
                <ol class="breadcrumb push-10-t">
                    <li>Serveis</li>
                    <li>Entrenadors</li>
                    <li><a class="link-effect" href="">Crear Entrenador</a></li>
                </ol>
            </div>
        </div>
    </div>
<!-- Page Header -->

<!-- Page Content -->

<form method="POST" action="{{ URL::to('/') }}/trainers">
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="block">
            
                
                <div class="block-content tab-content">
                    
                    <div class="tab-pane active" id="btabs-static-justified-home">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="block">
                                <div class="block-header">
                                </div>
                                <div class="block-content block-content-narrow">
                                    <div class="form-horizontal push-10-t">
                                        <div class="form-group">
                                            <div class="col-sm-9">
                                                <div class="form-material">
                                                    <input class="form-control" type="text" id="material-text" name="name" value="" placeholder="nom de l'entrenador">
                                                    <label for="material-text">Nom de l'entrenador</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <div class="form-material">
                                                    <br><textarea id="editor" autofocus name="description"></textarea>
                                                    <label for="material-email">Descripció de l'entrenador</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    </div>
                    
    
                </div>
            </div>
    </div>
    
    <div class="col-sm-9">
        <button class="btn btn-md btn-primary" type="submit">Crear Entrenador</button>
    </div>

</div>

<input type="hidden" name="_token" value="{{ csrf_token() }}">
</form>
<!-- Page Content -->

</main>



@include('dashboard.layouts.footer')
