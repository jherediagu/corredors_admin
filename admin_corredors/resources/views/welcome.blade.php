<!DOCTYPE html>
<html>
    <head>

        <meta charset="utf-8">

        <title>Corredors - Admin</title>

        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>


        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700%7COpen+Sans:300,400,400italic,600,700">
        <meta name="description" content="Corredors - Admin">

        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700%7COpen+Sans:300,400,400italic,600,700">


        <link href="{{ asset('/css/oneui.css') }}" rel="stylesheet" type="text/css" />    

    </head>
    <body>
             <!-- Login Content -->
        <div class="content overflow-hidden">
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4">
                    <!-- Login Block -->
                    <div class="block block-themed animated fadeIn">
                        <div class="block-header bg-primary">
                            <ul class="block-options">
                                <li>
                                    <a href="">Has perdut la contrasenya?</a>
                                </li>
                                <!--<li>
                                    <a href="" data-toggle="tooltip" data-placement="left" title="New Account"><i class="si si-plus"></i></a>
                                </li>-->
                            </ul>
                            <h3 class="block-title">Administració</h3>
                        </div>
                        <div class="block-content block-content-full block-content-narrow">

                            <!-- Login Title -->
                            <h1 class="h2 font-w600 push-30-t push-5">Corredors Administració</h1>
                            <p>Benvingut, inicia sessió per entrar.</p>
            

                            <form method="POST" action="login" class="js-validation-login form-horizontal push-30-t push-50">


                                @if (count($errors) > 0)

                                    <div  id="push-info"class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>

                                @endif

                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <div class="form-material form-material-primary floating">
                                            <input class="form-control" type="text" id="login-username" name="email">
                                            <label for="login-username">Usuari</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <div class="form-material form-material-primary floating">
                                            <input class="form-control" type="password" id="login-password" name="password">
                                            <label for="login-password">Contrasenya</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <label class="css-input switch switch-sm switch-primary">
                                            <input type="checkbox" id="login-remember-me" name="remember"><span></span> Recordar sessió?
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-6 col-md-4">
                                        <button class="btn btn-block btn-primary" type="submit"><i class="si si-login pull-right"></i> Log in</button>
                                    </div>
                                </div>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        </form>

                            <!-- END Login Form -->
                        </div>

                    </div>
                    <!-- END Login Block -->
                </div>
            </div>
        </div>
        <!-- END Login Content -->
        <script>
            $(document).ready(function () {

                $('#push-info').delay(2000).fadeOut();

            });
        </script>

        <!-- Login Footer -->
        <div class="push-10-t text-center animated fadeInUp">
            <small class="text-muted font-w600"><span class="js-year-copy"></span> &copy; Javajan</small>
        </div>
        <!-- END Login Footer -->

        <script src="{{ asset('/js/core/jquery.min.js') }}"></script>
        <script src="{{ asset('/js/core/bootstrap.min.js') }}"></script>
        <script src="{{ asset('/js/core/jquery.slimscroll.min.js') }}"></script>
        <script src="{{ asset('/js/core/jquery.scrollLock.min.js') }}"></script>
        <script src="{{ asset('/js/core/jquery.appear.min.js') }}"></script>
        <script src="{{ asset('/js/core/jquery.countTo.min.js') }}"></script>
        <script src="{{ asset('/js/core/jquery.placeholder.min.js') }}"></script>
        <script src="{{ asset('/js/core/js.cookie.min.js') }}"></script>
        <script src="{{ asset('/js/app.js') }}"></script>

        <!-- Page JS Plugins -->
        <script src="{{ asset('/js/plugins/jquery-validation/jquery.validate.min.js') }}"></script>

        <!-- Page JS Code -->
        <script src="{{ asset('/js/pages/base_pages_login.js') }}"></script>

</html>
