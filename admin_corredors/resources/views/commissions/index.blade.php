@include('dashboard.layouts.header')
@include('dashboard.layouts.sidemenu')

    
<main id="main-container">

<!-- Page Header -->
	<div class="content bg-gray-lighter">
	    <div class="row items-push">
	        <div class="col-sm-7">
	            <h1 class="page-heading">
	                Comissions<small> llistat de comissions del apartat qui som</small>
	            </h1>
	        </div>
	        <div class="col-sm-5 text-right hidden-xs">
	            <ol class="breadcrumb push-10-t">
	                <li>Qui som</li>
	                <li><a class="link-effect" href="">Comissions</a></li>
	            </ol>
	        </div>
	    </div>
	</div>
<!-- Page Header -->

<!-- Page Content -->
	<div class="content">
		
			<a href="{{ URL::to('/') }}/commissions/create"><button class="btn btn-success push-10-r push-10" type="button"><i class="fa fa-plus"></i> Afegir Comissió</button></a>

		    <div class="block">

	        <div class="block-content">

	            <table id="datatable" class="table table-striped table-hover">
	            <thead>
	                <tr>
	                    <th style="width: 4%;">id</th>
	                    <th style="width: 20%;">Comissió</th>
	                    <th>Objectiu</th>
	                    <th>Membres</th>
	                    <th style="width: 12%;">Data creació</th>
	                    <th class="text-center " style="width: 7%;"></th>
	                </tr>
	            </thead>
	            @foreach ($commissions as $commission)
		            <tr>
		                <td>{{ $commission->id }}</td>                
		                <td><b>{{ $commission->commission }}</b></td>
						<td><b>{{ $commission->objective }}</b></td>
		                <td><b>{{ $commission->members }}</b></td>
		                <td>{{ $commission->created_at }}</td>

		                <td class="text-center">
		                    <a class='btn btn-default btn-xs' data-toggle="tooltip" data-placement="top" title="Editar" href="{{ URL::to('/') }}/commissions/{{$commission->id}}/edit">
		                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
		                    </a>
		                    <a class='btn btn-default btn-xs' data-toggle="tooltip" data-placement="top" title="Eliminar" href="{{ URL::to('/') }}/commission/delete/{{$commission->id}}">
		                        <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
		                    </a>
		            
		                </td>
		            </tr>
	            @endforeach
	        </table>

	        </div>
	    </div>

	</div>

<!-- Page Content -->

</main>



@include('dashboard.layouts.footer')
