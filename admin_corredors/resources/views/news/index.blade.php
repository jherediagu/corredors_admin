@include('dashboard.layouts.header')
@include('dashboard.layouts.sidemenu')

    
<main id="main-container">

<!-- Page Header -->
	<div class="content bg-gray-lighter">
	    <div class="row items-push">
	        <div class="col-sm-7">
	            <h1 class="page-heading">
	                Noticies<small> llistat de noticies</small>
	            </h1>
	        </div>
	        <div class="col-sm-5 text-right hidden-xs">
	            <ol class="breadcrumb push-10-t">
	                <li>Noticies</li>
	                <li><a class="link-effect" href="">Noticies</a></li>
	            </ol>
	        </div>
	    </div>
	</div>
<!-- Page Header -->

<!-- Page Content -->
	<div class="content">
		
			<a href="{{ URL::to('/') }}/news/create"><button class="btn btn-success push-10-r push-10" type="button"><i class="fa fa-plus"></i> Afegir Noticia</button></a>

		    <div class="block">

	        <div class="block-content">

	            <table id="datatable" class="table table-striped table-hover">
	            <thead>
	                <tr>
	                    <th style="width: 4%;">id</th>
	                    <th>Títol</th>
	  	                <th>Subtítol</th>
	                    <th style="width: 20%;">Data creació</th>
	                    <th class="text-center " style="width: 7%;"></th>
	                </tr>
	            </thead>
	            @foreach ($news as $new)
		            <tr>
		                <td>{{ $new->id }}</td>                
						<td><b>{{ $new->title }}</b></td>
						<td><b>{{ $new->subtitle }}</b></td>
		                <td>{{ $new->created_at }}</td>

		                <td class="text-center">
		                    <a class='btn btn-default btn-xs' data-toggle="tooltip" data-placement="top" title="Editar" href="{{ URL::to('/') }}/news/{{$new->id}}/edit">
		                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
		                    </a>
		                    <a class='btn btn-default btn-xs' data-toggle="tooltip" data-placement="top" title="Eliminar" href="{{ URL::to('/') }}/news/delete/{{$new->id}}">
		                        <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
		                    </a>
		            
		                </td>
		            </tr>
	            @endforeach
	        </table>

	        </div>
	    </div>

	</div>

<!-- Page Content -->

</main>



@include('dashboard.layouts.footer')
