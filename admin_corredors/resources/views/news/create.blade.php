@include('dashboard.layouts.header')
@include('dashboard.layouts.sidemenu')
    <script type="text/javascript">
        $( document ).ready(function() {

            var editor = new Simditor({
              textarea: $('#editor')
              //optional options
            });
           
           });
    </script>
    
<main id="main-container">

<!-- Page Header -->
    <div class="content bg-gray-lighter">
        <div class="row items-push">
            <div class="col-sm-7">
                <h1 class="page-heading">
                    Crear Noticia<small></small>
                </h1>
            </div>
            <div class="col-sm-5 text-right hidden-xs">
                <ol class="breadcrumb push-10-t">
                    <li>Noticia</li>
                    <li><a class="link-effect" href="">Crear Noticia</a></li>
                </ol>
            </div>
        </div>
    </div>
<!-- Page Header -->

<!-- Page Content -->

<form method="POST" action="{{ URL::to('/') }}/news">
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="block">
            
                
                <div class="block-content tab-content">
                    
                    <div class="tab-pane active" id="btabs-static-justified-home">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="block">
                                <div class="block-header">
                                </div>
                                <div class="block-content block-content-narrow">
                                    <div class="form-horizontal push-10-t">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <div class="form-material">
                                                    <input class="form-control" type="text" id="material-text" name="title" value="" placeholder="nom de l'entrenador">
                                                    <label for="material-text">Títol</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <div class="form-material">
                                                    <input class="form-control" type="text" id="material-text" name="subtitle" value="" placeholder="nom de l'entrenador">
                                                    <label for="material-text">Subtítol</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <div class="form-material">
                                                    <br><textarea id="editor" autofocus name="content"></textarea>
                                                    <label for="material-email">Text de la noticia</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    </div>
                    
    
                </div>
            </div>
    </div>
    
    <div class="col-sm-9">
        <button class="btn btn-md btn-primary" type="submit">Crear Noticia</button>
    </div>

</div>

<input type="hidden" name="_token" value="{{ csrf_token() }}">
</form>
<!-- Page Content -->

</main>



@include('dashboard.layouts.footer')
