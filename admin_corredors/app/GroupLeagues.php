<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupLeagues extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'league_groups';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'name', 'description', 'created_at', 'updated_at'];
}
