<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Races extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'races';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'name', 'location', 'organization', 'type', 'date', 'description'];

    /**
     * Get the type record associated with the race.
     */
    public function types()
    {
        return $this->hasOne('App\RaceTypes' , 'id', 'type');
    }

    /**
     * Get the organization record associated with the race.
     */
    public function organizations()
    {
        return $this->hasOne('App\GroupLeagues' , 'id', 'organization');
    }
}
