<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersCorredors extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users_corredors';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['nickname','email','name','account_type','lastname','birthday','dni','city','address','postal_code','province','telephone','titular_name','titular_lastname','IBAN_country_code','IBAN_cheque_number','IBAN_bank_code','IBAN_sort_code','IBAN_account_number','concept'];

     /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];
}


