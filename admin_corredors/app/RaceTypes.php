<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RaceTypes extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'races_type';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'name', 'description', 'created_at', 'updated_at'];



    /**
     * Get the type that owns the race.
     */
    public function races()
    {
        return $this->belongsTo('App\Races' ,'type', 'id');
    }

}
