<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// create user admin
Route::get('create_admin', function () {

	DB::table('users')
		->insert(
		    [	'name' 			=> 'admin', 
		    	'email' 		=> 'admin@javajan.com',
		    	'password' 		=> Hash::make('t5OfU0'),
		    	'account_type' 	=> 1]
	);

	return view('welcome');

});

Route::get('/', 		'Auth\AuthController@index'); 		// Show login
Route::post('login', 	'Auth\AuthController@postLogin');  	// Post login
Route::get('logout', 	'Auth\AuthController@getLogout');	// Logout


// private routes after login
Route::group(['middleware' => 'auth'], function () {

	Route::get('dashboard', 		'DashboardController@index');		// Dashboard index
	

	// USERS ROUTES
	Route::get('users/create', 					'UsersController@create');			// Users Create View
	Route::post('users/store', 					'UsersController@store');			// Users Store View

	Route::get('users/registered', 				'UsersController@indexRegistered');	// Registed Users View
	Route::get('users/registered/edit/{id}', 	'UsersController@edit');			// Registed Users Edit View
	Route::any('users/registered/update/{id}', 	'UsersController@update');			// Registed Users Update
	Route::get('users/registered/delete/{id}', 	'UsersController@destroy');			// Registed Users Delete

	Route::get('users/subscribed', 				'UsersController@indexSubscribed');	// Subscribed Users View
	Route::get('users/subscribed/edit/{id}', 	'UsersController@edit');			// Subscribed Users Edit View
	Route::get('users/subscribed/delete/{id}', 	'UsersController@destroy');			// Subscribed Users Delete
	// END USER ROUTES



	// COMISSIONS ROUTES
	Route::resource('commissions', 		 'ComissionsController'); 			// Resources Comissions --> GET / POST / PUT / DELETE
	Route::get('commission/delete/{id}', 'ComissionsController@destroy'); 	// Comissions --> DELETE
	// END COMISSIONS ROUTES


	// SHOES ROUTES
	Route::resource('shoes', 		 'ShoesController'); 			// Resources Shoes --> GET / POST / PUT / DELETE
	Route::get('shoes/delete/{id}',  'ShoesController@destroy'); 	// Shoes --> DELETE
	// END SHOES ROUTES


	// CHALLENGES ROUTES
	Route::resource('challenges', 		 	'ChallengesController'); 				// Resources Challenges --> GET / POST / PUT / DELETE
	Route::post('uploadimage-challenge',  	'ChallengesController@uploadimage'); 	// Upload Image Challange
	Route::get('challenges/delete/{id}', 	'ChallengesController@destroy'); 		// Challenges --> DELETE
	// END CHALLENGES ROUTES


	// CLINICQR2 ROUTES
	Route::resource('clinicqr2', 		 	'ClinicQR2Controller'); 			// Resources ClinicQR2 --> GET / POST / PUT / DELETE
	Route::post('uploadimage-clinicqr2',  	'ClinicQR2Controller@uploadimage'); // Upload Image ClinicQR2
	Route::get('clinicqr2/delete/{id}', 	'ClinicQR2Controller@destroy'); 	// ClinicQR2 --> DELETE
	// END CLINICQR2 ROUTES


	// TRAINERS ROUTES
	Route::resource('trainers', 		 	'TrainersController'); 				// Resources Trainers --> GET / POST / PUT / DELETE
	Route::post('uploadimage-trainers',  	'TrainersController@uploadimage'); 	// Upload Image Trainers
	Route::get('trainers/delete/{id}', 		'TrainersController@destroy'); 		// Trainers --> DELETE
	// END TRAINERS ROUTES


	// NEWS ROUTES
	Route::resource('news', 		 	'NewsController'); 				// Resources News --> GET / POST / PUT / DELETE
	Route::get('news/delete/{id}', 		'NewsController@destroy'); 		// News --> DELETE
	// END NEWS ROUTES


	// RACES ROUTES
	Route::resource('races', 		 	'RacesController'); 				// Resources Races --> GET / POST / PUT / DELETE
	Route::get('races/delete/{id}', 	'RacesController@destroy'); 		// Races --> DELETE
	// END RACES ROUTES

	// RACE TYPES ROUTES
	Route::resource('race_types', 		 	'RaceTypesController'); 				// Resources Race types --> GET / POST / PUT / DELETE
	Route::get('race_types/delete/{id}', 	'RaceTypesController@destroy'); 		// Race types --> DELETE
	// END RACE TYPES ROUTES

	// ORGANIZATIONS ROUTES
	Route::resource('organizations', 		 	'OrganizationsController'); 				// Resources Organizations --> GET / POST / PUT / DELETE
	Route::get('organizations/delete/{id}', 	'OrganizationsController@destroy'); 		// Organizations --> DELETE
	// END RACE TYPES ROUTES
});