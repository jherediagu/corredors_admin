<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Races as Races;
use App\RaceTypes as RaceTypes;
use App\GroupLeagues as Organizations;

class RacesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $races = Races::all();
        return view('races.index',  ['races' => $races]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $types          = RaceTypes::lists('name','id')->all();
        $organizations  = Organizations::lists('name','id')->all();

        return view('races.create',  ['types' => $types , 'organizations' => $organizations]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $races_data = $request->input();

        $race = new Races($races_data);

        $race->save();

        $request->session()->flash('success', 'Cursa creada correctament!');
        return redirect('races');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $types          = RaceTypes::lists('name','id')->all();
        $organizations  = Organizations::lists('name','id')->all();
        $race           = Races::where('id', $id)->first();

        return view('races.edit', ['race' => $race ,'types' => $types, 'organizations' => $organizations]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $race = Races::find($id);

        $race_data = $request->input();

        $race->fill($race_data);

        $race->save();

        $request->session()->flash('success', 'Cursa editada correctament!');
        return redirect('races');    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // delete row
        Races::where('id', $id)->delete();
        return back();
    }
}
