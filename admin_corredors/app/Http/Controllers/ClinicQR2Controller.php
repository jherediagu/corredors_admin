<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ClinicQR2 as ClinicQR2;

class ClinicQR2Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $clinicqr2 = ClinicQR2::all();

        return view('clinicqr2.index' ,['clinicqr2' => $clinicqr2]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('clinicqr2.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $clinicqr2_data = $request->input();

        $clinicqr2 = new ClinicQR2($clinicqr2_data);

        $clinicqr2->save();

        $request->session()->flash('success', 'ClinicQR2 creada correctament!');
        return redirect('clinicqr2');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $clinicqr2 = ClinicQR2::where('id', $id)->first();

        return view('clinicqr2.edit',['clinicqr2' => $clinicqr2]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $clinicqr2 = ClinicQR2::find($id);
        
        $clinicqr2_data = $request->input();

        $clinicqr2->fill($clinicqr2_data);

        $clinicqr2->save();

        $request->session()->flash('success', 'ClinicQR2 editat correctament!');
        return redirect('clinicqr2');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ClinicQR2::where('id', $id)->delete();
        return back();
    }
}
