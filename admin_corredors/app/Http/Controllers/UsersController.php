<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\UsersCorredors as UsersCorredors;

class UsersController extends Controller
{
    
    /**
     * Display Subscribed Users view.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexSubscribed()
    {
        // account type subscribed = 1
        $users_corredors = UsersCorredors::where('account_type', 1)->get();
        
        return view('users.subscribed', ['registered' => $users_corredors]);
    }


    /**
     * Display Registered Users view.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexRegistered()
    {
        // account type registered = 0
        $users_corredors = UsersCorredors::where('account_type', 0)->get();

        return view('users.registered', ['registered' => $users_corredors]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $user_data = $request->input();

        $user = new UsersCorredors($user_data);

        //dd($user);

        $user->save();

        $request->session()->flash('success', 'Usuari creat correctament!');
        return redirect('dashboard');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $users_corredors = UsersCorredors::where('id', $id)->first();

        return view('users.registered.edit', ['user' => $users_corredors]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $user = UsersCorredors::find($id);

        $user_data = $request->input();

        $user->fill($user_data);

        $user->save();

        $request->session()->flash('success', 'Usuari editat correctament!');
        return redirect('dashboard');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // delete row
        UsersCorredors::where('id', $id)->delete();
        return back();
    }
}
