<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClinicQR2 extends Model
{
     /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'clinicqr2';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'title_clinic', 'title', 'description', 'date', 'hour', 'place', 'number_seats']; 

}
